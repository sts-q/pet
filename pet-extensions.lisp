;;;===========================================================================

;;; :file:   pet-extensions.lisp        pet extensions

;;;===========================================================================
(in-package :pet)

(print :pet-extensions)

;;;===========================================================================
(defun sort-lines( column lines )
        "i sl -- sl  // sort string list, compare strings from column i to string end"
        (sort lines (lambda( l1 l2 ) (string< l1 l2 :start1 column :start2 column)))
        )


(defun remove-trailing-cr( line )
           "s -- s  // remove trailing cr"
           (let ((len (length line)))
           (if (and (> len 0)   (char= #\cr (aref line (1- len))))
                   (subseq line 0 (1- len))
                   line)))

(defun remove-trailing-crs( viewer )
           "--  // remove trailing cr from all lines"
           (setf upper-roll~    (map 'list #'remove-trailing-cr upper-roll~))
           (set-text~           (map 'list #'remove-trailing-cr text~ ))
           t)

;;;===========================================================================
;;; :section:           get-irc-chatlog

(defun get-irc-chatlog()
        "-- text"
        (let* (
               (host    "lisp.metatem.net")
               (port    80)
               (path    "/mezzano/mezzano.irclog.txt")
               (bytes   (file.read-http-page-bytes host port path))
               (lines   (text.from-byte-array bytes)))
           (say / :time :> (truncate (length bytes) 1024) "KiB /" (length lines) " lines read from  " host)
           lines))

(defun wget-irc-chatlog( )
        "--"  (say / :time :> :wget-irc-chatlog)
        (labels ((date( s )     s )
                 (name( s )     (concat (spaces (- 12 (length s)))   s))
                 (message( s )  s)
                 (fmt( line )
                      (cond
                        ((equal "--- "  (take 4 line))          (concat "## " line))
                        ((search "-!-" line                   ) (concat "; "  line))
                        (t     (let* ((<pos  (position #\< line))
                                      (>pos  (and <pos (position #\> line :start <pos))))
                                   (if (and <pos >pos)
                                        (concat "  "  (date    (subseq line 0  <pos))
                                                " "   (name    (subseq line (+ 2 <pos)  >pos))
                                                "   " (message (subseq line (+ 2 >pos))))
                                        line))))))
        (let* ((lines        (get-irc-chatlog))
               (outfile      "REMOTE:/home/heiko/q/mezzano/irc-chatlog.md")
               (indent       33)
               (text-width   69)
               (br           (+ indent text-width)))
           (setf lines   (map 'list  #'fmt lines))                              (say / :time :> :fmt-done)
           (setf lines
                (let ((nt      nil)
                      (ot      lines)
                      (month   ""))
                   (loop
                        (return-when (not ot))
                        (let ((l (car ot)))
                        (when (equal "##" (take 2 l))
                                (let ((m (sixth (chunks l))))
                                        (when (not(equal m month))
                                                (setf month m)
                                                (push (drop 1 l) nt))))         ; push monthly headline
                          (push (pop ot) nt)))
                   (reverse nt)))                                               (say / :time :> :month-done)
           (setf lines
                (let ((nt   nil)
                      (ot   lines))
                   (loop
                        (return-when (not ot))
                        (cond
                          ((and (not (equal ";" (take 1 (car ot)))) (<  br (length (car ot))))
                                (let* ((h    (take indent (car ot)))
                                       (mess (drop indent (car ot)))
                                       (txt  (format-markdown-paragraph
                                                text-width
                                                (list mess)
                                                )))
                                   (push (concat h (car txt)) nt)
                                   (dolist (l (cdr txt))  (push (concat (spaces indent) l) nt)))
                                (pop ot))
                          (t    (push (pop ot) nt))
                          ))
                   (reverse nt)))                                               (say / :time :> :paragraphs-done)
           (file.write-lines outfile lines)
           (say / :time :> (length lines) "lines written to "  outfile)
           (gui :name :irc :cols 112 :lines 17 :from-mdir "irc-chatlog.md" :xpos 1500 :ypos 300)
           (jmp-text-end (viewer.with-name :irc))
           )))

;;;===========================================================================
;;; :section:           send commando to irc-client

(defvar irc.*client*            nil     "irc client instance")
(defvar irc.*tt*                nil     "from irc-server received messages")
(defvar irc.*links*             nil     "links found")
(defvar irc.*speak-toggle*      nil     "Print copy of logged messages to log.")
(defparameter irc.*autostart-toggle*  t "Autostart IRC Client, restart when connection lost.")

(defvar irc.*tell*              nil     "Send message, when someone joins again.")

(defvar irc.*mess-quit*         "Mezzano-OS with Pet IRC client."
                                "send to each channel with quit command.")

(defparameter irc.*dont-log* '(
        :PING
        :JOIN
        :NICK
        :PART
        :QUIT
        :RPL-MOTD
        )   "don't log these commands")

(defun hhmm( time )
        "i -- s"
        (multiple-value-bind (ss mm hh)
                (decode-universal-time time)
            (declare (ignore ss))
            (format nil "~2,'0d:~2,'0d" hh mm)))

(defun irc.logfun( time prefix command parameters )
        "i s s sl --  // push from irc-server received message to log list"
        (let ((channel  nil)
              (nick     nil)
              (message  nil)
              )
        (when (stringp command)
                (setf command (intern command :keyword)))

        (when (and irc.*speak-toggle* (eql :PRIVMSG command))           ; print to logstream, not t: irc calls
                (format logstream.*stream* "~&; ~A ~A <~A>" (hhmm time) (car parameters) prefix )
                (let ((lines  (format-markdown-paragraph 69 (cdr parameters))))
                   (dolist (l lines)   (format logstream.*stream* "~&~30,T~A" l))))

        (unless (member command irc.*dont-log*)
                (setf time     (hhmm time))
                (when (member command '(:PRIVMSG :JOIN))
                        (let ((pos  (position #\! prefix)))
                           (setf channel  (pop parameters))
                           (setf nick     (subseq prefix 0 pos))
                           (setf prefix   (subseq prefix (1+ pos)))
                           ))
                (when (member command '(:QUIT :NICK :MODE
                                        :PART :NOTICE))
                        (let ((pos  (position #\! prefix)))
                           (setf nick     (subseq prefix 0 pos))
                           (setf prefix   (subseq prefix (1+ pos)))
                           ))

                (when (member command '(:PART :MODE))
                        (setf channel (pop parameters)))

                (when (member command '(:SAY))
                        (setf channel (pop parameters))
                        (setf nick    (pop parameters)))

                (when (member command '(:KICK))
                        (setf channel (pop parameters))
                        (setf nick    (pop parameters)))

                (when (member command '(    ; :NOTICE
                                        :RPL-HELPSTART :RPL-HELPTXT :RPL-ENDOFHELP
                                        :RPL-STATSUPTIME :RPL-STATSCONN :RPL-ENDOFSTATS
                                        :RPL-VERSION :RPL-ISUPPORT
                                        :RPL-MOTDSTART
                                        :RPL-WELCOME :RPL-YOURHOST :RPL-CREATED
                                        :RPL-MYINFO :RPL-ISUPPORT :RPL-ISUPPORT :RPL-ISUPPORT
                                        :RPL-LUSERCLIENT :RPL-LUSEROP :RPL-LUSERUNKNOWN :RPL-LUSERCHANNELS
                                        :RPL-LUSERME :RPL-LOCALUSERS :RPL-GLOBALUSERS :RPL-STATSCONN
                                        :RPL-MOTD
                                        :RPL-WHOISUSER :RPL-WHOISCHANNELS :RPL-WHOISSERVER :RPL-WHOISHOST
                                        :RPL-WHOISIDLE :RPL-WHOISLOGGEDIN :RPL-ENDOFWHOIS  :RPL-WHOISSECURE
                                        ; :RPL-ENDOFMOTD
                                        ))
                      (setf nick    (pop parameters)))

                (when (member command '(:RPL-TIME))
                        (setf nick    (pop parameters))
                        (pop parameters))

                (when (member command '(
                                        :RPL-WHOREPLY  :RPL-ENDOFWHO
                                        :RPL-AWAY
                                        :RPL-NAMREPLY  :RPL-ENDOFNAMES
                                        :RPL-CHANNELURL
                                        :RPL-TOPIC     :RPL-TOPIC-TIME
                                        :ERR-LINKCHANNEL
                                        ))
                        (setf nick    (pop parameters))
                        (setf channel (pop parameters))
                        (when (member channel '("=" "@") :test #'equal)
                                (setf channel (pop parameters))))

                (mapc (lambda( s )  (when (equalp "http" (take 4 s))
                                       (push   (list time channel nick   (subseq s 0 (position #\Space s)))
                                               irc.*links*)))
                      (mapcan #'chunks parameters))

                (setf message (if (not parameters) nil (apply #'concatenate 'string (map 'list (lambda( s ) (concatenate 'string s " "))  parameters))))
                (push (list   time   command channel nick message prefix)   irc.*tt*)

                (let* ((viewer (viewer.with-name :main)))
                   (when (and (equal "pet-irc-page.md" (file-namestring (path viewer)))
                              (eql :PRIVMSG command))
                        (mark.push viewer :user)
                        (jmp-text-start viewer)
                        (let* ((s (concat "; " channel " UNREAD"))
                               (y (forward-find viewer 0 (lambda( line ) (search s line)))))
                           (when y
                                (jmp-to-position-fn viewer y 0)
                                (set-text viewer
                                        (above viewer (cy viewer))
                                        (concat time" "channel"  "nick)
                                        (format-markdown-paragraph 69 (list message) :indent 25)
                                        (concat "; " channel " UNREAD")
                                        (below viewer  (cy viewer)))))
                        (mark.find viewer (mark.pop viewer :user))
                        (rewrite-screen viewer))))

        (when (eql :JOIN command)
           (let ((tell (assoc nick irc.*tell* :test #'equal)))
                (when (and tell (equal channel (second tell)))
                        (sleep 1)
                        (irc.send :chan (second tell))
                        (sleep 1)
                        (irc.send :say (third tell))
                        (setf irc.*tell* nil)
                        )))))

(defun logtime( rec )           (first rec))
(defun command( rec )           (second rec))
(defun channel( rec )           (third rec))
(defun nick( rec )              (fourth rec))
(defun message( rec )           (fifth rec))
(defun from( rec )              (sixth rec))
(defun command-is( cmd rec )    (eql cmd (command rec)))
(defun channel-is( chan rec )   (equalp chan (channel rec)))


(defun irc.dump-log()
        (dolist (l (reverse (take 2500 pet::irc.*tt*)))
                (format t  "~&~A ~A~22,T ~A~37,T~A~53,T ~A~95,T ~A"
                        (logtime l) (command l) (channel l) (nick l) (message l) (from l)))
        (say / :> (length pet::irc.*tt*) :entries)
        )

(defun irc.dump-links()   (dolist (l (reverse irc.*links*))  (say / l)))


;;;---------------------------------------------------------------------------
(defun change-main-to-irc( text )
        "Display irc-page in viewer main"
        (let* ((viewer (viewer.with-name :main)))
           (show viewer "pet-irc-page.md")
           (text-set viewer  text)
           (pet:rewrite-screen viewer)))

(defun irc.said()
        (let  ((channels  (sort (remove-duplicates (map 'list #'channel irc.*tt*) :test #'equalp) #'string<))
               (lines nil))
        (flet ((++(line)  (push line lines)))
        (flet ((lf()      (++ (empty-string))))
        (flet ((said( channel )
                (lf)
                (++ (format nil  "##  ~A" channel))
                (dolist (l (reverse irc.*tt*))
                    (when (or
                          (and (command-is :PRIVMSG l) (channel-is channel l))
                          (and (command-is :SAY l) (channel-is channel l)))
                       (let ((ls (format-markdown-paragraph 69 (list(message l)))))
                       (++ (concat (format nil "~&~A ~A~20,T~A~35,T" (logtime l) (channel l) (nick l)) (car ls)))
                       (dolist (m (cdr ls))   (++ (format nil "~35,T~A~%" m))))))
                (++ (copy-seq ";---------------------------------------------------------"))
                (++ (concat "; " channel " UNREAD"))))
        (++ "# IRC Channels")
        (dolist (ch channels)   (said ch))
        (lf)
        (++ "===============================================================================")
        (++ "# Links found")
        (lf)
        (dolist (l   (sort (remove-duplicates   irc.*links*   :test #'equal :key #'fourth) #'string< :key #'fourth))
                (++ (format nil "~&~A  ~A~20,T ~A~40,T ~A" (first l) (second l) (third l) (fourth l))))
        (lf)
        (++ "===============================================================================")
        (change-main-to-irc (reverse lines)))))))


;;;---------------------------------------------------------------------------
(defun irc.spawn()
        "// spawn mezzano.irc-client"
        (multiple-value-bind (thread instance)
                (mezzano.irc-client:spawn  :xpos 5 :ypos 1040 :width 1250 :height 320   :logfun 'irc.logfun)
                (declare (ignore thread))
           (setf irc.*client* instance)))

(defun irc.send( command &optional text )
        (let ((irc  irc.*client*)
              (line (concat "/" (string-downcase(symbol-name command)) " " text)))
           (if irc
                (mezzano.irc-client:irc-send irc line)
                (format t "~&irc.send: no client found."))))

(defun irc.connect()
        ""
        (let ((pwd   (first (file.read-lines (path.make +load-path+ "irc-mess.txt")))))
           (irc.send :nick    "sts-q")
;;;        (sleep 5)
           (irc.send :connect "libera")
           (sleep 3)
           (irc.send :msg     (concat "NickServ identify "pwd))
           (sleep 3)
           (irc.send :time)
           (sleep 3)
           ))

(defun irc.join()
        ""
;;;     (irc.send :join "#freenode")
;;;     (irc.send :join "#emacs")
;;;     (irc.send :join "#clim")
;;;     (irc.send :join "#lispcafe")
;;;     (irc.send :join "#Mecrisp")
;;;     (irc.send :join "#IRChelp")
;;;     (irc.send :join "#lisp")
;;;     (irc.send :join "#concatenative")
;;;     (irc.send :join "#gopher")
;;;     (irc.send :join "#tcc")
;;;     (irc.send :join "#retro")
;;;     (irc.send :join "#gopherproject")
        (irc.send :join "#chicken")
        (irc.send :join "#guile")
;;;     (irc.send :join "#bitreich-de")
;;;     (irc.send :join "#vlang")
        (irc.send :join "#clschool")
;;;     (irc.send :join "#racket")
;;;     (irc.send :join "#oberon")
;;;     (irc.send :join "#osdev")
;;;     (irc.send :join "#sml")
;;;     (irc.send :join "#forth")
;;;     (irc.send :join "#squeak")
        (irc.send :join "#mnt-reform")
        (irc.send :join "#Mezzano")
        (irc.send :join "#vis-editor")
;;;     (irc.send :join "#Beagle")
        (sleep 3)
        (irc.send :whois "sts-q")
        (irc.send :stats "u")
        (irc.send :time)
        (irc.send :chan "#Mezzano")
        )


(defun irc.say( viewer )
        "/ Send selected lines to irc."
        (declare (ignore viewer))
        (say / "## " :irc-say)
        (if selection.active?
                (dolist (l (selection.lines))
                        (irc.send :say l)
                        (say / :/irc-say l))
                (error "No active selection found.")))

(defun irc.speak-toggle()
        ""
        (notf irc.*speak-toggle*)
        (say / (if irc.*speak-toggle*  "Printing"  "No printing of") "messages to log."))

(defun irc.autostart-toggle()
        ""
        (notf irc.*autostart-toggle*)
        (say / (if irc.*autostart-toggle*  "Do"  "No") "autostart of IRC Client."))

(defun irc.clear()
        "--  // clear recorded messages and links."
        (setf irc.*tt*    nil
              irc.*links* nil)
        (irc.said))

(defun irc.connectedp()
        "-- b  // Are we still connected?"
        (mezzano.irc-client:connectedp irc.*client*))


(defun irc.disconnect()
        ""
;;;     (irc.send :part mess-part)
        (irc.send :quit irc.*mess-quit*))

(defun irc.start()
        "--  // Start IRC Client: spawn, connect, join."
        (irc.spawn)
        (sleep 3)
        (irc.connect)
        (sleep 3)
        (irc.join)
        (say / :now :irc-client-started)
        )

(defun irc.stop()
       "--  // Stop IRC Client: disconnect, quit, close gui."
       (irc.disconnect)
       )


;;;===========================================================================
;;; :section:           another run on keyboard layout


(defun print-s3-keymap()
        (say / "")
        (say / "  |----  std-qwerty-keymap  -----------------------------------------------|      ")
        (say / "; |      ~   !   `   @   $   %   &   /   \\\   |   ?   {   }                 |      ")
        (say / "  |            '   S   U   G   Q   V   P   C   D   W   <   >               |      ")
        (say / "  |             A   O   I   L   F   B   T   E   N   R   *   =              |      ")
        (say / "  |               X   Y   Z   H   J   K   M   ;   :   _                    |      ")
        (say / "  |                                                                        |      ")
        (say / "; |      ^   1   2   3   4   5   6   7   8   9   0   (   )                 |    ")
        (say / "  |            q   w   e   r   t   z   u   i   o   p   ü   +               |     ")
        (say / "  |             a   s   d   F   g   h   J   k   l   ö   ä   #              |      ")
        (say / "  |               y   x   c   v   b   n   m   ,   .   -                    |      ")
        (say / "  |------------------------------------------------------------------------|      ")
        (say / "                                                                                  ")
        (say / "  |----  94-characters-on-xx-keys, second run  ----------------------------|      ")
        (say / "; |      ~   !   `   @   $   %   &   /   \\\   |   ?   {   }                 |      ")
        (say / "  |            '   S   U   G   Q   V   P   C   D   W   <   >               |      ")
        (say / "  |             A   O   I   L   F   B   T   E   N   R   *   =              |      ")
        (say / "  |               X   Y   Z   H   J   K   M   ;   :   _                    |      ")
        (say / "  |                                                                        |      ")
        (say / "; |      ^   1   2   3   4   5   6   7   8   9   0   (   )                 |    ")
        (say / "  |            \"   s   u   g   q   v   p   c   d   w   [   ]               |     ")
        (say / "  |             a   o   i   L   f   b   T   e   n   r   +   #              |      ")
        (say / "  |               x   y   z   h   j   k   m   ,   .   -                    |      ")
        (say / "  |------------------------------------------------------------------------|      ")
        (say / "                                                                                  ")
        (say / "  |----  94-characters-on-THREE-rows  -------------------------------------|      ")
        (say / "  |            '   S   U   G   Q   V   P   C   D   W                       |      ")
        (say / "  |             A   O   I   L   F   B   T   E   N   R   *                  |      ")
        (say / "  |               Y   X   Z   H   J   K   M   ;   :   _                    |      ")
        (say / "  |                                                                        |      ")
        (say / "  |            \"   s   u   g   q   v   p   c   d   w                       |     ")
        (say / "  |             a   o   i   L   f   b   T   e   n   r   +                  |      ")
        (say / "  |               y   x   z   h   j   k   m   ,   .   -                    |      ")
        (say / "  |                                                                        |      ")
        (say / "  |            ^   ~   @   7   8   9   {   [   ]   }                       |     ")
        (say / "  |             !   ?   &   4   5   6   .   (   )   <   >                  |      ")
        (say / "  |               \\   |   /   1   2   3   0   °   =   #                    |      ")
        (say / "  |------------------------------------------------------------------------|      ")
        (say / "                                                                                  ")
        (say / "  |----  94-characters-on-TWO-rows  ---------------------------------------|      ")
        (say / "  |            ^   ~   @   7   8   9   {   [   ]   }                       |      ")
        (say / "  |             !   ?   &   4   5   6   (   )   <   >   *                  |      ")
        (say / "  |               \\/ |   0:  1,  2.  3-  =#  ;   :   _                     |      ")
        (say / "  |                                                                        |      ")
        (say / "  |            \"'  s   u   g   q   v   p   c   d   w                       |     ")
        (say / "  |             a   o   i   L   f   b   T   e   n   r   +                  |      ")
        (say / "  |               y   x   z   h   j   k   m   ,   .   -                    |      ")
        (say / "  |------------------------------------------------------------------------|      ")
        (say / "                                                                                  ")
        (say / "  |----  94-characters-on-TWO-rows  ---------------------------------------|      ")
        (say / "  |            '   ^   ~   @   °   °   {   [   ]   }                       |      ")
        (say / "  |             !   ?   &   °   °   °   (   )   <   >   *                  |      ")
        (say / "  |               |   \\   /   °   °   #   =   ;   :   _                    |      ")
        (say / "  |                                                                        |      ")
        (say / "  |            \"   s   u   g   q   v   p   c   d   w                       |     ")
        (say / "  |             a   o   i   L   f   b   T   e   n   r   +                  |      ")
        (say / "  |               y   x   z   h   j   k   m   ,   .   -                    |      ")
        (say / "  |------------------------------------------------------------------------|      ")
        (say / "                                                                                  ")
        )
;         +*#=({)}1234567890^[]~!`@$%&/\|?<>    34
;         +*#=({)}1234567890^[]~!`@  &/\|?<>    32
;         +*#=({)}          ^[]~!`@  &/\|?<>    22
;         +*#= { }          ^[]~!`@  &/\|?<>    20
;         +*#= { }          ^[]~! @  &/\|?<>    19         ^~!@&?\|/     +*#={}[]<>     9  10


;;;===========================================================================
;;; :section:           load

(defun load-file-or-selection( viewer intro )
  "s -- // intro s gets before loading prepended to selection, not to file..."
  (let* ((silentp  (viewer.views-bookmarks-file-p viewer))
         (snip (cond
                 (selection.active?
                                (concat
                                        (format nil "~&;---  snippet  -----------------~%")
                                        intro   (format nil "~%")
                                        (format-text (selection.lines))
                                        (format nil "~&;---  snippet  -----------------~%")
                                        ))

                 (t             (say /"loading" path~)
                                (text-get viewer))))
        )
        (when (and selection.active? (not silentp))
                (format t "~&## LOAD-FILE-OR-SELECTION~{~&~A~}~&" (selection.lines)))
        (selection.clear)
        (rewrite-screen viewer)
        (with-input-from-string (s snip)   (mezzano.internals::load-lisp-source s))
        (when (not silentp)   (say / ";---  loaded." /))))


(defun load-current-file( viewer )
        (cond
          (selection.active?   (say / "Eval selection with B2, please."))
          (t   (text-save viewer)
               (load-file-or-selection viewer ""))))


;;;===========================================================================
;;; :section:           filer

(defun filer-spawn( initial-path )
  (multiple-value-bind (w h) (mezzano.gui.compositor::screen-dimensions)
  w ; ignore
  (mezzano.supervisor:make-thread
        (lambda () (mezzano.gui.filer::main (merge-pathnames initial-path) 780 (- h 10)))
        :name "pete-spawned-filer"
        :initial-bindings `(
                (*terminal-io*          ,logstream.*stream*)
                (*standard-input*       ,(make-synonym-stream '*terminal-io*))
                (*standard-output*      ,(make-synonym-stream '*terminal-io*))
                (*error-output*         ,(make-synonym-stream '*terminal-io*))
                (*trace-output*         ,(make-synonym-stream '*terminal-io*))
                (*debug-io*             ,(make-synonym-stream '*terminal-io*))
                (*query-io*             ,(make-synonym-stream '*terminal-io*))))))


;;;===========================================================================
;;; :section:           iterfile
(defun iter( viewer )
;;;     (save viewer)
        (save-all)
        (file.write   (concat +current-iterdir+ "iterfile")   "")
;;;     (say / "iterfile " +current-iterdir+)
        )


;;;===========================================================================
;;; :section:           pub
(defun pub( viewer )
        "Publish viewer or selection to pet-pub.txt"
        (let ((pet-pub.txt (path.make +load-path+ "pet-pub.txt"))
              (viewer  (viewer.with-name viewer)))
           (if selection.active?
                (progn
                   (file.write-lines (pet-pub.txt (selection.lines)))
                   (selection.clear))
                (progn  (save viewer)
                        (file.copy path~ pet-pub.txt)
                        ))))

;;;===========================================================================


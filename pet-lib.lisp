;;;===========================================================================

;;; :file:   pet-lib.lisp               Library definitions, not specific for Pet

;;;===========================================================================
;;;     remember
;;;     never stop stopping to smile at the flowers ~
;;;     ( Noa[h], distinctly.pink )
;;;===========================================================================

(defpackage :pet.lib
        (:use :cl
              :uiop)
        (:import-from :alexandria   :with-gensyms)
        (:export
                #:return-when
                #:return-unless
                #:notf
                #:get-keymap-nickname
                #:+std-window-line-length+
                #:today-now
                #:lf
                #:err
                #:ROADWORK
                #:say-not-found

                #:as-colour

                #:say

                #:etimes.clear
                #:etimes.take
                #:etimes.dump
                #:etimes.last

                #:join
                #:unjoin
                #:rolldown
                #:rollup
                #:take
                #:drop
                #:setf-to-maxlen
                #:transpose
                #:flatten
                #:flattenrec

                #:concat
                #:spaces
                #:char-at=
                #:idxin
                #:idxin0
                #:wordchrp
                #:phrasechrp
                #:at-word-start-p
                #:toggle-char-case-fn
                #:headseqp
                #:empty-string
                #:empty-line
                #:chunks-1
                #:chunks
                #:justify-right
                #:justify-left
                #:despace
                #:despace-left
                #:despace-1
                #:indentation
                #:only-whitespace-p

                #:text.copy
                #:text.from-byte-array
                #:format-markdown-paragraph
                #:untabify
                #:text.remove-trailing-spaces

                #:*dir-nicks*
                #:dirnick.directories
                #:dirnick.dir
                #:dirnick.nick
                #:dirnick.nick-read
                #:dirnick.nick-show

                #:path.make
                #:path.=
                #:path.host
                #:path.path
                #:path.base
                #:path.ext
                #:path.file
                #:path.nodes
                #:path.__man
                #:path.__demo

                #:file.type
                #:file.type-simplified
                #:file.comment-char
                #:file.copy
                #:file.read-bytes
                #:file.read-ascii-lines
                #:file.read-lines
                #:file.write-bytes
                #:file.write
                #:file.write-lines
                #:file.read-iso-8859-1-lines
                #:file.read-http-page-bytes
                #:file.read-http-page-lines

                #:dd?
                #:ddw
                #:dda
                #:dda+
                #:dda.

                ))


(in-package :pet.lib)

(print :pet.lib)

;;;===========================================================================
;;; :section:           diversa

(defmacro  tst( form )
        `(format t "~&~A ~12,12T--> ~A" ',form  ,form))


(defmacro return-when( test &optional value )
        "// return value from block nil if test passes"
        `(when ,test  (return ,value))
        )

(defmacro return-unless( test &optional value )
        "// return value from block nil if test fails"
        `(unless ,test  (return ,value))
        )

;;; found in phoe's toolbox
(defmacro notf( place )
        "// toggle boolean value"
        `(setf ,place (not ,place)))

(defun get-keymap-nickname()
        (let ((name     (mezzano.gui.compositor:name   mezzano.gui.compositor::*current-keymap*)))
           (cond
             ((equal name "En-GB")              :GB)
             ((equal name "En-US")              :US)
             ((equal name "No-BK")              :BK)
             ((equal name "PanCyr")             :PC)
             ((equal name "De")                 :DE)
             ((equal name "ES-ESP")             :ES)
             ((equal name "dvorak-writer")      :DW)
             ((equal name "dvorak-programmer")  :DP)
             ((equal name "dvorak-mouse")       :DM)
             ((equal name "sts-30")             :S3)
             )))


(defparameter +std-window-line-length+  112     "repl, pet")


(defun today-now()
  "Return a string with date and time."
  (multiple-value-bind (sec min h day month year)
      (get-decoded-time)
    (format nil "~d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d" year month day h min sec)))

(defun lf()   (terpri))

(defun err( message &rest values )
        "Call 'error' with formated message and values."
        (if values
                (error (format nil "~&~A ~{~&~A~}" message values))
                (error (format nil "~&~A" message))))

(defun ROADWORK( here )        (err :ROADWORK here))
(defun say-not-found( text )   (format t "~&#######  not found: < ~A >" text))


;;;---------------------------------------------------------------------------
;;; :section:           say


(defparameter say.*time*                (get-universal-time)            "init saytime")

;;; say.do
;;; st l -- // print to stream or t, not to nil.
;;;            Makes a separate format instruction from each argument.
;;;            Returns last `otherwise`-value.
(defmacro say.do( stream  something )
        (with-gensyms (v)
           `(let (,v)
                ,@(map 'list (lambda( x )
                      (case x
                        ('/         `(format ,stream "~&"))
                        ('//        `(format ,stream "~2&"))
                        (:-         `(format ,stream "~&------------~%"))
                        (:>         `(format ,stream "~12,12T"))
                        (:now       `(format ,stream "~a " (today-now)))
                        (:time      `(let ((now (get-universal-time)))  (format ,stream "<~3d> " (- now say.*time*))  (setf say.*time* now)))
                        (:?         `(format ,stream "~&   options:   /  //  :-  :>  :now  :time  :? "))
                        (otherwise  `(progn (setf ,v ,x) (format ,stream "~a " ,v)))
                        ))
                      something)
                ,v)
                ))


(defmacro say ( &rest something )
        `(say.do t ,something)
        )


;;;---------------------------------------------------------------------------
;;; :section:           etimes

(defvar *etimes*                        nil     "Taken execution times. [milliseconds]")
(defparameter etimes.last               -1      "Last taken etime")

(defun etimes.clear()   (setf *etimes* nil))

(defmacro etimes.take( name &rest body )
        "Take execution time. [milliseconds]"
        `(let ((etime-t1    (mezzano.supervisor::get-internal-run-time))
               (etime-res   (progn ,@body)))
           (push   (list ,name (truncate (- (mezzano.supervisor::get-internal-run-time) etime-t1) 1000))
                         *etimes*)
           (setf etimes.last (second (car *etimes*)))
           etime-res))

(defun etimes.dump()
        (say (format nil "~{~{~12,a   ~8d~}~%~}" (reverse *etimes*)))
        (say   :> (length *etimes*) "etimes taken.")
        (values))


;;;---------------------------------------------------------------------------
;;; :section:           list helper funs

(defun join( l x )
        "Append x to tail of l, lists only."
        (concatenate 'list l (list x)))

(defun unjoin( seq )
        "l -- l x  // return l with last item removed and last item"
        (let ((last (1- (length seq))))
                (when (eql -1 last)     (err "unjoin: sequence is empty"))
                (values (subseq seq 0 last)  (elt seq last))))

(defun rolldown( i seq )
        "i l -- l  // i times roll **first item down** to tail of list"
        (dotimes (j i)  (setf seq (join (cdr seq) (car seq))))
        seq)

(defun rollup( i seq )
        "i l -- l  // i times roll **last item up** to front of list"
        (dotimes (j i)  (setf seq (multiple-value-bind ( l x ) (unjoin seq)  (cons x l))))
        seq)

(defun take( i l )
        "Take first i elements of l. if i<0 return emptyness."
        (subseq l 0 (min (max 0 i) (length l))))

(defun drop( i l )
        "Drop first i elements l. if i<0 return l."
        (subseq l (min (max 0 i) (length l))))

(defmacro setf-to-maxlen( i l )
        "setf list|string|seq  to max i elements of length"
        `(setf ,l   (take ,i ,l)))

(defun transpose( ll )   (apply #'mapcar #'list ll))


(defun flatten( l )
        (cond   ((null l)               nil)
                ((listp (car l))        (append (car l) (flatten (cdr l))))
                (t                      (cons   (car l) (flatten (cdr l))))))

(defun flattenrec( list )
          (cond ((null list)            nil)
                ((consp (car list))     (nconc (flattenrec (car list)) (flattenrec (cdr list))))
                (t                      (cons   (car list)   (flattenrec (cdr list))))))


;;;---------------------------------------------------------------------------
;;; :section:           string helper funs

(defun concat( &rest strings )  (apply #'concatenate 'string strings))
(defun spaces( i )              (make-sequence 'string (max 0 i) :initial-element #\space))


(defun char-at=( s i c ) (char= (char s i) c))
(defun idxin(  s i )  (min     (length s)  i))
(defun idxin0( s i )  (min (1- (length s)) i))


(defun wordchrp( c &key also )
  "c s -- b"
  (or (alphanumericp c)                                         ; not wordchar: '.-'
      (member c '(      #\_     ))
      (position c also)))


(defun phrasechrp( c )
        (or (alphanumericp c)
            (member c '(  #\_ #\- #\. #\+ #\/))))


(defun at-word-start-p( line x )
        (and    (> x 0)
                (not (>= x (length line)))
                (wordchrp (char line x))
                (not(wordchrp (char line (1- x))))
                ))


(defun toggle-char-case-fn( word )
  "s -- s"
  (cond
    ((zerop (length word))              word)
    ((not(alpha-char-p (char word 0)))  word)
    ((upper-case-p (char word 0))       (string-downcase word))
    (t                                  (string-upcase word))))


(defun headseqp( h s )  (equal h (take (length h) s)))


(defun empty-string()  (make-sequence 'string 0))
(defun empty-line()    (list(make-sequence 'string 0)))


(defun chunks-1( string chr )
  "s c -- sl  // split string at chr (split str at LF to lines)  # chunks"
  (let ((sl nil)
        (i 0)
        (j 0))
    (loop for c across string
          do (if (eql c chr)
                 (progn (push (subseq string i j) sl)
                        (incf j)
                        (setf i j))
                 (incf j)))
    (push (subseq string i j) sl)
    (reverse sl)))


(defun chunks( string )
        "Split string at spaces. (split line at whitespace)  # chunks-1"
        (let ((nl      nil)
              (len     (length string))
              (start   0)
              (i       0))
        (flet ((done()          (eql len i)))
        (flet ((atspace()       (eql #\space (elt string i))))
        (flet ((skip-spaces()   (loop (return-when (or (done) (not(atspace))))   (incf i))))
        (flet ((skip-word()     (loop (return-when (or (done)     (atspace)))    (incf i))))
        (loop
                (return-when (done))
                (skip-spaces)   (setf start i)
                (skip-word)     (when (< start i)   (push (subseq string  start i) nl)))
        (reverse nl)))))))


(defun justify-right( width str )       (concat   (spaces (- width (length str)))   str))
(defun justify-left(  width str )       (concat   str   (spaces (- width (length str)))))


(defun despace(   s )                   (string-right-trim " " s))
(defun despace-left( s )                (string-left-trim  " " s))
(defun despace-1( s )                   (concat (string-right-trim " " s) " "))
(defun indentation( s )
        "s -- i"
        (let ((i (position-if-not (lambda( c ) (char= c #\space)) s)))
            (if i i 0)))


(defun only-whitespace-p( str )         (not (find #\space str :test-not #'eql)))

;;;---------------------------------------------------------------------------
;;; :section:           text

(defun text.copy( lines )
        "ls -- ls  // make a fresh copy of both, the list and the strings it holds"
        (map 'list #'copy-seq lines))


(defun text.from-byte-array( bytes )
        "ba -- text  // return list of strings from array of ascii characters"
        (let* ((len     (length bytes))
               (lines   nil)
               (start   0)
               (i       0)
               (c       0))
        (flet ((get-line( start end )   (map 'string #'code-char (subseq bytes start end))))
        (flet ((push-line( start end )  (push (get-line start end)  lines)))
        (flet ((step()                  (incf i)))
        (flet ((c()                     (setf c (elt bytes i))))
        (loop
                (return-when (eql len i))
                (c)
                (cond
                  ((eql 10 c)   (push-line start i)  (step)  (setf start i))
                  ((eql 13 c)   (ROADWORK :file.read-lines/chr-13))
                  (t            (step))
                  ))
        (when (< start i)   (push-line start i))
        (reverse lines)))))))


(defun format-markdown-paragraph( columns lines &key indent)
        "i sl -- sl  // format lines to length of maximal i chars."
        (labels ((fitp( clen wlen )     (<= (+ clen 1 wlen) columns))
                 (++( l w )             (concat l (if l " " "") w))
                 (loops( nt l s)
                        (let* ((nxtspc  (position #\space s))
                               (nxtword (if nxtspc (subseq s 0 nxtspc)                  s))
                               (s       (if nxtspc (despace-left (subseq s nxtspc))    nil))
                               (clen    (length l))
                               (wlen    (length nxtword))
                              )
                        (cond
                         ((not s)               (reverse  (if (fitp clen wlen)
                                                                (cons (++ l nxtword) nt)
                                                                (cons nxtword (cons l nt)))))
                         ((fitp clen wlen)      (loops nt           (++ l nxtword)  s))
                         (t                     (loops (cons l nt)  nxtword         s))
                         )))
                 (fmt( lines )
                        (if lines
                                 (loops nil nil (despace-left (format nil "~{~a ~}" lines)))
                                 nil)))
        (let ((ols   lines)  ; old incomming lines
              (cls   nil)    ; current paragraphs lines
              (nls   nil)    ; new lines ==  result
              )
            (loop
                (return-unless ols)
                (cond
                  ((emptyp (car ols))
                        (setf nls   (concatenate 'list nls (fmt (reverse cls)) (empty-line)))
                        (pop ols)
                        (setf cls nil))
                  (t    (push (car ols) cls)
                        (pop  ols))))
            (let ((res (concatenate 'list nls (fmt (reverse cls)))))
                (if indent
                        (map 'list (lambda( l ) (concat (spaces indent) l)) res)
                        res)
                ))))


(defun untabify( lines &key (tab-width 8))
        "sl [i] -- sl  // expand tab-characters to spaces"
        (flet ((expand-line( l )
                        (let ((len       (length l))
                              (snippets  nil)
                              (pos       0)
                              (newlen    0)
                              (r         0))
                        (loop
                                (return-when (>= pos len))
                                (let ((tpos (position #\tab l :start pos)))
                                   (cond
                                     (tpos      (push (subseq l pos tpos) snippets)
                                                (incf newlen (- tpos pos))
                                                (setf pos (1+ tpos))
                                                (setf r   (mod newlen tab-width))
                                                (setf r   (if (zerop r)   8   (- tab-width r)))
                                                (push (spaces r) snippets)
                                                (incf newlen r))
                                     (t         (push (subseq l pos) snippets)
                                                (setf pos len)))))
                        (apply #'concatenate 'string (reverse snippets)))))     ; call-arguments-limit: 500
        (map 'list #'expand-line lines)))

(defun __untabify()
        (say / (untabify '( "abc" )))
        (say / (untabify '(  )))
        (say / (untabify '( "abc" "def" "ghi")))
        (say / (untabify '( "aXbXc" "defX" "Xghi")))
        (say / (first (untabify '( "aXbXcXdXeXfXgXhXiX"))))
        (say / (first (untabify '( "a--Xb----Xc---Xd------Xe-------XgXhXiX"))))
        (say / (first (untabify '( "a--Xb----Xc---Xd------..Xf-XgXhXiX"))))
        (say / (first (untabify '( "a--Xb----Xc---Xd------.......Xf-XgXhXiX"))))
        (say / (first (untabify '( "a--Xb----Xc---Xd------........Xf-XgXhXiX"))))
        (say / (first (untabify '( "a--Xb----Xc---Xd------.........XgXhXiX"))))
        (say / (first (untabify '( "a--Xb----XXXXXXXiX"))))
        (say / "0123456789.123456789.123456789.123456789.123456789.123456789.123456789")
        )


(defun text.remove-trailing-spaces( lines )
        "sl -- sl"
        (map 'list #'despace lines))


;;;---------------------------------------------------------------------------
;;; :section:           dirnick

(defvar *dir-nicks*                     nil     "Nicknames for frequently used directories. setfed in cfg.")


(defun dirnick.directories()
        "-- ls  // list of directories"
        (map 'list (lambda( kv ) (second kv))   *dir-nicks*))

(defun dirnick.dir( nick )
        "u -- s|nil  // return directory of keyword nick or nil"
        (let ((kv (assoc nick *dir-nicks*)))
           (if kv (second kv) nil)))

(defun dirnick.nick( dir )
        "s -- u  // return nick of dir as keyword"
        (let ((kv   (find-if (lambda(kv) (equal dir (second kv)))   *dir-nicks*)))
            (if kv  (car kv)  nil)))

(defun dirnick.nick-read( str )         "s -- u"   (read-from-string (concat ":" str)))
(defun dirnick.nick-show( nick )        "u -- s"   (if nick (string-downcase (format nil "~a" nick))  nil))



;;;---------------------------------------------------------------------------
;;; :section:           path

(defun path.__man()
""
(say // "## ===========  topic path.

file mezzano/file/fs.lisp
pckg mezzano.file-system
type path       s                                       'REMOTE:/home/me/my-file.md'
type pathname   mezzano.file-system::pathname           slots: host device directory name type version
type host

pathname                    string|pathname|file-stream|synonym-stream --> class-instance pathname
make-pathname               from &key-args: :host :device :directory :type :version
pathnamep
pathnames-equal

pathname-host               #<Mezzano.File.System.Remote::Remote-File-Host 'REMOTE' 192.168...:port ...>
pathname-device             nil
pathname-directory          (:absolut 'home' 'me' )
pathname-name               'my-file'
pathname-type               'md'
pathname-version            nil

namestring                  'REMOTE:/home/me/my-file.md'
host-namestring             'REMOTE'
directory-namestring        '/home/me/
file-namestring             'my-file.md'

merge-pathnames             take properties from one to the other pathname

more: fs.lisp cl-fad uiop   path.__demo
")(values))


(defun path.make( path file &optional ext)
        "s|u s [s|u] -- s  // make path"
        (let* ((a     nil)
               (path  (cond
                        ((stringp path)                                                 path)
                        ((not path)                                                     "")
                        ((and (symbolp path) (setf a (assoc path *dir-nicks*)))         (second a))
                        (t                                     (err "path.make: strange path:" path))))
               (ext   (cond
                        ((stringp ext)          (concat "." ext))
                        ((not ext)              "")
                        ((symbolp ext)          (concat "." (string-downcase (string ext))))
                        (t                      (err "path.make: strange ext:" ext))))
                        )
           (concat path file ext)))

(defun path.=( path1 path2 )    "s s -- b"                                      (equal path1 path2))
(defun path.host( path )        "s -- s      // REMOTE|LOCAL"                   (host-namestring  path))
(defun path.path( path )        "s -- s      // file path"                      (directory-namestring path))
(defun path.base( path )        "s -- s      // file base-name."                (pathname-name path))
(defun path.ext( path )         "s -- s|nil  // file extension."                (pathname-type path))
(defun path.file( path )        "s -- s      // file name"                      (file-namestring path))
(defun path.nodes( path )       "s -- l"                                        (pathname-directory path))


(defun path.__demo()
     (let ((p "REMOTE:/home/me/my-file.md"))
        (tst (path.host p))
        (tst (path.path p))
        (tst (path.file p))
        (tst (path.base p))
        (tst (path.ext p))
        (tst (path.nodes p))
        (tst (path.make :mezzano "my-file" :md))
        (tst (path.make "me/" "my-file" ))
        (tst (path.make "" "my-file" ))
        (tst (path.make nil "my-file"))
        )
)


;;;---------------------------------------------------------------------------
;;; :section:           file access

(defun file.type( path )
        "p -- u|nil  // return file extension as keyword symbol {path.ext}"
        (let ((e (path.ext path)))
           (if e   (intern (string-upcase e) :keyword)   nil)))

(defun file.type-simplified( path )
        (case (file.type path)
          (:lisp        :lisp)
          (:rabbit      :rabbit)
          (:md          :md)                    ; markdown
          (:sit         :md)                    ; "house intern" markdown, deprecated
          (:txt         :md)                    ; text, works mostly
          (:ml          :ml)                    ; ocaml
          (:mli         :ml)                    ; ocaml
          (:tex         :tex)
          (:pas         :pas)
          (:m           :pas)
          (:obn         :pas)
          (:v           :v)
          (:zig         :v)
          (:c           :v)
          (:go          :v)
          (:lua         :lua)
          (:nelua       :lua)
          (t            nil)
          ))


(defun file.comment-char( path )
        "s -- s  // return line-comment characters of file type"
        (let ((ext (file.type path)))
            (cond
                ((path.= path "/home/heiko/q/mecrisp/forth/basisdefinitions.txt")       "\\")
                ((path.= path "/home/heiko/q/mecrisp/forth/leds-v2.txt")                "\\")
                ((equal ext :lisp)      ";")
                ((equal ext :forth)     "\\")
                ((equal ext :v)         "//")
                ((equal ext :zig)       "//")
                ((equal ext :ml)        "(*")   ; )
                ((equal ext :m)         "(*")   ; )
                ((equal ext :obn)       "(*")   ; )
                ((equal ext :tex)       "%%")
                ((equal ext :pas)       "(*")
                ((equal ext :c)         "//")
                ((equal ext :go)        "//")
                ((equal ext :lua)       "--")
                (t                      ";")
                )))


(defun file.copy( source destination  &optional directory)
        "p p -- // copy file unsigned-byte-8-wise. Overwrite if exists.
                   If given, prepend directory to both."
        (let ((bsize   (* 16 1024))
              (etype   '(unsigned-byte 8)))
           (when directory
                (setf source      (path.make directory source)
                      destination (path.make directory destination)))
           (with-open-file (in  source       :direction :input   :element-type etype)
           (with-open-file (out destination  :direction :output  :element-type etype)
           (loop with buffer = (make-array bsize :element-type etype)
                for size = (read-sequence  buffer in)
                while (plusp size)
                do  (write-sequence buffer out  :end size))))))


(defun file.read-bytes( name  )
        (with-open-file( s name )
        (let ((bytes   (make-array (file-length s)  :element-type '(unsigned-byte 8))))
           (read-sequence bytes s)
           bytes)))

(defun file.read-ascii-lines( name )
        "s -- text"
        (text.from-byte-array (file.read-bytes name)))


(defun file.read-lines( name &key (ascii nil ))
        "p -- text  // name e.g. :  REMOTE:/some-path/my-file.type"
        (if ascii
                (file.read-ascii-lines name)                                     ; read ascii
                (with-open-file (s name)                                         ; read utf-8
                    (if (listen s)
                        (do ((l     (read-line s)   (read-line s nil 'eof))
                             (text  nil             (push l text)))
                           ((eq l 'eof)
                               (reverse text)))
                        (list "")))))                                            ; file was empty

(defun file.write-bytes( name bytes )
        "s ba -- // write byte-array"
        (with-open-file( s name   :direction :output)
                (write-sequence bytes s)
                ))

(defun file.write( name string )
        "p s -- // write text to file"
        (with-open-file( s name :direction :output)   (princ string s)))


(defun file.write-lines( name text )
        "p text -- // write text to file"
        (let ((text (format nil "~{~a~%~}"  text)))
           (file.write name text)))


(defun file.read-iso-8859-1-lines( name )
        "s -- text  // in order to get"
        (flet ((translate( byte )
                      (case byte
;;;                       (252          (say :translate-u) #\ä)     ; ??? magic happens without this line, too
                          (otherwise    (code-char byte)))))
        (macrolet ((next()        '(setf c (read-byte s nil)))
                   (push-line()   '(push (concatenate 'string (reverse cs)) lines)))
        (let ((lines  nil)
              (cs     nil)
              (c      nil))
        (with-open-file (s name
                         :direction    :input
                         :element-type '(unsigned-byte 8))
                (next)
                (loop
                        (return-when (not c))
                        (cond
                         ((eql 13 c)
                                (push-line)
                                (setf cs nil)
                                (next)
                                (when (eql 10 c) (next)))
                         ((eql 10 c)
                                (push-line)
                                (setf cs nil)
                                (next)
                                (when (eql 13 c) (next)))
                         (t
                                (push (translate c) cs)
                                (next)))))
        (when cs (push-line))
        (reverse lines)))))


(defun file.read-http-page-bytes( host port path )
        "s i s -- ba  // return byte-array"
        (multiple-value-bind ( a b c d body )
                (mezzano.file-system.http::http-request host port path )
            (declare (ignore a b c d))
            body
            ))


(defun file.read-http-page-lines( host port path )
        "s i s -- text  // return lines read from http, ascii"
        (let ((bytes  (file.read-http-page-bytes host port path)))
            (chunks-1 (map 'string  #'code-char   bytes) #\Linefeed)))


;;;-----------------------------------------------------------------------------
;;; :section:           dot documentation

(defun fundoc( name )
  "f --  // print function documentation"
  (say / "fundoc: " name)
  (labels ((docs( k )  (let ((v (documentation name k)))   (when v (say k v /)))))
  (when  (fboundp name)
          (let* ((info  (mezzano.internals::function-debug-info (symbol-function name))))
               (format t "~a~%~A ~A~%"
                    (mezzano.internals::debug-info-source-pathname   info)
                    (mezzano.internals::debug-info-name   info)
                    (let ((ll (mezzano.internals::debug-info-lambda-list   info)))  (if ll ll "()"))
                    )))
  (map 'list #'docs '(function compiler-macro method-combination setf structure type variable))
  (when (boundp name)
      (let ((v (symbol-value name)))
        (cond
          ((integerp v)       (say "integer: " v / ))
          ((stringp  v)       (say "string:  " (take 32 v)"..." / ))
          ((listp    v)       (say "list:    ")  (if v   (say (length v) "elements" )   (say nil)) (say /))
          )))
    t))

(defun package-external-functions-list( package )
  "package -- l  // Retrieves all functions in a package."
  (let ((res (list)))
    (do-external-symbols (sym package)
      (when (and (fboundp sym)
                 (eql (symbol-package sym)
                      (find-package package)))
        (push sym res)))
    res))

(defun package-functions-list( package )
  "package -- l  // Retrieves all functions in a package."
  (let ((res (list)))
    (do-all-symbols (sym package)
      (when (and (fboundp sym)
                 (eql (symbol-package sym)
                      (find-package package)))
        (push sym res)))
    res))


;;;-------------------------------------
(defun dd?()
(princ
"## =======  Dot Documentation  =======

ddw  name               Print documentation of name.
dda  package            Print exported functions of package.
dda+ package            Print all functions of package.
dda. name               Print all functions of topic.
")
           (let* ((ps   '(:pet :pet.lib :stdlib))                       ; find topics in these packages
                  (syms (mapcan #'package-functions-list ps))
                  (syms (map 'list #'symbol-name syms))
                  (syms (remove-if (lambda( s ) (not (find #\. s))) syms))
                  (syms (map 'list (lambda( s ) (subseq s 0 (position #\. s))) syms))
                  (syms (remove-duplicates syms :test #'equal))
                  (syms (sort syms #'string<)))
              (say // "Topics so far in packages" ps ":")
              (format t "~&~{~A~%~}~%" (format-markdown-paragraph
                                                   69
                                                   (list (format nil "~{~A ~}" syms))
                                                   :indent 12))
           (values)))


(defmacro ddw( name )
  "u -- // (ddw fac)   (ddw pet:save)"
  `(fundoc ',name))


(defmacro dda( name )
  "package-name --  // print exported functions of package"
  (say // "## dda" name /)
  `(let* ((fs (package-external-functions-list ',name))
          (fs (sort fs (lambda( a b ) (string< (symbol-name a) (symbol-name b)))))
         )
        (mapcar (lambda( f )(format t "~A~%"  (take (1- +std-window-line-length+) (format nil "~A ~25T~A ~35T\"~A\""
                  f
                  (let ((ll (mezzano.internals::debug-info-lambda-list (mezzano.internals::function-debug-info (symbol-function f)))))  (if ll ll "()"))
                  (let ((ds (mezzano.internals::debug-info-docstring   (mezzano.internals::function-debug-info (symbol-function f)))))  (if ds ds ""  ))
                  ))))
                fs)

        t))


(defmacro dda+( name )
  "package-name --  // print all functions of package"
  (say // "## dda+" name)
  `(let* ((fs (package-functions-list ',name))
          (fs (sort fs (lambda( a b ) (string< (symbol-name a) (symbol-name b)))))
            )
        (mapcar (lambda( f )(format t "~A ~25T~A~%"
                                    f
                  (mezzano.internals::debug-info-lambda-list   (mezzano.internals::function-debug-info (symbol-function f)))))
                fs)
        T
        ))


(defmacro dda.( name )
  "package-name --  // print functions of topic: '.' appended to name"
  (say // "## dda." name)
  `(let* ((fs (apropos-list (read-from-string (concat (symbol-name ',name) "."))))
          (fs (remove-if (lambda(f) (not(fboundp f)))
                        fs))
          (fs (sort fs (lambda( a b ) (string< (symbol-name a) (symbol-name b)))))
            )
        (mapcar (lambda( f )(format t "~A~%"  (take (1- +std-window-line-length+) (format nil "~A ~25T~A ~35T\"~A\""
                  f
                  (let ((ll (mezzano.internals::debug-info-lambda-list (mezzano.internals::function-debug-info (symbol-function f)))))  (if ll ll "()"))
                  (let ((ds (mezzano.internals::debug-info-docstring   (mezzano.internals::function-debug-info (symbol-function f)))))  (if ds ds ""  ))
                  ))))
                fs)
        T
        ))


;;;===========================================================================

(provide 'pet.lib)

;;;===========================================================================


;;;===========================================================================

;;; :file:   pet-colours.lisp           colours: rgb hsl hwb

;;;===========================================================================

(in-package :pet.lib)

(print :pet.lib/colours)

;;;===========================================================================
;;; Mezzano colour:
;;;     (deftype colour() '(unsigned byte 32)     ( alpha red green blue , each 8 bits)
;;;===========================================================================

;;;     http://www.colors.commutercreative.com/
;;;     https://drafts.csswg.org/css-color/#the-hwb-notation
;;;     http://www.niwa.nu/2013/05/math-behind-colorspace-conversions-rgb-hsl/
;;;     https://blog.datawrapper.de/beautifulcolors/

;;;===========================================================================

;;; :section:           conversions

;;;     function hueToRgb(t1, t2, hue) {
;;;       if(hue < 0) hue += 6;
;;;       if(hue >= 6) hue -= 6;
;;;
;;;       if(hue < 1) return (t2 - t1) * hue + t1;
;;;       else if(hue < 3) return t2;
;;;       else if(hue < 4) return (t2 - t1) * (4 - hue) + t1;
;;;       else return t1;
;;;     }
;;;

;;;     function hslToRgb(hue, sat, light) {                    ; hue: [0,6)  sat: [0,1]  light: [0,1]
;;;       if( light <= .5 ) {
;;;         var t2 = light * (sat + 1);
;;;       } else {
;;;         var t2 = light + sat - (light * sat);
;;;       }
;;;       var t1 = light * 2 - t2;
;;;       var r = hueToRgb(t1, t2, hue + 2);
;;;       var g = hueToRgb(t1, t2, hue);
;;;       var b = hueToRgb(t1, t2, hue - 2);
;;;       return [r,g,b];
;;;     }
         
(defun hsl/norm-to-rgb( hue sat light )
    (flet ((hueToRgb( t1 t2 hue )
                (when (<  hue 0) (setf hue (+ hue 6)))
                (when (>= hue 6) (setf hue (- hue 6)))
                (cond
                 ((< hue 1)     (+   (*  (- t2 t1) hue )       t1))
                 ((< hue 3)     t2)
                 ((< hue 4)     (+   (*  (- t2 t1) (- 4 hue))  t1))
                 (t             t1))
                 ))
    (let* ((t2   (if (<= light 0.5)
                        (* light (1+ sat))
                        (-  (+ light sat)  (* light sat))))
           (t1   (- (* light 2) t2))
           (r    (hueToRgb t1 t2 (+ hue 2)))
           (g    (hueToRgb t1 t2 hue))
           (b    (huetoRgb t1 t2 (- hue 2)))
           )
        (values r g b))))


;;;     function hwbToRgb(hue, white, black) {
;;;       var rgb = hslToRgb(hue, 1, .5);
;;;       for(var i = 0; i < 3; i++) {
;;;         rgb[i] *= (1 - white - black);
;;;         rgb[i] += white;
;;;       }
;;;       return rgb;
;;;     }
;;;
(defun hwb/norm-to-rgb( hue white black )
        "white,black: [0,1], sum<=1"
        (multiple-value-bind (r g b) (hsl/norm-to-rgb hue 1  0.5)
                (setf r (* r (- 1 white black)))
                (setf r (+ r white))
                (setf g (* g (- 1 white black)))
                (setf g (+ g white))
                (setf b (* b (- 1 white black)))
                (setf b (+ b white))
                (values r g b)
                ))
        
        
(defun rgb-to-hsl( r g b )
        "i i i -- i i i  // r g b: 0..255   h:0..360   s l:0..100"
        (let* ((r (/ r 255))
               (g (/ g 255))
               (b (/ b 255))
               (min (min r g b))
               (max (max r g b))
               (l   (/ (+ max min) 2))
               (grayp  (eql max min))
               (s       (cond
                          (grayp        0)
                          ((< l 0.5)    (/ (- max min) (+ max min)))
                          (t            (/ (- max min) (- 2 max min)))
                          ))
               (h       (cond
                          (grayp        0)
                          ((eql r max)       (/ (- g b) (- max min)))
                          ((eql g max)  (+ 2 (/ (- b r) (- max min))))
                          ((eql b max)  (+ 4 (/ (- r g) (- max min))))
                          ))
                )
                (values (round(* 60 h)) (round (* 100 s)) (round (* 100 l)))))
        

;;;---------------------------------------------------------------------------

(defun colour-to-rgb( value )
        (values  
                (mezzano.gui:colour-red-as-octet value)
                (mezzano.gui:colour-green-as-octet value)
                (mezzano.gui:colour-blue-as-octet value)
                ))
                
(defun hsl-to-rgb( h s l )
        "i i i -- i i i  // [0-360] [0-100] [0-100]  -->  rgb: [0-255];   120 100 50 --> 0 255 0"
        (multiple-value-bind (r g b) (hsl/norm-to-rgb (/ h 60) (/ s 100) (/ l 100))
                (values (round(* 255 r)) (round (* 255 g)) (round (* 255 b)))))
                
(defun hwb-to-rgb( h w b )
        "hue white black to red green blue"
        (when (> (+ w b) 100)   (error (format nil "hwb-to-rgb: w+b > 100: ~a ~a ~a" h w b)))
        (multiple-value-bind( r g b ) (hwb/norm-to-rgb (/ h 60) (/ w 100) (/ b 100))
                (values (round(* 255 r)) (round (* 255 g)) (round (* 255 b)))))


(defun make-colour-from-hsl( h s l &optional (opacity 255))
        (multiple-value-bind (r g b) (hsl-to-rgb h s l)
                (mezzano.gui:make-colour-from-octets r g b opacity)))
                
(defun make-colour-from-hwb( hue white black &optional (opacity 255))
        "i i i [i] -- i  // hue:0..360   white,black:0..100
        to (deftype colour() '(unsigned byte 32))
        error if (white + black) > 100"
        (multiple-value-bind ( r g b ) (hwb-to-rgb hue white black)
                (mezzano.gui:make-colour-from-octets r g b opacity)))
        

;;;---------------------------------------------------------------------------
;;; :section:           named colours

(defparameter *named-colours*
        '(
;;; CSS colours (:name                  value           rgb                     hsl)
                (:aliceblue             #xf0f8ff         240 248 255            208 100 97)
                (:antiquewhite          #xfaebd7         250 235 215)
                (:aqua                  #x00ffff         0 255 255)
                (:aquamarine            #x7fffd4         127 255 212)
                (:azure                 #xf0ffff         240 255 255)
                (:beige                 #xf5f5dc         245 245 220)
                (:bisque                #xffe4c4         255 228 196)
                (:black                 #x000000         0 0 0)
                (:blanchedalmond        #xffebcd         255 235 205)
                (:blue                  #x0000ff         0 0 255)
                (:blueviolet            #x8a2be2         138 43 226)
                (:brown                 #xa52a2a         165 42 42)
                (:burlywood             #xdeb887         222 184 135)
                (:cadetblue             #x5f9ea0         95 158 160)
                (:chartreuse            #x7fff00         127 255 0)
                (:chocolate             #xd2691e         210 105 30)
                (:coral                 #xff7f50         255 127 80)
                (:cornflowerblue        #x6495ed         100 149 237            219 79 66)
                (:cornsilk              #xfff8dc         255 248 220)
                (:crimson               #xdc143c         220 20 60)
                (:cyan                  #x00ffff         0 255 255)
                (:darkblue              #x00008b         0 0 139)
                (:darkcyan              #x008b8b         0 139 139)
                (:darkgoldenrod         #xb8860b         184 134 11)
                (:darkgray              #xa9a9a9         169 169 169)
                (:darkgreen             #x006400         0 100 0)
                (:darkgrey              #xa9a9a9         169 169 169)
                (:darkkhaki             #xbdb76b         189 183 107)
                (:darkmagenta           #x8b008b         139 0 139)
                (:darkolivegreen        #x556b2f         85 107 47)
                (:darkorange            #xff8c00         255 140 0)
                (:darkorchid            #x9932cc         153 50 204)
                (:darkred               #x8b0000         139 0 0)
                (:darksalmon            #xe9967a         233 150 122)
                (:darkseagreen          #x8fbc8f         143 188 143)
                (:darkslateblue         #x483d8b         72 61 139)
                (:darkslategray         #x2f4f4f         47 79 79)
                (:darkslategrey         #x2f4f4f         47 79 79)
                (:darkturquoise         #x00ced1         0 206 209)
                (:darkviolet            #x9400d3         148 0 211)
                (:deeppink              #xff1493         255 20 147)
                (:deepskyblue           #x00bfff         0 191 255)
                (:dimgray               #x696969         105 105 105)
                (:dimgrey               #x696969         105 105 105)
                (:dodgerblue            #x1e90ff         30 144 255)
                (:firebrick             #xb22222         178 34 34)
                (:floralwhite           #xfffaf0         255 250 240)
                (:forestgreen           #x228b22         34 139 34)
                (:fuchsia               #xff00ff         255 0 255)
                (:gainsboro             #xdcdcdc         220 220 220)
                (:ghostwhite            #xf8f8ff         248 248 255)
                (:gold                  #xffd700         255 215 0)
                (:goldenrod             #xdaa520         218 165 32             43 74 49)
                (:gray                  #x808080         128 128 128)
                (:green                 #x008000         0 128 0)
                (:greenyellow           #xadff2f         173 255 47)
                (:grey                  #x808080         128 128 128)
                (:honeydew              #xf0fff0         240 255 240)
                (:hotpink               #xff69b4         255 105 180)
                (:indianred             #xcd5c5c         205 92 92)
                (:indigo                #x4b0082         75 0 130)
                (:ivory                 #xfffff0         255 255 240)
                (:khaki                 #xf0e68c         240 230 140)
                (:lavender              #xe6e6fa         230 230 250)
                (:lavenderblush         #xfff0f5         255 240 245)
                (:lawngreen             #x7cfc00         124 252 0)
                (:lemonchiffon          #xfffacd         255 250 205)
                (:lightblue             #xadd8e6         173 216 230)
                (:lightcoral            #xf08080         240 128 128)
                (:lightcyan             #xe0ffff         224 255 255)
                (:lightgoldenrodyellow  #xfafad2         250 250 210)
                (:lightgray             #xd3d3d3         211 211 211)
                (:lightgreen            #x90ee90         144 238 144)
                (:lightgrey             #xd3d3d3         211 211 211)
                (:lightpink             #xffb6c1         255 182 193)
                (:lightsalmon           #xffa07a         255 160 122)
                (:lightseagreen         #x20b2aa         32 178 170)
                (:lightskyblue          #x87cefa         135 206 250)
                (:lightslategray        #x778899         119 136 153)
                (:lightslategrey        #x778899         119 136 153)
                (:lightsteelblue        #xb0c4de         176 196 222)
                (:lightyellow           #xffffe0         255 255 224)
                (:lime                  #x00ff00         0 255 0)
                (:limegreen             #x32cd32         50 205 50)
                (:linen                 #xfaf0e6         250 240 230)
                (:magenta               #xff00ff         255 0 255)
                (:maroon                #x800000         128 0 0)
                (:mediumaquamarine      #x66cdaa         102 205 170)
                (:mediumblue            #x0000cd         0 0 205)
                (:mediumorchid          #xba55d3         186 85 211)
                (:mediumpurple          #x9370db         147 112 219)
                (:mediumseagreen        #x3cb371         60 179 113)
                (:mediumslateblue       #x7b68ee         123 104 238)
                (:mediumspringgreen     #x00fa9a         0 250 154)
                (:mediumturquoise       #x48d1cc         72 209 204)
                (:mediumvioletred       #xc71585         199 21 133)
                (:midnightblue          #x191970         25 25 112)
                (:mintcream             #xf5fffa         245 255 250)
                (:mistyrose             #xffe4e1         255 228 225)
                (:moccasin              #xffe4b5         255 228 181            38 100 85)
                (:navajowhite           #xffdead         255 222 173)
                (:navy                  #x000080         0 0 128)
                (:oldlace               #xfdf5e6         253 245 230)
                (:olive                 #x808000         128 128 0)
                (:olivedrab             #x6b8e23         107 142 35)
                (:orange                #xffa500         255 165 0)
                (:orangered             #xff4500         255 69 0)
                (:orchid                #xda70d6         218 112 214)
                (:palegoldenrod         #xeee8aa         238 232 170)
                (:palegreen             #x98fb98         152 251 152)
                (:paleturquoise         #xafeeee         175 238 238)
                (:palevioletred         #xdb7093         219 112 147)
                (:papayawhip            #xffefd5         255 239 213)
                (:peachpuff             #xffdab9         255 218 185)
                (:peru                  #xcd853f         205 133 63)
                (:pink                  #xffc0cb         255 192 203)
                (:plum                  #xdda0dd         221 160 221)
                (:powderblue            #xb0e0e6         176 224 230)
                (:purple                #x800080         128 0 128)
                (:rebeccapurple         #x663399         102 51 153)
                (:red                   #xff0000         255 0 0)
                (:rosybrown             #xbc8f8f         188 143 143)
                (:royalblue             #x4169e1         65 105 225)
                (:saddlebrown           #x8b4513         139 69 19)
                (:salmon                #xfa8072         250 128 114)
                (:sandybrown            #xf4a460         244 164 96)
                (:seagreen              #x2e8b57         46 139 87)
                (:seashell              #xfff5ee         255 245 238)
                (:sienna                #xa0522d         160 82 45)
                (:silver                #xc0c0c0         192 192 192)
                (:skyblue               #x87ceeb         135 206 235)
                (:slateblue             #x6a5acd         106 90 205)
                (:slategray             #x708090         112 128 144)
                (:slategrey             #x708090         112 128 144)
                (:snow                  #xfffafa         255 250 250)
                (:springgreen           #x00ff7f         0 255 127)
                (:steelblue             #x4682b4         70 130 180)
                (:tan                   #xd2b48c         210 180 140)
                (:teal                  #x008080         0 128 128)
                (:thistle               #xd8bfd8         216 191 216)
                (:tomato                #xff6347         255 99 71)
                (:turquoise             #x40e0d0         64 224 208)
                (:violet                #xee82ee         238 130 238)
                (:wheat                 #xf5deb3         245 222 179)
                (:white                 #xffffff         255 255 255)
                (:whitesmoke            #xf5f5f5         245 245 245)
                (:yellow                #xffff00         255 255 0)
                (:yellowgreen           #x9acd32         154 205 50             80 61 50)
;;; CSS colours (:name                  value           rgb                     hsl)
        ))
        
(defun get-colour-value( name )
        (let ((v   (assoc name *named-colours*)))
                (if v (second v) (error "get-colour-value: strange colour name: ~a" name))))
                
(defun get-colour-octets( name )        
        (colour-to-rgb (get-colour-value name)))
                
        

;;;---------------------------------------------------------------------------
;;; :section:           as-colour

(defun as-colour( fmt   a   &optional   b c (f 255))
    "u i|u [i] [i] [i] -- i l l  // return   colour   (list :rgb r g b)  (list :hsl h s l)
    :gray gray                    [opacity]     ; :gray 32
    :hex  value                                 ; :hex  #x0000ff
    :hsl  hue|:name  sat   light  [opacity]     ; :hsl  240 100 50  
    :hwb  hue|:name  white black  [opacity]     ; :hwb  :blue 10 20
    :name :name                                 ; :name :blue
    :rgb  red green blue          [opacity]     ; :rgb  0 0 255  128
    
    strange error message: The value of Mezzano.gui::blue is 1200, 
    which is not of type (unsigned-byte 8).
    --> some rgb value is out of range [0..255]
    "
    (flet ((h( x )
                (cond 
                  ((integerp x) x)
                  ((symbolp x)  (multiple-value-call #'rgb-to-hsl (get-colour-octets x)))
                  (t            (error "symbol or integer as first parameter for hsl or hwb expected."))
                  )))
    (let ((colour
              (case fmt
                (:rgb           (mezzano.gui:make-colour-from-octets  a  b c  f))
                (:gray          (mezzano.gui:make-colour-from-octets  a  a a  (if b b 255)))
                (:hsl           (make-colour-from-hsl              (h a) b c  f))
                (:hwb           (make-colour-from-hwb              (h a) b c  f))
                (:hex           (+ (ash #xff 24)  a))
                (:name          (+ (ash #xff 24)    (get-colour-value a)))
                (otherwise      (error "as-colour: strange fmt:   ~a ~a" fmt a))
                )))
    (multiple-value-bind ( r g b ) (colour-to-rgb colour)
    (multiple-value-bind ( h s l ) (rgb-to-hsl r g b)
    (values 
        colour
        (list :rgb r g b)
        (list :hsl h s l))
    )))))

;;;===========================================================================



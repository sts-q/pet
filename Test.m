
MODULE Test;

IMPORT  Out;

CONST
  stackSize             = 256;

TYPE
  Item     = RECORD END;
  IntItem  = RECORD( Item )  i : INTEGER END;
  RealItem = RECORD( Item )  f : REAL    END;
  ItemPointer = POINTER TO Item;

VAR
  dsp : INTEGER;
  dstack:   ARRAY stackSize        OF ItemPointer;



PROCEDURE printItem( VAR ip : ItemPointer );
  VAR  inti : IntItem;
       item : Item;
       i : INTEGER;
BEGIN
  item := ip^;
  IF ip^ IS IntItem THEN  inti := ip^( IntItem );  END;
  Out.Int( inti.i, 0 );
  Out.Ln;
  Out.Int( 900, 0 );
END printItem;



PROCEDURE insertItem( ip: ItemPointer );
BEGIN
  dstack[ dsp ] := ip;
  INC( dsp );
END insertItem;


PROCEDURE insertItems;
  VAR  ip  : POINTER TO IntItem;
BEGIN
  NEW( ip );
  ip^.i := 17;
  insertItem( ip );
  ip^.i := 18;
  insertItem( ip );
END insertItems;



PROCEDURE readItems;
  VAR ip : ItemPointer;
BEGIN
  ip := dstack[ 0 ];  printItem( ip );
END readItems;



BEGIN
  dsp := 0;
  Out.Char( '[' );
  insertItems;
  readItems;
  Out.Char( ']' );
END Test.

(*--------------------------------------------------------------
[18
900]
--------------------------------------------------------------*)




;;;===========================================================================

;;; :file:   pet.lisp                   Plain Editor for Text

;;;===========================================================================

;;; Pet - A Plain Editor for Text.
;;; This code is licensed under MIT License.
;;; http://bitbucket.org/sts-q/pet
;;; Copyright (c) 2019 2020 Heiko Kuhrt
;;; Heiko.Kuhrt (at) yahoo.de

;;; They won’t tell you that they don’t understand it; they will
;;; happily invent their way through the gaps and obscurities.
;;; (V.A. Vyssotsky (found on cat-v.org))
;;;===========================================================================

;;;  ******       Formatting in this file is handmade and assumes a line length of 112 characters.        ******!

;;;     viewer          A window with frame, close button, contents.
;;;     text            A list of strings.

;;; type abbreviations
;;;     u v             symbols
;;;     v               viewer
;;;     i j k           integers
;;;     b               boolean flag
;;;     c               character
;;;     l               list
;;;     p               path
;;;     s               string
;;;     sd              string designator
;;;     ls              list of strings
;;;     text            ls
;;;     name            s
;;;     path            s                       ; here currently in most cases just a simple string
;;;     e               event
;;;     st              stream
;;;     x               anything
;;;     [i]             optional integer
;;;     []              optional nothing

;;;===========================================================================
(print :pet)

(defpackage :pet
  (:use :cl
        :pet.lib
        )

  (:export
           #:autosave.start
           #:autosave.stop
           #:ed-hook-set
           #:ed-hook-reset
           #:find-push
           #:gui
           #:gui-close
           #:gui-close-all
           #:jmp-text-start
           #:jmp-text-end
           #:jmp-line-start
           #:jmp-line-end
           #:logstream.start
           #:logstream.stop
           #:pub
           #:remove-trailing-crs
           #:reset
           #:rewrite-screen
           #:show
           #:save
           #:save-all
           #:save-as
           #:start
           #:set-ruler-position
           #:text-unlock
           #:text-get-lines
           #:text-set
           #:viewer.with-name
           ))


(in-package :pet)


(defclass pet ()
  ((%thread     :initarg :thread      :reader thread)
   (%fifo       :initarg :fifo        :reader fifo              :documentation "event queue for this viewer")
   (%window     :initarg :window      :reader window)
   (%frame      :initarg :frame       :reader frame)
   (%font       :initarg :font        :reader font              :documentation "must be monospace")

   (%name       :initarg :name        :accessor name            :documentation "name of viewer")
   (%path       :initarg :path        :accessor path            :documentation "full path name")
   (%upper-roll :initarg :upper-roll  :accessor upper-roll      :documentation "text above screen")
   (%text       :initarg :text        :accessor text            :documentation "text on screen an below")
   (%cols       :initarg :cols        :accessor cols            :documentation "viewer width in characters")
   (%lines      :initarg :lines       :accessor lines           :documentation "viewer height in lines")
   (%width      :initarg :width       :accessor width           :documentation "viewer width in pixel")
   (%height     :initarg :height      :accessor height          :documentation "viewer height in pixel")
   (%bwin-xpos  :initarg :bwin-xpos   :accessor bwin-xpos       :documentation "viewer basewindow x-position in pixel")
   (%bwin-ypos  :initarg :bwin-ypos   :accessor bwin-ypos       :documentation "viewer basewindow y-position in pixel")
   (%bwin-cols  :initarg :bwin-cols   :accessor bwin-cols       :documentation "viewer basewindow width in cols")
   (%bwin-lines :initarg :bwin-lines  :accessor bwin-lines      :documentation "viewer basewindow height in lines")
   (%cy         :initarg :cy          :accessor cy              :documentation "cursor position: first line is line 1")
   (%cx         :initarg :cx          :accessor cx              :documentation "cursor position: first column is col 0")
   (%hx         :initarg :hx          :accessor hx              :documentation "horizontal scrolling to left offset in cols")
   (%modifiedp  :initarg :modifiedp   :accessor modifiedp       :documentation "text is modified or unchanged since last save")
   (%insertp    :initarg :insertp     :accessor insertp         :documentation "text is in insert mode or overwrite mode")
   (%lockedp    :initarg :lockedp     :accessor lockedp         :documentation "text is locked or not, because someone else changes it right now")
   (%redrawp    :initarg :redrawp     :accessor redrawp         :documentation "viewer needs a redraw: nil|:key")

   (%clickables :initarg :clickables  :accessor clickables))    ; currently unused

  (:default-initargs :name         :pet-viewer
                     :path         ""
                     :upper-roll   nil
                     :text         nil
                     :bwin-xpos    nil          ; get their defaults from (pet:gui ...)
                     :bwin-ypos    nil          ; ''
                     :bwin-cols    nil          ; ''
                     :bwin-lines   nil          ; ''
                     :cy           1
                     :cx           0
                     :hx           0
                     :modifiedp    nil
                     :insertp      t
                     :lockedp      nil
                     :redrawp      :screen
                     :clickables   '()
                     )

  (:documentation "Class pet makes instances of viewers:
                   A Viewer holds all data nessassary to describe a pet window and its content.")

  ) ; defclass pet


(define-symbol-macro thread~            (thread viewer))
(define-symbol-macro fifo~              (fifo viewer))
(define-symbol-macro window~            (window viewer))
(define-symbol-macro frame~             (frame viewer))
(define-symbol-macro font~              (font viewer))

(define-symbol-macro name~              (name viewer))
(define-symbol-macro path~              (path viewer))
(define-symbol-macro upper-roll~        (upper-roll viewer))
(define-symbol-macro text~              (text viewer))
(define-symbol-macro cols~              (cols viewer))
(define-symbol-macro lines~             (lines viewer))
(define-symbol-macro width~             (width viewer))
(define-symbol-macro height~            (height viewer))
(define-symbol-macro bwin-cols~         (bwin-cols viewer))
(define-symbol-macro bwin-lines~        (bwin-lines viewer))
(define-symbol-macro bwin-xpos~         (bwin-xpos viewer))
(define-symbol-macro bwin-ypos~         (bwin-ypos viewer))
(define-symbol-macro modifiedp~         (modifiedp viewer))
(define-symbol-macro insertp~           (insertp viewer))
(define-symbol-macro lockedp~           (lockedp viewer))
(define-symbol-macro redrawp~           (redrawp viewer))
(define-symbol-macro cy~                (cy viewer))
(define-symbol-macro cx~                (cx viewer))
(define-symbol-macro hx~                (hx viewer))

(define-symbol-macro alltext~           (append (reverse upper-roll~) text~))
(define-symbol-macro activep~           (mezzano.gui.widgets:activep (frame viewer)))   ; setf-able

(defmacro above~( i )                   `(above viewer ,i))
(defmacro line~( i )                    `(line viewer ,i))
(defmacro segment~( i j )               `(segment viewer ,i ,j))
(defmacro below~( i )                   `(below viewer ,i))
(defmacro set-text~( &rest args )       `(set-text viewer ,@args))
(defmacro set-line~( line )             `(set-line viewer ,line))



;;;===========================================================================
;;; :chapter: data
(print :---data)
;;;---------------------------------------------------------------------------
;;; :section:           constants: pet

(defparameter *config*                  :mezz   "Name of current configuration of windows and window sizes.")

(defvar *viewers*                       nil     "list of viewers: (list (list name viewer) ...)")

(defvar *ed-hook-old*                   nil     "Previous ed-hook, usually Med.")
(defvar autosave.*thread*               nil     "")
(defvar *text-locked-by*                nil     "Who locked?")

(defvar logstream.*thread*              nil     "pet-logstream-thread")
(defvar logstream.*stream*              nil     "logging all messages from pet, was: popup-io-stream, do not reset to nil")

(defvar *marks*                         nil    "marks: cx cy all-y pathname")
(defvar *find-stack*                    nil)

(defvar *last-location*                 nil     "")

(defvar *previous-files*                nil     "previously visited files")
(defvar *previous-directories*          nil     "previously visited directories")

(defvar *cmdlog*                        nil     "command log: type fn args")

(defvar +labels+                        nil     "nicks to dispay")
(defvar +label-map+                     nil     "command-nick to command-function mapping")
(defvar +label-click-positions+         nil     )


(defparameter +maximal-marks-per-file+  5       "remember at most n marks per file")
(defparameter *ruler-vertical-line-position*    73)

(defparameter autosave.+interval+       900     "save every n seconds")
(defparameter *remove-trailing-spaces*  t     "at every file save or text-get remove trailing spaces")


(defparameter xmark.+col+               96      "column where to insert xmark")      ; see: (xmark.insert()...)

(defvar *clipboard*                     nil     "stack of text snippets")
(defparameter clipboard.+maxlen+        3       "maximal stack-depth of clipboard")

(defparameter *selection*               '((:bottom nil)( :bottomless nil ))  "stack of text snippets: selections")
(defparameter selection.maxlen          2   "")
(defparameter selection.active?         nil     "Is there an active selection?")

(defparameter *char-table*              nil     "utf-8, currently not in use.")


(defparameter +fun-with-file-types+     nil     "file-type specific search-functions for chapter, section")
(defparameter +file+                    (concatenate 'string ":file"":"))
(defparameter +chapter+                 (concatenate 'string ":chapter"":"))
(defparameter +section+                 (concatenate 'string ":section"":"))
(defparameter +label+                   (concatenate 'string "(defun"" "))                ; )

(defparameter +previous-files-max+       9      "in bookmarks list at max i files")
(defparameter +previous-directories-max+ 9      "in bookmarks list at max i directories, plus *dir-nicks*")
(defparameter +tablines-max+             9      "in bookmarks list at max i chapters or sections, sort of 'open tabs'")


;;;===========================================================================
;;; :section:           constants: window

(defparameter ruler-horizontal-p        t       "nick: '-'")
(defparameter ruler-vertical-style      nil     "nick: '|'  possible values: nil ruler line")
(defparameter *time-exec-p*             nil     "nick: 'tm'  if true wrap exec in (time ...)")
(defparameter *echo-command-p*          nil     "nick: 'E'   if true echo every command dispatch at pete-console.")

(defparameter +font+                    (mezzano.gui.font:open-font
                                         mezzano.gui.font:*default-monospace-font*
                                         16))

(defparameter +line-spacing+            1       "vertical distance between lines")
(defparameter +line-height+             (+ +line-spacing+ (max 0 (mezzano.gui.font:line-height +font+))))
(defparameter +char-width+              10      "monospace fonts only")
(defparameter +hscroll-step-width+      24      "at horizontal scrolling do n chars per step")


(defparameter +editor-win-editlines+    25)     ;   25          ; screen-height default value
(defparameter +editor-win-cols+         112)    ;   94          ; line-length default value
(defparameter +editor-win-xpos+         783)    ;  950
(defparameter +editor-win-ypos+         520)    ;  500
(defparameter editor-win.+border-width-left+    40    "distance window-frame to character")
(defparameter editor-win.+border-width-right+   8     "distance window-frame to character")
(defparameter editor-win.+border-width-bottom+  2     "distance window-frame to last line")

(defparameter editor-win.*height*      nil      "height in pixel")

;;;(defvar *console-io-stream*             nil     "Pet terminal output")
(defparameter +pet-console-xpos+        90)     ; 1250   310
(defparameter +pet-console-ypos+        0)      ;   12   600
(defparameter +pet-console-cols+       112)
(defparameter +pet-console-lines+       15)

(defparameter +key-map+                 nil     "keyboard-key to command-function mapping")

(defparameter +mouse-wheel-scroll-amount+ 2     "scroll n lines per mouse wheel event")
(defparameter mouse-delta-click-time    500     "double click delay time")
(defparameter mouse-1-click-count       1)
(defparameter mouse-1-click-time        0)
(defparameter internal-run-time-to-msec   (truncate mezzano.internals::internal-time-units-per-second 1000))

(defparameter mx                        0     "mouse event position: column in current screen")
(defparameter my                        1     "mouse event position: line in current screen")
(defparameter was1                      nil)    ; mouse button 1 was already down
(defparameter was2                      nil)    ; mouse button 2 was already down
(defparameter was3                      nil)    ; mouse button 3 was already down
(defparameter was4                      nil)    ; wheel down
(defparameter was5                      nil)    ; wheel up
(defparameter *last-mouse-event*        nil)

(defconstant +o+                        #\(     "opening parenthesis")
(defconstant +c+                        #\)     "closing parenthesis")

;;;---------------------------------------------------------------------------
;;; :section:           constants: colour

(defparameter +in-lavender+             (as-colour :rgb 230 160 255))
(defparameter +in-magenta+              (as-colour :rgb 240 50 230))

(defparameter +as-labels-bg-colour+     nil)
(defparameter +as-labels-fg-colour+     nil)
(defparameter +text-bg-colour+          nil)
(defparameter +as-ruler-shade+          nil)
(defparameter +as-ruler-line+           nil)
(defparameter +as-long-line-hint+       nil)
(defparameter +text-border-colour+      nil)

(defparameter +as-plain-text+           nil)
(defparameter +as-comment+              nil)
(defparameter +as-chapter+              nil)
(defparameter +as-section+              nil)
(defparameter +as-selection-bg+         nil)

(defparameter +as-cursor-fg+            nil)
(defparameter +as-cursor-bg+            nil)
(defparameter +as-overwrite-cursor-fg+  nil)
(defparameter +as-overwrite-cursor-bg+  nil)
(defparameter +as-passive-cursor-fg+    nil)
(defparameter +as-passive-cursor-bg+    nil)


;;;---------------------------------------------------------------------------
;;; :section:           colour schemes

(defun colours.mezzano-style()
    (let ((name   mezzano.gui.theme:*background*))
        (setf +as-labels-bg-colour+     mezzano.gui.theme:*active-frame*
              +as-labels-fg-colour+     mezzano.gui.theme:*frame-title*

              +text-bg-colour+          name
              +as-ruler-shade+          (as-colour :gray  56 #xD8)
              +as-ruler-line+           (as-colour :gray  16 128)
              +as-long-line-hint+       +in-magenta+
              +text-border-colour+      name

              +as-plain-text+           mezzano.gui.theme:*foreground*
              +as-comment+              (as-colour :gray  76)
              +as-comment+              (as-colour :name :SlateGray)
              +as-chapter+              (as-colour :hwb  240  60   0)
              +as-section+              (as-colour :rgb  240  60   0)
              +as-selection-bg+         (as-colour :rgb   64  52 100    #xD8)

              +as-cursor-fg+            (as-colour :rgb 200 200 200  )
              +as-cursor-bg+            (as-colour :rgb 255   0   0  )
              +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  )
              +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  )
              +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  )
              +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        ))
(colours.mezzano-style) ; init colours


(defun colours.on-lightgray()
       (let ((name :DarkGray))
        (setf +as-labels-bg-colour+     (as-colour :name :MidnightBlue ))
        (setf +as-labels-fg-colour+     (as-colour :rgb 255 232  16 ))

        (setf +text-bg-colour+          (as-colour :name name))
        (setf +as-ruler-shade+          (as-colour :hsl name 0 60))
        (setf +as-ruler-line+           (as-colour :rgb   16  16  16    128))
        (setf +as-long-line-hint+       +in-magenta+)
        (setf +text-border-colour+      (as-colour :hsl name 0 69))

        (setf +as-plain-text+           (as-colour :gray  16))
        (setf +as-comment+              (as-colour :gray  76))
        (setf +as-chapter+              (as-colour :name :MediumBlue))
        (setf +as-section+              (as-colour :rgb   80 128 230))
        (setf +as-selection-bg+         (as-colour :rgb   64  52 100    255))

        (setf +as-cursor-fg+            (as-colour :rgb 200 200 200  ))
        (setf +as-cursor-bg+            (as-colour :rgb 255   0   0  ))
        (setf +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  ))
        (setf +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  ))
        (setf +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  ))
        (setf +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        ))

(defun colours.on-darkgray()
        (setf +as-labels-bg-colour+     (as-colour :rgb   0   0 255 0 ))
        (setf +as-labels-fg-colour+     (as-colour :rgb 255 232  16 ))

        (setf +text-bg-colour+          (as-colour :gray  48))
        (setf +as-ruler-shade+          (as-colour :gray  28))
        (setf +as-ruler-line+           (as-colour :rgb   16  16  16    128))
        (setf +as-long-line-hint+       +in-magenta+)
        (setf +text-border-colour+      (as-colour :gray  64))

        (setf +as-plain-text+           (as-colour :gray 136))
        (setf +as-comment+              (as-colour :gray  76))
        (setf +as-chapter+              (as-colour :rgb   80 128 230))
        (setf +as-section+              (as-colour :rgb   80 128 230))
        (setf +as-selection-bg+         (as-colour :rgb   64  52 100    255))

        (setf +as-cursor-fg+            (as-colour :rgb 200 200 200  ))
        (setf +as-cursor-bg+            (as-colour :rgb 255   0   0  ))
        (setf +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  ))
        (setf +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  ))
        (setf +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  ))
        (setf +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        )

(defun colours.on-black()
        (setf +as-labels-bg-colour+     (as-colour :rgb   0   0 255 0 ))
        (setf +as-labels-fg-colour+     (as-colour :rgb 255 232  16 ))

        (setf +text-bg-colour+          (as-colour :gray  16))
        (setf +as-ruler-shade+          (as-colour :gray  28))
        (setf +as-ruler-line+           (as-colour :rgb   16  16  16    128))
        (setf +as-long-line-hint+       +in-magenta+)
        (setf +text-border-colour+      (as-colour :gray  48))

        (setf +as-plain-text+           (as-colour :gray 136))
        (setf +as-comment+              (as-colour :gray  76))
        (setf +as-chapter+              (as-colour :rgb   80 128 230))
        (setf +as-section+              (as-colour :rgb   80 128 230))
        (setf +as-selection-bg+         (as-colour :rgb   64  52 100    255))

        (setf +as-cursor-fg+            (as-colour :rgb 200 200 200  ))
        (setf +as-cursor-bg+            (as-colour :rgb 255   0   0  ))
        (setf +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  ))
        (setf +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  ))
        (setf +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  ))
        (setf +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        )


(defun colours.on-linen()
       (let ((name :linen))                                                           ; 30 67 94
        (setf +as-labels-bg-colour+     (as-colour :rgb #x4e #x4e #x4e #xd8)
              +as-labels-fg-colour+     (as-colour :hwb 55 40 0)

              +text-bg-colour+          (as-colour :name name)
              +as-ruler-shade+          (as-colour :hsl name 67 90)
              +as-ruler-line+           (as-colour :gray  16 128)
              +as-long-line-hint+       +in-magenta+
              +text-border-colour+      (as-colour :hsl name 30 90)

              +as-plain-text+           (as-colour :gray 8)
              +as-comment+              (as-colour :name :SlateGray)
              +as-chapter+              (as-colour :hwb  240  0   0)
              +as-section+              (as-colour :rgb  240  60   0)
              +as-selection-bg+         (as-colour :hsl name 67 80)

              +as-cursor-fg+            (as-colour :rgb 200 200 200  )
              +as-cursor-bg+            (as-colour :rgb 255   0   0  )
              +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  )
              +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  )
              +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  )
              +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        ))


(defun colours.on-seashell()
       (let ((name :seashell))                                                           ; 30 67 94
        (setf +as-labels-bg-colour+     (as-colour :rgb #x4e #x4e #x4e #xd8)
              +as-labels-fg-colour+     (as-colour :hwb 55 40 0)

              +text-bg-colour+          (as-colour :name name)
              +as-ruler-shade+          (as-colour :hsl name 67 90)
              +as-ruler-line+           (as-colour :gray  16 128)
              +as-long-line-hint+       +in-magenta+
              +text-border-colour+      (as-colour :hsl name 30 90)

              +as-plain-text+           (as-colour :gray 8)
              +as-comment+              (as-colour :name :SlateGray)
              +as-chapter+              (as-colour :hwb  240  0   0)
              +as-section+              (as-colour :rgb  240  60   0)
              +as-selection-bg+         (as-colour :hsl name 67 80)

              +as-cursor-fg+            (as-colour :rgb 200 200 200  )
              +as-cursor-bg+            (as-colour :rgb 255   0   0  )
              +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  )
              +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  )
              +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  )
              +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        ))

(defun colours.on-tan()
    (let ((name :tan))                                                                  ; 34 44 69
        (setf +as-labels-bg-colour+     (as-colour :rgb #x4e #x4e #x4e #xd8)
              +as-labels-fg-colour+     (as-colour :hwb 55 40 0)

              +text-bg-colour+          (as-colour :name name)
              +as-ruler-shade+          (as-colour :hsl  name 44 65)
              +as-ruler-line+           (as-colour :hsl  name 44 65 120)
              +as-long-line-hint+       +in-magenta+
              +text-border-colour+      (as-colour :hsl name 32 65)

              +as-plain-text+           (as-colour :gray 16)
              +as-comment+              (as-colour :gray  76)
              +as-chapter+              (as-colour :hwb  240  60   0)
              +as-section+              (as-colour :rgb  240  60   0)
              +as-selection-bg+         (as-colour :hsl name 44 48)

              +as-cursor-fg+            (as-colour :rgb 200 200 200  )
              +as-cursor-bg+            (as-colour :rgb 255   0   0  )
              +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  )
              +as-overwrite-cursor-bg+  (as-colour :rgb 0   128   0  )
              +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  )
              +as-passive-cursor-bg+    (as-colour :rgb  0   0 172  ))
        ))


(defun colours.on-cornflowerblue()
        (setf +as-labels-bg-colour+     (as-colour :rgb   0    0 255 128))
        (setf +as-labels-fg-colour+     (as-colour :rgb  255 232  16 ))

        (setf +text-bg-colour+          (as-colour :name :CornFlowerBlue))              ; 219 79 66
        (setf +as-ruler-shade+          (as-colour :hsl  :CornFlowerBlue 79  72))
        (setf +as-ruler-line+           (as-colour :hsl  :CornFlowerBlue 79  66  128))
        (setf +as-long-line-hint+       +in-magenta+)

        (setf +as-plain-text+           (as-colour :gray  16))
        (setf +as-comment+              (as-colour :gray 100))
        (setf +as-chapter+              (as-colour :hwb  240   0   0))
        (setf +as-section+              (as-colour :hwb  240   0   0))
        (setf +as-selection-bg+         (as-colour :hwb  293  30  30  220))

        (setf +as-cursor-fg+            (as-colour :rgb 200 200 200  ))
        (setf +as-cursor-bg+            (as-colour :rgb 255   0   0  ))
        (setf +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  ))
        (setf +as-overwrite-cursor-bg+  (as-colour :hwb  125   0   0))
        (setf +text-border-colour+      (as-colour :hsl :CornFlowerBlue 60 60))
        )

(defun colours.on-MidnightBlue()
    (let ((name :MidnightBlue))                                                         ; 240 64 27
        (setf +as-labels-bg-colour+     (as-colour :rgb   0    0 255 128))
        (setf +as-labels-fg-colour+     (as-colour :rgb  255 232  16 ))

        (setf +text-bg-colour+          (as-colour :name name))
        (setf +as-ruler-shade+          (as-colour :hsl  name 64 23 ))
        (setf +as-ruler-line+           (as-colour :hsl  name 64 50 120))
        (setf +as-long-line-hint+       +in-magenta+)

        (setf +as-plain-text+           (as-colour :gray 128))
        (setf +as-comment+              (as-colour :gray  64))
        (setf +as-chapter+              (as-colour :hsl name  64 80))
        (setf +as-section+              (as-colour :hsl  240   0 27))
        (setf +as-selection-bg+         (as-colour :hsl name 64 35))

        (setf +as-cursor-fg+            (as-colour :rgb 200 200 200  ))
        (setf +as-cursor-bg+            (as-colour :rgb 255   0   0  ))
        (setf +as-overwrite-cursor-fg+  (as-colour :rgb 255 255 255  ))
        (setf +as-overwrite-cursor-bg+  (as-colour :hwb  125   0   0))
        (setf +as-passive-cursor-fg+    (as-colour :rgb 128 128 128  ))
        (setf +as-passive-cursor-bg+    (as-colour :rgb  0   0 255  ))
        (setf +text-border-colour+      (as-colour :hsl name 100 30))
        (setf +text-border-colour+      (as-colour :hsl name 64 30))
        (setf +text-border-colour+      (as-colour :hsl name 45 30))
        ))


;;;===========================================================================
;;; :chapter: lib
(print :---lib)
;;;---------------------------------------------------------------------------
;;; :section:           lib

;;; lock only for semi-atomic functions, that should work always in itself
;;; lock text when:
;;;     * set-text: insert key event, mouse do something event
;;;     * text-get: save, autosave
(defmacro with-text-locked( viewer &rest body)
  `(progn
        (loop for i from 1 to 600 do
                (unless lockedp~ (return))
                (when (zerop (mod i 10))  (say /(today-now) :pet (name ,viewer) "is locked, waiting..."))
                (when (zerop (mod i 40))  (setf lockedp~ nil) (err  "Text is locked, giving up. Unlocked text, now."))
                (sleep 0.1))
        (setf lockedp~ t)
        (multiple-value-bind
                (r e)

                (ignore-errors ,@body)

            (unless  (and (eql t r) (not e))
                (say / "#################  with-text-locked Error:" (name ,viewer) / e /)
                (sleep 2)))
        (setf lockedp~ nil)
     ))

(defun text-unlock()
        "unlock all texts in order to manually release deadlock"
        (dolist (viewer *viewers*)
                (setf lockedp~ nil))
        (say / :text-unlocked)
        (values))


(defun line-fg-colour( viewer str )
        (cond
         ((search +file+ str)                                           +as-chapter+)
         ((funcall (ftf viewer :chapter) str)                           +as-chapter+)
         ((funcall (ftf viewer :section) str)                           +as-chapter+)
         ((eql 0 (search (file.comment-char path~) (despace-left str))) +as-comment+)
         (t                                                             +as-plain-text+)
         ))

(defun cursor-fg-colour( viewer ) (if insertp~   +as-cursor-fg+  +as-overwrite-cursor-fg+))

(defun cursor-bg-colour( viewer )
  (if (viewer.active-p viewer)
      (if insertp~
          +as-cursor-bg+
          +as-overwrite-cursor-bg+)
      +as-passive-cursor-bg+  ))

(defun xmark.text( viewer )
        (case (file.type-simplified path~)
          (:pas         "(* X-mark-X *)")
          (:v           "// X-mark-X")
          (t            (concat ";"" X-mark-X"))
          ))


;;;---------------------------------------------------------------------------
;;; :section:           editor-win

(defun editor-win.width( cols )
        "i -- i  // width of editor window in pixel"
        (+   editor-win.+border-width-left+ (* cols +char-width+)  editor-win.+border-width-right+))

(defun editor-win.height( &optional height )
        "[i]-- i  // [set] height in pixel"
        (cond
         (height                (setf editor-win.*height* height))
         (editor-win.*height*   editor-win.*height*)
         (t                     (+ 4 editor-win.+border-width-bottom+ (+ 2 (* +editor-win-editlines+ +line-height+))))
         ))

(defun editor-win.height-2( viewer n type )
        "v|nil i u --  // height in :pixel|:lines to set both viewer slots consistent"
        (let (height lines)
            (cond
                ((eql type :pixel)    (setf lines (- (truncate
                                                        (- n 4 editor-win.+border-width-bottom+)
                                                        +line-height+)
                                                   2)))
                ((eql type :lines)    (setf lines n))
                (t                    (err "editor-win.height: strange type: "))
                )
        (setf height   (+ 4 (* (+ 2 lines) +line-height+)))
        (when viewer
                (setf height~ height
                      lines~  lines))
        (values height lines)
        ))

(defun editor-win.bottom-border-width( viewer )
        "-- i  // width of bottom border"
        (if editor-win.*height*
                (-   editor-win.*height*  4   (* +line-height+ (+ 2 lines~)))
                editor-win.+border-width-bottom+
                ))


(defmacro nextdrawis( draw )
        "// draw is one of: nowt thisline oneup onedown screen"
        `(setf (redrawp viewer) ,draw)
        )


;;;---------------------------------------------------------------------------
;;; :section:           line

(defun asline( &rest strings )  (despace (apply #'concat strings)))

(defun head( line x )       (subseq line 0 (min (length line) x)))
(defun body( line x end )   (subseq line (min (length line) x) (min (length line) end)))
(defun tail( line x )       (subseq line (min (length line) x)))


;;---------------------------------------------------------------------------
;;; :section:           text

(defun text-locked-p()          *text-locked-by*)

;;;(defun format-text( lines )     (format nil "~{~a~%~}" lines))
(defun format-text( lines )
         (with-output-to-string (stream)
            (dolist (l lines)
              (write-line l stream))))

(defun visible-text( viewer )   (subseq text~ 0 lines~))        ; on screen visible lines of text

(defun roll-up( viewer n )
  "roll-up is scroll-down"
  (dotimes (i n)
    (setf upper-roll~   (cons (car text~) upper-roll~)
          text~         (cdr text~))
    ))

(defun roll-down( viewer n )
  "roll-down is scroll-up"
  (dotimes (i n)
    (setf text~         (cons (car upper-roll~)  text~)
          upper-roll~   (cdr upper-roll~))
    ))

(defun bottom-roll-p( viewer )  (> (length text~) lines~))              ; some text is below screen invisible
(defun is-last-line(  viewer y )(>= y (length text~)))

(defun all-y( viewer i )        (+ i (length upper-roll~) -1))                ; index: text -> alltext

(defun above( viewer i )        (subseq text~ 0      (1- i)))
(defun line(  viewer i )        (elt    text~ (1- i)))
(defun segment( viewer i j )    (subseq text~ (1- i) j))
(defun below( viewer i )        (subseq text~ i       ))

(defun text.line( viewer y )
        "i -- s  // return line y, alltext"
        (elt alltext~ (1- y)))

(defun set-text( viewer &rest args )
  (setf (modifiedp viewer)      t)
  (setf (text viewer)           (apply #'append (mapcar (lambda( x )
                                                  (cond
                                                   ((listp x )    x)
                                                   ((stringp x)   (list x))
                                                   (t             (err x "set-text: list or string expected:"))
                                                   ))
                                                args))))


(defun set-line( viewer line )   (set-text viewer (above~ cy~) line (below~ cy~)))


(defun delete-char-fn( viewer y col )
  (let* ((line  (line~ y)))
    (set-line~  (asline (head line col) (tail line (1+ col))))
    ))

(defun replace-char-fn( viewer y col c )
  (let* ((line  (line~ y)))
    (set-line~  (asline (head line col) (format nil "~a" c) (tail line (1+ col))))
    ))

(defun jmp-to-position-fn( viewer ally col)
  (when (and ally col)
    (let* ((txt                 alltext~)
           (upper-roll          (reverse (subseq txt 0   (1- ally))))
           (text                         (subseq txt (1- ally)))
           )
      (setf upper-roll~        upper-roll
            text~              text
            cy~                1               ; now relative to text, that is first line on screen
            cx~                col
            ))))


(defun backward-find( viewer i fn )
       (let* ((txt   (reverse (take  (1+ i) alltext~)))
              (ln     (1+ i))
              (col   nil))
       (loop
                (return-unless txt)
                (setf col  (funcall fn (car txt)))
                (return-when col)
                (setf txt (cdr txt))
                (decf ln))
       (if col
                (values  ln col)
                (values nil nil))))


(defun forward-find( viewer i fn )
       (let* ((txt   (drop i alltext~))
              (ln    (1+ i))
              (col   nil))
       (loop
                (return-unless txt)
                (setf col  (funcall fn (car txt)))
                (return-when col)
                (setf txt (cdr txt))
                (incf ln))
       (if col
                (values  ln col)
                (values nil nil))))


;;;--------------------------------------------------------------------------
;;; :section:           clipboard.

;;; tos: top of stack

(defparameter clip-types        '(:char
                                  :word :hyphen-phrase :dot-phrase :colon-phrase
                                  :sexp
                                  :line))

'(
        (dda. pet::clipboard)           ; exec with Ctrl-Enter
        (pet::clipboard.dump)
        (pet::clipboard.tos)
)
;;;-------------------------------------

(defun clipboard.push( type lines )
        "// Push snippet to clipboard."
        (unless (member type clip-types)
                (err "sww: clipboard.push: strange type."))
        (push (list type lines) *clipboard*)
        (setf-to-maxlen clipboard.+maxlen+ *clipboard*))

(defun clipboard.dump()
        "// Print clipboard contents."
        (dolist (rec *clipboard*)
                (say // ":"(first rec) /)
                (dolist (l (second rec))   (say l /))))

(defun clipboard.rolldown()
        "// Roll tos down to bottom of clipboard stack."
        (setf *clipboard*  (rolldown 1 *clipboard*)))

(defun clipboard.tos()
        "-- type lines  // Return clipboard tos: type contents or nil nil, lines are a fresh copy."
        (values (first (car *clipboard*))  (text.copy (second (car *clipboard*)))))


(defun clipboard.insert( viewer )                                                               ; XmarX
        "// Insert tos of clipboard into text at cursor position."
        (flet ((insert-simple-tap( txt fn )
                        (let* ((line  (line~ cy~))
                               (pos   (or  (funcall fn line cx~)   cx~))
                               (len   (length txt)))
                           (set-line~ (asline (head line pos)  txt " " (tail line pos)))
                           (setf cx~ (+ pos len))
                           ))
               (insert-chars( lines )
                        (let* ((above   (above~ cy~))
                               (line    (line~  cy~))
                               (below   (below~ cy~))
                               (head    (head line cx~))
                               (tail    (tail line cx~))
                               (insertion  (cond
                                             ((eql 1 (length lines))
                                                        (asline head (first lines) tail))
                                             ((eql 2 (length lines))
                                                  (list (asline head (first lines))
                                                        (asline      (second lines)  tail)))
                                             (t
                                                  (cons          (asline head (first lines))
                                                     (join       (butlast (cdr lines))
                                                                 (asline  (car(last lines))   tail))))
                                             )))
                           (set-text~ above insertion below)
                           )))
        (multiple-value-bind (type lines) (clipboard.tos)
        (case type
                (:char          (say :char))
                (:word          (say :word))
                (:hyphen-phrase (say :hyphen-phrase))
                (:dot-phrase    (say :dot-phrase))
                (:colon-phrase  (say :colon-phrase))
                (:sexp          (say :sexp))
                (:line          (say :line))
                (otherwise      (err "clipboard.insert: strange clipboard-type found." type))
                )
        (case type
            (:char              (insert-chars lines))
            (:word              (insert-simple-tap (first lines) #'find-word-start))
            (:hyphen-phrase     (insert-simple-tap (first lines) #'find-hyphen-phrase-start))
            (:dot-phrase        (insert-simple-tap (first lines) #'find-dot-phrase-start))
            (:colon-phrase      (insert-simple-tap (first lines) #'find-colon-phrase-start))
            (:sexp              (setf cx~ (or (find-colon-phrase-start (line~ cy~) cx~)  cx~))
                                (insert-chars lines))
            (:line              (set-text~  (above~ cy~) lines (below~ (1- cy~))))
            (otherwise          (err "clipboard.insert: insertion of type ROADWORK." type))
            )
            t )))


;;;--------------------------------------------------------------------------
;;; :section:           find-history

(defun find-string-value()      (car *find-stack*))
(defun find-pop()               (pop *find-stack*))
(defun push-to-find-stack( x )
        (setf *find-stack*      (cons x   (take 23   (remove x   *find-stack* :test #'equal)))))

(defun find-push( &optional x (pushmarkp t) )
        "i|s|[] -- // rotate i times | push s | print contents"
        (let ((viewer (viewers.get-last-active)))
        (cond
           ((integerp x)
              (cond
                ((> x 0)        (setf *find-stack*   (rolldown x       *find-stack*)))
                ((< x 0)        (setf *find-stack*   (rollup   (abs x) *find-stack*))))
                (find-push)     ; print stack
                (values))
           ((stringp  x)        (push-to-find-stack x)
                                (when pushmarkp (mark.push viewer :user))
                                x)
           ((eq nil   x)        (dolist (x (reverse *find-stack*)) (say / x))  (values))
           (t                   (err "find-push: integer, string or nil expected")))))


;;;---------------------------------------------------------------------------
;;; :section:           find next/prev empty-line current-section

(defun find-word-char-backwards( line i )
  (cond
    ((eql i 0)                  0)
    ((wordchrp (char line i))   i)
    (t                          (find-word-char-backwards line (1- i)))
    ))

(defun find-word-char-forward( line i )
  (cond
    ((>= i (length line))       i)
    ((wordchrp (char line i))   i)
    (t                          (find-word-char-forward line (1+ i)))
    ))

;;;-------------------------------------

(defun find-simple-tap-start( line i also )
        "s i s -- i|nil  // simple tap:  word  hyphen-word  dotted.word  my.colon::named-word"
        (if (wordchrp (char line i) :also also)
            (loop(cond
                ((zerop i)                                      (return 0))
                ((not (wordchrp (char line (1- i)) :also also)) (return i))
                (t                                              (decf i))
                ))
            nil))

(defun find-simple-tap-end( line i also )
        "s i s -- i|nil"
        (let ((len  (length line)))
           (if (and   (< i len)   (wordchrp (char line i) :also also))
                (loop(cond
                        ((= i len)                             (return  i))
                        ((wordchrp (char line i) :also also)   (incf i))
                        (t                                     (return  i))
                        ))
                nil)))


(defun find-word-start( line i )                (find-simple-tap-start line i ""))
(defun find-word-end( line i )                  (find-simple-tap-end   line i ""))
(defun find-hyphen-phrase-start( line i )       (find-simple-tap-start line i "-"))
(defun find-hyphen-phrase-end( line i )         (find-simple-tap-end   line i "-"))
(defun find-dot-phrase-start( line i )          (find-simple-tap-start line i "-."))
(defun find-dot-phrase-end( line i )            (find-simple-tap-end   line i "-."))
(defun find-colon-phrase-start( line i )        (find-simple-tap-start line i "-.:"))
(defun find-colon-phrase-end( line i )          (find-simple-tap-end   line i "-.:"))


;;;-------------------------------------
(defun find-prev-word-start( line i )
  "s i -- i|nil"
  (when (>= i (length line))  (setf i (1- (length line))))
  (cond
    ((<= i 0)                   nil)
    ((wordchrp (char line i))   (find-word-start line  i))
    (t                          (find-word-start line  (find-word-char-backwards line i)))
    ))

(defun find-next-word-end( line i )
  "s i -- i|nil"
  (cond
   ((>= i (length line))        (length line))                  ; i points behind line
   ((wordchrp (char line i))    (find-word-end line i))
   (t                           (find-word-end line    (find-word-char-forward line i)))
   ))


(defun find-prev-phrase-start( line i )
  "i point at '-'"
  (when (>= i (length line))  (setf i (1- (length line))))
  (cond
     ((zerop (length line))             0)
     ((zerop i)                         0)
     ((wordchrp (char line (1- i)))
                (let ((i (find-word-start line (1- i)))
                        )
                    (if (and (> i 0)  (char-at= line (1- i) #\-))
                                        (find-prev-phrase-start line (1- i))
                                        i)))
     (t                                 i)
     ))

(defun find-next-phrase-end( line i )
  (cond
     ((eql i (length line))             i)
     ((and (< i (1- (length line)))  (wordchrp (char line (1+ i))))
                (let ((i  (find-next-word-end line i)))
                    (if (and  (< i (length line))  (char-at= line i #\-))
                                        (find-next-phrase-end line (1+ i))
                                        i)))
    (t                                  i)
    ))


(defun find-next-empty-line( viewer y )
        (cond
         ((is-last-line viewer y)               nil)
         ((only-whitespace-p (line viewer y))   y)
         (t                                     (find-next-empty-line viewer (1+ y)))
         ))


(defun get-current-section( viewer )
        "return first section or chapter to be found upwards from current line. screen-changing."
        (jmp-down-1 viewer)
        (jmp-prev-section viewer)
        (let* ((s        (line viewer cy~))
               (s        (if (headseqp ";;; :" s)   (subseq s 4)   s))
               )
           (if (eql cy~ 1) nil s)))


;;;---------------------------------------------------------------------------
;;; :section:           thing-at-point

(defun thing-at-point( viewer )
  (let ((line  (line~ cy~)))
  (flet((wordstart()    (find-prev-word-start   line cx~))
        (wordend()      (find-next-word-end     line cx~))
        (phrasestart()  (find-prev-phrase-start line cx~))
        (phraseend()    (find-next-phrase-end   line cx~))
        )
  (let ((res (cond
                ((>= cx~ (length line))                 nil)
                ((wordchrp (char line cx~))             (subseq line (wordstart) (wordend)))
                ((char-at= line cx~ #\-)                (subseq line (phrasestart) (phraseend)))
                (t                                      nil)
                )))
      (say /"thing-at-point: <" res ">" /)
      res))))

;;;---------------------------------------------------------------------------
;;; :section:           selection basics

(defun selection.type()         (first (car *selection*)))
(defun selection.y1()           (second (car *selection*)))
(defun selection.y2()           (third (car *selection*)))
(defun selection.x1()           (fourth (car *selection*)))
(defun selection.x2()           (fifth (car *selection*)))
(defun selection.lines()        (sixth (car *selection*)))

(defun selection.drop() "drop tos"      (pop *selection*)   t)
(defun selection.clear()                (when selection.active?   (setf selection.active? nil)))

(defun selection.to-clipboard()
        "Push current selection to clipboard."
        (if selection.active?
                (clipboard.push (selection.type) (selection.lines))
                (err "sww: selection.to-clipboard")))

(defun selection.delete( viewer )
        "Delete selection from text."
        (when selection.active?
            (let ((above  (above~ (selection.y1)))
                  (below  (below~ (selection.y2))))
                (set-text~  above below)
                (setf cy~   (selection.y1)
                      cx~   0))))


;;; :section:           selection shift-up adjust

(defun selection.shift-up( viewer i j )
  "Switch selection type to next higher level: char -> word -> pharse ..."
  (let* ((lines   (text.copy (subseq (text viewer) (1- i) j))))
    (setf *selection* (cons   (list   :line i j nil nil   lines)
                              *selection*))))


(defun selection.adjust()
        "Adjust selection extent."
        (ROADWORK :selection.adjust))

;;;---------------------------------------------------------------------------
;;; :section:           mark.

(defun mark-here-p( viewer mark )
        (and
                (path.= (path viewer) (fourth mark))
                (eql (name viewer) (fifth mark))
                ))

(defun mark-type-p( type mark )   (eql type (sixth mark)))

(defun marks-dump()
        (format t "   x   y    z pathname~80T viewer~88T type~%")
        (format t "~{~{~4d~4d~5d ~a~80T ~a~88T ~a~}~%~}"        (reverse *marks*))
        (format t "   x   y    z pathname~80T viewer~88T type~%")
        (say / :> (length *marks*) :marks-found))

'(
        (marks-dump)
        (dda. mark)
 )

;;;-------------------------------------
(defun mark.sort( viewer )
    "// get marks of current file and viewer on top, limit number of stored marks"
    (let* ((others       (remove-if     (lambda( m ) (mark-here-p viewer m)) *marks*))
           (here         (remove-if-not (lambda( m ) (mark-here-p viewer m)) *marks*)))
        (setf *marks* (append (take +maximal-marks-per-file+ here) others))))

(defun mark.push( viewer type &optional dip )
  "// push mark. dip: push mark underneath topmost mark"
  (let ((m (list  cx~
                  cy~
                  (all-y viewer 1)
                  (copy-seq  (path viewer))
                  (name viewer)
                  type)))
     (if dip
         (push m (cdr *marks*))
         (push m *marks*))))

(defun mark.pop( viewer type )
  "// pop mark"
  (mark.sort viewer)
  (let ((m (car *marks*)))
    (cond
     ((not m)                           (say / "no mark found at all")          nil)
     ((not (mark-here-p viewer m))      (say / "no mark found here")            nil)
     ((not (mark-type-p type m))        (say / "no mark found of type: " type)  nil)
     (t                                 (pop *marks*)))))

(defun mark.rotate( viewer )
        "// rotate marks: jump to previous marks. Currently: swap topmost two marks."
        (mark.sort  viewer)
        (let ((m1  (mark.pop viewer :user))
              (m2  (mark.pop viewer :user))
              )
           (when m1 (push m1 *marks*))
           (when m2 (push m2 *marks*))
           m2))

(defun mark.swap( viewer )
        "// exchange topmost two marks"
        (mark.rotate viewer))

(defun mark.find( viewer mark )
    "// jump to mark"
    (when mark
        (multiple-value-bind (x y z file) (values-list mark)
        (unless (equal file (path viewer)) (ROADWORK :mark.find-in-other-file.))
        (let ((z0 (all-y viewer 1)))
                (if( > z z0 )( roll-up viewer (- z z0) )( roll-down viewer (- z0 z)) )
                (setf   cx~ x
                        cy~ y)))))


;;;---------------------------------------------------------------------------
;;; :section:           files: last location: (path file section)
(defun set-last-location( viewer )
        (multiple-value-bind (path subdir name type)    (path-location (path viewer))
        (let*               ((section                   (get-current-section viewer)))
            (if (symbolp path)  (setf path (dirnick.nick-show path)))
            (setf *last-location* (list   path subdir type name section)))))

(defun format-location( loc )
        (format nil
                "    (locate\"~a\")"
                (let* ((s       (justify-right 15 (first loc)))
                       (s       (concat s  " " (second loc)" "))
                       (s       (concat s   (spaces (- 32 (length s) (length (third loc))))   (third loc)" "))
                       (s       (justify-left 51 (concat s   (fourth loc))))
                       (s       (if (fifth loc)
                                        (justify-left 95 (concat s "|" (fifth loc)))
                                        (justify-left 95         s)
                                        )))
                       s)))

;;;---------------------------------------------------------------------------
;;; :section:           files: extension/type specific


(setf +fun-with-file-types+
 (list
        `(:lisp
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:label         ,(lambda( line )  (or (and (zerop (indentation line))
                                                           (not (equal "" line))
                                                           (not (equal (head line 1) ";")))
                                                      (search +section+  line)
                                                      (search +chapter+  line))))
                (:comment       #'toggle-comment)
                )
        `(:rabbit
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:label         ,(lambda( line )  (or (zerop (indentation line)))
                                                      (search +section+  line)
                                                      (search +chapter+  line)))
                (:comment       #'toggle-comment)
                )
        `(:forth
                (:chapter       ,(lambda( line ) (search +chapter+ line)))
                (:section       ,(lambda( line ) (search +section+ line)))
                (:label         ,(lambda( line ) (search +label+   line)))
                (:comment       #'toggle-comment)
                )
        `(:tex
                (:chapter       ,(lambda( line ) (search "\section"    line)))
                (:section       ,(lambda( line ) (or (search "\\subsection" line) (search "\\section" line))))
;;;             (:label         ,(lambda( line ) (search +label+   line)))
                (:comment       #'toggle-comment)
                )
        `(:md
                (:chapter       ,(lambda( line ) (eql 0 (search "# "  line))))
                (:section       ,(lambda( line ) (or
                                                        (eql 0 (search "# " line))
                                                        (eql 0 (search "## " line))
                                                        (eql 0 (search "### " line))
                                                        (eql 0 (search "#### " line))
                                                        )))
                (:label         ,(lambda( line )
                                  (or
                                   (let ((indentation (indentation line))
                                         (colon-pos   (position #\colon line)))
                                     (if (or (not(eql 0 indentation))  (not colon-pos))
                                         nil
                                         (not (position-if-not #'phrasechrp (subseq line 0 colon-pos)))
                                         ))
                                   (eql 0 (search "# " line))
                                   )))
                (:comment       #'toggle-comment)
                )
        `(:ml
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:comment       #'toggle-pascal-comment)
                )
        `(:pas
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:comment       #'toggle-pascal-comment)
                )
        `(:v
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:comment       #'toggle-comment)
                )
        `(:c
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:comment       #'toggle-c-comment)
                )
        `(:go
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:comment       #'toggle-c-comment)
                )
        `(:lua
                (:chapter       ,(lambda( line )      (search +chapter+ line)))
                (:section       ,(lambda( line )  (or (search +section+ line)
                                                      (search +chapter+ line))))
                (:comment       #'toggle-c-comment)
                )
        `(nil
                (:chapter       ,(lambda( line ) (search +chapter+ line)))
                (:section       ,(lambda( line ) (search +section+ line)))
                (:label         ,(lambda( line ) (search +label+   line)))
                (:comment       #'toggle-comment)
                )
  ))

(defun ftf( viewer name )
        "ftf: file-type-function"
        (second (assoc name (cdr(assoc (file.type-simplified path~) +fun-with-file-types+)))))

;;;---------------------------------------------------------------------------
;;; :section:           files: path

(defun path-location( path )
      (let*    ((name   (path.base path))
                (type   (path.ext  path))
                (path   (path.path path))
                (nick   (dirnick.nick (concat "REMOTE:" path)))
                (subdir ".")
               )
         (cond
           (nick        (setf path nick))
           (t           (let ((p (path.nodes path)))
                           (when (> (length p) 2)
                              (let* ((p-2    (subseq p 0 (1- (length p))))
                                     (nick-2 (dirnick.nick (concat "REMOTE:" (path.path (make-pathname :directory p-2)))))
                                     (sd     (elt p (1- (length p))))
                                    )
                                 (when nick-2
                                        (setf path   nick-2
                                              subdir sd)
                                              ))))))
         (values path subdir name type)))


;;;---------------------------------------------------------------------------
;;; :section:           files: load save ...

(defun text-load( viewer path )
  (setf (path viewer)           path)
  (setf (text viewer)           (file.read-lines path))
  (setf (upper-roll viewer)     nil)
  (setf (modifiedp viewer)      nil)
;;;  (say /"loaded   " path)
  )

(defun text-save( viewer )
    (when (and (modifiedp viewer) (not (path.= (path viewer) "")))
        (file.write (path viewer) (text-get viewer))
        (setf (modifiedp viewer) nil)
        (when (not (viewer.views-bookmarks-file-p viewer))   (say /"saved    " (path viewer)))
        ))

(defun change-file( viewer path )
  "v|u path -- // change-file: save current file, load new file"
  (unless (probe-file path) (err "change-file: file not found." path))
  (let ((viewer  (viewer.with-name viewer)))
  (unless (equal (path viewer) "")
          (mark.push viewer :changefile)
          (set-last-location viewer)
          (previous-files-insert (path viewer))
          (text-save viewer))
  (text-load viewer path)
  (mark.sort viewer)
  (mark.find viewer (mark.pop viewer :changefile))
  (rewrite-screen viewer)
  ))

(defun backup-file( viewer )
  (when (not (path.= path~ ""))
    (let ((path (concat path~ ".bak")))
      (say / path)
      (file.write path (text-get viewer))
      (say / "backup   " path)
      )))


;;;---------------------------------------------------------------------------
;;; :section:           files: previous (last visited files)
(defun previous-files-insert( path )
        (let* ((host    (host-namestring path))
               (dir     (directory-namestring path))
               (file    (file-namestring path))
               (path    (concat host ":" dir file))
              )
        (unless (equal file "pet-bookmarks.sit")
            (setf *previous-files*
                        (cons path
                                (subseq  (remove-if (lambda( p ) (equal p path))  *previous-files* )
                                         0 (1- +previous-files-max+))))
            (setf *previous-directories*
                        (sort (subseq (remove-duplicates
                                                (cons (concat host ":" dir) *previous-directories*)
                                       :test 'equal)
                               0  +previous-directories-max+)
                         #'string<=)))))

(defun previous-files-list()        *previous-files*)
(defun previous-directories-list()  (map 'list #'directory-namestring *previous-directories*))


;;;---------------------------------------------------------------------------
;;; :section:           command log
(defun logcmd( type name &rest args )     (push (list type name args) *cmdlog*))

(defun cmdlog-dump()
        (map nil (lambda( x ) (format t "~a~%" x))  (reverse (subseq *cmdlog* 0 25)))
        (say :> (length *cmdlog*) "commands logged." ))

(defun cmdlog-reset()  (setf *cmdlog* nil))

;;;===========================================================================
;;; :chapter: utf-8 character support
;;; :section:           character table
;;;---------------------------------------------------------------------------
(setf *char-table*  `(
        ( ,(code-char #x039b )     s-lambda             )
        ( ,(code-char #x03bb )     lambda               )
        ( ,(code-char #x019b )     lambda-stroked       )
        ( ,(code-char #x039b )     s-phi                )

        ( ,(code-char #x2336 )     apl-i-beam                   )
        ( ,(code-char #x2337 )     apl-squish-quad              )
        ( ,(code-char #x2338 )     apl-quad-equal               )
        ( ,(code-char #x2339 )     apl-quad-divide              )
        ( ,(code-char #x233a )     apl-quad-diamond             )
        ( ,(code-char #x233b )     apl-quad-jot                 )
        ( ,(code-char #x233c )     apl-quad-circle              )
        ( ,(code-char #x233d )     apl-circle-stile             )
        ( ,(code-char #x233e )     apl-circle-jot               )
        ( ,(code-char #x233f )     apl-slash-bar                )
        ( ,(code-char #x2340 )     apl-backslash-bar            )
        ( ,(code-char #x2341 )     apl-quad-slash               )
        ( ,(code-char #x2342 )     apl-quad-backslash           )
        ( ,(code-char #x2343 )     apl-quad-less-than           )
        ( ,(code-char #x2344 )     apl-quad-greater-than        )
        ( ,(code-char #x2345 )     apl-leftwards-vane           )
        ( ,(code-char #x2346 )     apl-rightwards-vane          )
        ( ,(code-char #x2347 )     apl-quad-leftwards-arrow     )
        ( ,(code-char #x2348 )     apl-quad-rightwards-arrow    )
        ( ,(code-char #x2349 )     apl-circle-backslash         )
        ( ,(code-char #x234a )     apl-down-tack-underbar       )
        ( ,(code-char #x234b )     apl-delta-stile              )
        ( ,(code-char #x234c )     apl-quad-down-caret          )
        ( ,(code-char #x234d )     apl-quad-delta               )
        ( ,(code-char #x234e )     apl-down-tack-jot            )
        ( ,(code-char #x234f )     apl-upwards-vane             )
        ( ,(code-char #x2350 )     apl-quad-upwards-arrow       )
        ( ,(code-char #x2351 )     apl-up-tack-overbar          )
        ( ,(code-char #x2352 )     apl-del-style                )
        ( ,(code-char #x2353 )     apl-quad-up-caret            )
        ( ,(code-char #x2354 )     apl-quad-del                 )
        ( ,(code-char #x2355 )     apl-up-tack-jot              )
        ( ,(code-char #x2356 )     apl-downwards-vane           )
        ( ,(code-char #x2357 )     apl-quote-downwards-vane     )
        ( ,(code-char #x2358 )     apl-quote-underbar           )
        ( ,(code-char #x2359 )     apl-delta-underbar           )
        ( ,(code-char #x235a )     apl-diamond-underbar         )
        ( ,(code-char #x235b )     apl-jot-underbar             )
        ( ,(code-char #x235c )     apl-circle-underbar          )
        ( ,(code-char #x235d )     apl-up-shoe-jot              )
        ( ,(code-char #x235e )     apl-quote-quad               )
        ( ,(code-char #x235f )     apl-circle-star              )
        ( ,(code-char #x2360 )     apl-quad-colon               )
        ( ,(code-char #x2361 )     apl-up-tack-diaeresis        )
        ( ,(code-char #x2362 )     apl-del-diaeresis            )
        ( ,(code-char #x2363 )     apl-star-diaeresis           )
        ( ,(code-char #x2364 )     apl-jot-diaeresis            )
        ( ,(code-char #x2365 )     apl-circle-diaeresis         )
        ( ,(code-char #x2366 )     apl-down-shoe-stile          )
        ( ,(code-char #x2367 )     apl-left-shoe-stile          )
        ( ,(code-char #x2368 )     apl-tilde-diaeresis          )
        ( ,(code-char #x2369 )     apl-greater-than-diaeresis   )
        ( ,(code-char #x236a )     apl-comma-bar                )
        ( ,(code-char #x236b )     apl-del-tilde                )
        ( ,(code-char #x236c )     apl-zilde                    )
        ( ,(code-char #x236d )     apl-stile-zilde              )
        ( ,(code-char #x236e )     apl-semicolon-underbar       )
        ( ,(code-char #x236f )     apl-quad-not-equal           )
        ( ,(code-char #x2370 )     apl-quad-question            )
        ( ,(code-char #x2371 )     apl-down-caret-tilde         )
        ( ,(code-char #x2372 )     apl-up-caret-tilde           )
        ( ,(code-char #x2373 )     apl-iota                     )
        ( ,(code-char #x2374 )     apl-rho                      )
        ( ,(code-char #x2375 )     apl-omega                    )
        ( ,(code-char #x2376 )     apl-alpha-underbar           )
        ( ,(code-char #x2377 )     apl-epsilon-underbar         )
        ( ,(code-char #x2378 )     apl-iota-underbar            )
        ( ,(code-char #x2379 )     apl-omega-underbar           )
        ( ,(code-char #x237a )     apl-alpha                    )
        ))

(defun chartable-dump()
        (mapc (lambda( rec )
                        (format t "  ~c  ~a~22T   ~s~%" (first rec) (second rec)  (first rec)))
              *char-table*)
        (say /(length *char-table*) "characters."))

(defun utf-insert-chars( viewer k l )
        (let* ((cs "")
               (above (above~ cy~))
               (below  (below~ (1- cy~)))
              )
          (dotimes (i   (1+ (- l k)))     (setf cs (concat cs (format nil " ~c" (code-char (+ k i))))))
          (set-text~ above cs below)
          ))

(defun insert-apl-chars( viewer )
  (declare (ignore viewer))
;;;     (utf-insert-chars #x2336 #x237a )                       ; apl
;;;     (utf-insert-chars #x2395 #x2395 )                       ; ×
;;;     (utf-insert-chars #x00f7 #x00f7 )                       ; ÷
;;;     (utf-insert-chars #x00d7 #x00d7 )                       ; ⎕
;;;     (utf-insert-chars #x0370 #x03ff )                       ; greek
;;;     (utf-insert-chars #x16a0 #x16f0 )                       ; runik
;;;     (utf-insert-chars #x27e6 #x27ef )                       ; math brackets
;;;     (utf-insert-chars #x1d400 #x1d7ff )                     ; math                  currently not available
(say"
ᚠ ᚡ ᚢ ᚣ ᚤ ᚥ ᚦ ᚧ ᚨ ᚩ ᚪ ᚫ ᚬ ᚭ ᚮ ᚯ ᚰ ᚱ ᚲ ᚳ ᚴ ᚵ ᚶ ᚷ ᚸ ᚹ ᚺ ᚻ ᚼ ᚽ ᚾ ᚿ ᛀ ᛁ ᛂ ᛃ ᛄ ᛅ ᛆ ᛇ
ᛈ ᛉ ᛊ ᛋ ᛌ ᛍ ᛎ ᛏ ᛐ ᛑ ᛒ ᛓ ᛔ ᛕ ᛖ ᛗ ᛘ ᛙ ᛚ ᛛ ᛜ ᛝ ᛞ ᛟ ᛠ ᛡ ᛢ ᛣ ᛤ ᛥ ᛦ ᛧ ᛨ ᛩ ᛪ ᛫ ᛬ ᛭ ᛮ ᛯ ᛰ
Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ ΢ Σ Τ Υ Φ Χ Ψ Ω
α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ ς σ τ υ φ χ ψ ω
⍶       ⍷       ⍸    ⟦ ⟧ ⟨ ⟩ ⟪ ⟫ ⟬ ⟭ ⟮ ⟯        ⍹
× ÷ ⎕ ⌶ ⌷ ⌸ ⌹ ⌺ ⌻ ⌼ ⌽ ⌾ ⌿ ⍀ ⍁ ⍂ ⍃ ⍄ ⍅ ⍆ ⍇ ⍈ ⍉ ⍊ ⍋ ⍌ ⍍ ⍎ ⍏ ⍐ ⍑ ⍒
⎕
⍓ ⍔ ⍕ ⍖ ⍗ ⍘ ⍙ ⍚ ⍛ ⍜ ⍝ ⍞ ⍟ ⍠ ⍡ ⍢ ⍣ ⍤ ⍥ ⍦ ⍧ ⍨ ⍩ ⍪ ⍫ ⍬ ⍭ ⍮ ⍯ ⍰ ⍱ ⍲"))


;;;===========================================================================
;;; :chapter: command functions
;;; :section:           insert char
;;;---------------------------------------------------------------------------
(print :---command-functions)

(defun do-nothing( viewer )   )

(defun insert-char( viewer c )
  (let* ((above         (above~ cy~))
         (line          (line~  cy~))
         (below         (below~ cy~))
         (head          (head line  cx~))
         (tail          (tail line  cx~))
         (spaces        (spaces (max 0 (- cx~ (length head)))))          ; in case of far right
         (newline       (concat head spaces (list c) tail))
         )
    (set-text~   above newline below)
    (incf cx~)))

(defun overwrite-char(  viewer c )
  (let* ((above         (above~  cy~))
         (line          (line~   cy~))
         (below         (below~  cy~ ))
         (head          (head line cx~))
         (tail          (tail line (1+ cx~)))
         (spaces        (spaces (max 0 (- cx~ (length head)))))          ; in case of far right
         (newline       (concat head spaces (list c) tail))
         )
       (set-text~   above newline below)
       (incf cx~)))

(defun insert-newline( viewer )
  (let* ((above (above~ cy~))
         (line  (line~  cy~))
         (below (below~ cy~))
         (i     (indentation line))
         (sps   (if (> cx~ i)  i cx~))
         )
    (set-text~   above  (head line cx~)   (concat (spaces sps) (tail line cx~))   below)
    (incf cy~)
    (setf cx~ i)
    (when (> cy~ lines~) (scroll-down-1 viewer))
    ))

(defun insert-indented-newline( viewer )
  (let* ((above (above~ cy~))
         (line  (line~  cy~))
         (below (below~ cy~))
         )
    (set-text~   above  (head line cx~)  (concat (spaces cx~) (tail line cx~)) below)
    (incf cy~)
  ))

(defun join-with-prev-line( viewer )                                    ; retain no spaces
  (let* ((above         (above~ (1- cy~)))
         (head          (despace       (line~ (1- cy~))))
         (tail          (despace-left  (line~ cy~)))
         (below         (below~ cy~))
         )
    (set-text~   above (asline head tail) below)
    (setf cy~ (1- cy~))
    (setf cx~ (length head))
    ))

(defun join-with-next-line( viewer )                                    ; retain spaces
  (let* ((above         (above~ cy~))
         (head          (despace       (line~ cy~)))
         (tail          (despace-left  (line~ (1+ cy~))))
         (below         (below~ (1+ cy~)))
         )
    (set-text~   above   (asline head  (spaces (max 1 (- cx~ (length head))))  tail)   below)
    )   )

(defun insert-tab( viewer )
  (let* ((line  (line~  cy~))
         (head  (head line cx~))
         (tail  (tail line cx~))
         (n     (let ((m  (mod cx~ 8))) (if (= m 0) 8 (- 8 m))))
         )
    (set-line~   (asline head (spaces n) tail))
    (incf cx~ n)
    ))

(defun backspace-cmd( viewer )
  (cond
    ((> cx~ 0)          (if insertp~
                                (delete-char-fn viewer cy~ (1- cx~))
                                (replace-char-fn viewer cy~ (1- cx~) #\space))
                        (decf cx~)
                        (nextdrawis :thisline))
    ((> cy~ 1)           (let ((above (above~ (1- cy~)))
                              (head  (line~  (1- cy~)))
                              (tail  (line~  cy~))
                              (below (below~ cy~ ))
                             )
                            (set-text~ above (concat head tail) below)
                            (setf cx~ (length head))
                            (setf cy~ (1- cy~))))
    (t                  nil)))


(defun delete-cmd( viewer )
  (let ((line   (line~ cy~)))
     (cond
       ((>= cx~ (length line))   (let ((above      (above~ cy~))
                                      (nextline   (line~  (1+ cy~)))
                                      (below      (below~ (1+ cy~)))
                                      (n          (- cx~ (length line))))
                                    (set-text~  above  (asline  line (spaces n) (despace-left nextline)) below)))
       (t    (delete-char-fn viewer cy~  cx~)
             (nextdrawis :thisline)))))


(defun rotate-character( c )
        (case c
          (#\/  #\|)(#\|  #\\)(#\\  #\/)
          (#\!  #\?)(#\?  #\!)
          (#\=  #\#)(#\#  #\-)(#\- #\=)

          (#\"  #\')(#\'  #\`)(#\` #\")
          (#\&  #\%)(#\%  #\$)(#\$ #\&)

;;;       (#\0  #\1)(#\1  #\2)(#\2  #\3)(#\3  #\4)(#\4  #\5)(#\5  #\6)(#\6  #\7)(#\7  #\8)(#\8  #\9)(#\9  #\0)

;;;       (#\a  #\ä)(#\ä  #\a)
;;;       (#\U  #\Ü)(#\Ü  #\U)
;;;       (#\A  #\Ä)(#\Ä  #\A)
;;;       (#\O  #\Ö)(#\Ö  #\O)
;;;       (#\u  #\ü)(#\ü  #\u)
;;;       (#\o  #\ö)(#\ö  #\o)
;;;       (#\s  #\ß)(#\ß  #\s)
;;;       (#\C  #\Γ)(#\Γ #\C)

;;;       (#\a  #\A)(#\A  #\Ä) (#\Ä  #\ä) (#\ä  #\a)
          (#\a  #\A)(#\A  #\ä) (#\ä  #\Ä) (#\Ä  #\a)
          (#\b  #\B)(#\B  #\b)
          (#\c  #\C)(#\C  #\c)
          (#\d  #\D)(#\D  #\d)
          (#\e  #\E)(#\E  #\e)
          (#\f  #\F)(#\F  #\f)
          (#\g  #\G)(#\G  #\g)
          (#\h  #\H)(#\H  #\h)
          (#\i  #\I)(#\I  #\i)
          (#\j  #\J)(#\J  #\j)
          (#\k  #\K)(#\K  #\k)
          (#\l  #\L)(#\L  #\l)
          (#\m  #\M)(#\M  #\m)
          (#\n  #\N)(#\N  #\n)
;;;       (#\o  #\O)(#\O  #\Ö) (#\Ö  #\ö) (#\ö  #\o)
          (#\o  #\O)(#\O  #\ö) (#\ö  #\Ö) (#\Ö  #\o)
          (#\p  #\P)(#\P  #\p)
          (#\q  #\Q)(#\Q  #\q)
          (#\r  #\R)(#\R  #\r)
          (#\s  #\S)(#\S  #\s)
          (#\t  #\T)(#\T  #\t)
;;;       (#\u  #\U)(#\U  #\Ü) (#\Ü  #\ü) (#\ü  #\u)
          (#\u  #\U)(#\U  #\ü) (#\ü  #\Ü) (#\Ü  #\u)
          (#\v  #\V)(#\V  #\v)
          (#\w  #\W)(#\W  #\w)
          (#\x  #\X)(#\X  #\x)
          (#\y  #\Y)(#\Y  #\y)
          (#\z  #\Z)(#\Z  #\z)

          (#\1  #\:)(#\:  #\1)
          (#\2  #\,)(#\,  #\2)
          (#\3  #\.)(#\.  #\3)

          (otherwise    c)
          ))

(defun rotate-last-character( viewer )
   (let* ((above        (above~ cy~))
          (line         (line~  cy~))
          (below        (below~ cy~))
          (head         (head line (1- cx~)))
          (c            (elt  line (1- cx~)))
          (tail         (tail line     cx~))
          (newline      (concat head  (list (rotate-character c)) tail))
          )
       (set-text~  above newline below)
       ))

;;;---------------------------------------------------------------------------
;;; :section:           jmp

(defun jmp-left-1( viewer )   (when (> cx~ 0)   (decf cx~))     (nextdrawis :thisline))
(defun jmp-right-1( viewer )  (incf cx~)                        (nextdrawis :thisline))

(defun jmp-up-1( viewer )
    (cond
        ((> cy~ 1)                       (decf cy~))
        (t                              (when (upper-roll viewer) (roll-down viewer 1)))))

(defun jmp-down-1( viewer )
    (cond
        ((>= cy~ (length text~))          )                                       ; do nothing
        ((>= cy~ (lines viewer))          (when (bottom-roll-p viewer)   (roll-up viewer 1)))   ; scroll down if
        (t                              (incf cy~))))                            ; jump down


(defun jmp-line-start( viewer )         ; better with horizontal scrolling?
  (let ((indentation   (indentation (line viewer cy~))))
     (setf cx~ (cond
                ((= cx~ indentation)    0)
                (t                     indentation)
                ))))

(defun jmp-line-end( viewer )
    (let ((line  (despace (line~ cy~))))
        (set-text~  (above~ cy~)  line  (below~ cy~))
        (setf cx~   (length line))))


(defun jmp-text-start( viewer )
        (setf   cy~  1
                cx~  0
                text~           alltext~
                upper-roll~     nil))

(defun jmp-text-end( viewer )
  (let* ((txt   alltext~)
         (la    (length txt))
         (lt    (min (lines viewer) la)))
    (setf cy~           (min lines~ la)
          cx~           0
          upper-roll~   (reverse(subseq txt  0   (- la lt)))
          text~         (subseq txt  (- la lt)))
    ))

(defun jmp-prev-tab( viewer )   (setf cx~  (max   0 (* 8 (-  (truncate cx~ 8) (if (= (mod cx~ 8) 0) 1 0))))))
(defun jmp-next-tab( viewer )   (setf cx~  (min 104 (* 8 (1+ (truncate cx~ 8))))))

(defun jmp-prev-word-start( viewer )
  (let* ((line   (line~ cy~))
         (x      (find-prev-word-start line (if (at-word-start-p line cx~) (max 0 (1- cx~)) cx~)))
        )
    (when x (setf cx~ x))
    ))

(defun jmp-next-word-end( viewer )
  (let* ((line   (line~ cy~))
         (x      (find-next-word-end line cx~))
        )
      (when x (setf cx~ x))
      ))

(defun jmp-top-of-screen( viewer )
        (setf cy~ 1
              cx~ 0))


;;; :section:           scroll
;;;---------------------------

(defun scroll-up-1( viewer )
    (cond
      ((upper-roll viewer)
                (roll-down viewer 1)
                (when (< cy~ (lines viewer))   (incf cy~))
                (unless (upper-roll viewer)  (rewrite-screen viewer)))
      (t  (nextdrawis :nowt))))

(defun scroll-down-1( viewer )
    (cond
      ((> (length text~) (min lines~ 10))
                (roll-up viewer 1)
                (when (> cy~ 1)   (decf cy~))
                (when (<= (length text~) (min lines~ 10))  (rewrite-screen viewer)))
      (t   (nextdrawis :nowt))))

(defun scroll-up( viewer  i )   (dotimes (j i) (scroll-up-1 viewer)))
(defun scroll-down( viewer  i ) (dotimes (j i) (scroll-down-1 viewer )))

(defun scroll-left(  viewer i )
        (setf hx~ (max 0 (- hx~ i)))
        (when (>= cx~ (+ hx~ (cols viewer))) (setf cx~ (1- (+ hx~ (cols viewer)))))
        )

(defun scroll-right( viewer  i )
        (setf hx~ (+ hx~ i))
        (when (> hx~ cx~) (setf cx~ hx~))
        )

;;;-----------------------------------
;;; :section:           paren matching

(defun jmp-matching-paren( viewer )
  (let* ((line   (line~ cy~)))
    (labels ((one-line-back()
                      (cond
                          ((> cy~ 1)            (decf cy~))
                          (t                    (scroll-up viewer 1)    (setf cy~ 1)))
                      (setf line (line~ cy~)))

             (one-line-forth()
                      (cond
                          ((< cy~ lines~)       (incf cy~))
                          (t                    (scroll-down viewer 1)  (setf cy~ lines~)))
                      (setf line (line~ cy~)))

             (forward( i n )
                  (loop
                      (incf i)
                      (cond
                        ((>= i (length line))
                             (cond
                                ((< cy~ (length text~))         (one-line-forth) (setf i -1))
                                (t                              (return nil))))
                        ((and (char-at= line i +c+) (= n 0))    (return i))
                        ((char-at= line i +o+  )                (incf n))
                        ((char-at= line i +c+  )                (decf n))
                        (t                                      )  ; inc i
                        )))

             (backward( i n )
                  (loop
                      (decf i)
                      (cond
                        ((< i 0)
                             (cond
                                ((or (> cy~ 1) upper-roll~)     (one-line-back) (setf i (length line)))
                                (t                              (return nil))))
                        ((and (char-at= line i +o+) (= n 0))    (return i))
                        ((char-at= line i +o+  )                (incf n))
                        ((char-at= line i +c+  )                (decf n))
                        (t                                      ) ; dec i
                        )))
             )
      (mark.push viewer :sys)
      (let ((x (cond
                ((>= cx~ (length line))       (say /"Cursor is not on a parenthesis.") nil)
                ((char-at= line cx~ +o+ )     (forward  cx~ 0))
                ((char-at= line cx~ +c+ )     (backward cx~ 0))
                (t                           (say /"Cursor is not on a parenthesis.") nil)
                )))
        (cond
          (x    (mark.pop viewer :sys)   (setf cx~ x))
          (t    (mark.find viewer (mark.pop viewer :sys))
                (say /"jmp-matching-paren: No matching parenthesis found."))
                )))))


(defun jmp-prev-sexp( viewer )
  (let ((line     (line~ cy~)))
    (labels ((one-line-back()
                        (cond
                          ((> cy~ 1)     (decf cy~))
                          (t             (scroll-up viewer 1)  (setf cy~ 1)))
                        (setf line (line~ cy~)))

             (find-start( i )
                        (cond
                         ((< i 0)
                                (cond
                                    ((or (> cy~ 1) upper-roll~) (one-line-back)(find-start (1- (length line))))
                                    (t                          nil)))
                         ((char-at= line i +o+)
                                                                nil)
                         ((char-at= line i +c+)                 i)
                         (t                                     (find-start (1- i)))
                         ))
            )
      (let ((start (find-start (idxin0 line (1- cx~)))))
        (cond
          (start                (setf cx~ start)
                                (jmp-matching-paren viewer))

          (t                    (say /"Next Parenthesis to left is a opening one."))
        )))))


(defun jmp-next-sexp( viewer )
  (let ((line (line~ cy~)))
    (labels ((one-line-forth()
                        (cond
                          ((< cy~ lines~)                 (incf cy~))
                          (t                              (scroll-down viewer 1) (setf cy~ lines~)))
                        (setf line (line~ cy~))
                        )
             (find-start( i )
                        (cond
                         ((>= i (length line))
                                (cond
                                        ((< cy~ (length text~))   (one-line-forth)  (find-start 0))
                                        (t                      nil)))
                         ((char-at= line i +o+)                 i)
                         ((char-at= line i +c+)                 nil)
                         (t                                     (find-start (1+ i)))
                         )))
      (mark.push viewer :sys)
      (let ((start (find-start (idxin line cx~))))
        (cond
          (start                (setf cx~ start)
                                (jmp-matching-paren viewer)
                                (setf cx~ (1+ cx~))
                                (mark.pop viewer :sys))

          (t                    (say /"Next Parenthesis to right is a closing one.")
                                (mark.find viewer (mark.pop viewer :sys)))
          )))))

;;;---------------------------------------------------------------------------
;;; :section:           delete copy move

(defun delete-word( viewer )
  "delete the larger region indicated by [Ctrl-left Ctrl-right] and [set-mark Ctrl-left]"
  (let* ((line  (line~ cy~))
         (x     (find-prev-word-start line (if (at-word-start-p line cx~) (1- cx~) cx~))))
    (when x
       (let ((head  (head line   x))
             (tail  (tail line   (max cx~ (find-next-word-end line x)))))
          (set-line~   (asline head tail))
          (setf cx~ x)
          ))))

(defun delete-whitespace( viewer )
  (let* ((line   (line~  cy~)))
     (set-line~   (asline (head line cx~) (despace-left (tail line cx~))))
     ))

(defun delete-line( viewer )
        (set-text~   (above~ cy~) (below~ cy~)))

;;;(defun delete-line-head( viewer )
;;;     (set-line~   (tail (line~ cy~) cx~))
;;;     (setf cx~ 0))

(defun delete-line-head( viewer )
        (let* ((line     (line~ cy~))
               (indent   (indentation line)))
           (cond
             ((eql indent cx~)
                (set-line~  (tail line cx~))
                (setf cx~ 0))
             (t
                (set-line~   (concat (spaces indent) (tail line cx~)))
                (setf cx~ indent)))))

(defun delete-line-tail( viewer )
        (set-line~   (head (line~ cy~) cx~)))

(defun copy-line( viewer )
  (let* ((line  (line~ cy~)))
    (set-text~   (above~ cy~)  line (copy-seq line) (below~ cy~))
    ))

(defun move-line-up( viewer )
  (let* ((prev  (line~ (1- cy~)))
         (line  (line~ cy~))
         )
    (set-text~ (above~ (1- cy~)) line prev (below~ cy~))
    (setf cy~ (1- cy~))
    ))

(defun move-line-down( viewer )
  (let* ((line  (line~ cy~))
         (next  (line~ (1+ cy~)))
         )
    (set-text~ (above~ cy~) next line (below~ (1+ cy~)))
    (setf cy~ (1+ cy~))
    ))

(defun move-line-tail-up( viewer )
        (roadwork :move-line-tail-up)
        )

(defun move-line-tail-down( viewer )
        (roadwork :move-line-tail-down)
        )

;;;---------------------------------------------------------------------------
;;; :section:           jmp-chapter/section/label
;;; jmp-top-of-screen happens in mouse-...

(defun jmp-prev-chapter( viewer )
  (let ((y  (backward-find viewer (+ (length (upper-roll viewer)) cy~ -2) (ftf viewer :chapter))))
        (if y
                (jmp-to-position-fn viewer  y 0)
                (jmp-text-start viewer)
                )))

(defun jmp-next-chapter( viewer )
  (let ((y  (forward-find viewer (+ (length (upper-roll viewer)) cy~ ) (ftf viewer :chapter))))
        (if y
                (jmp-to-position-fn viewer  y 15)
                (jmp-text-end viewer)
                )))


(defun jmp-prev-section( viewer )
  (let ((y  (backward-find viewer (+ (length (upper-roll viewer)) cy~ -2) (ftf viewer :section))))
        (if y
                (progn (jmp-to-position-fn viewer  y 0) (scroll-up viewer 1))
                (jmp-text-start viewer)
                )))

(defun jmp-next-section( viewer )
  (let ((y  (forward-find viewer (+ (length (upper-roll viewer)) cy~ ) (ftf viewer :section))))
        (if y
                (progn (jmp-to-position-fn viewer  y 15)  (scroll-up viewer 1))
                (jmp-text-end viewer)
                )))


(defun jmp-prev-label( viewer )
  (let ((y  (backward-find viewer (+ (length (upper-roll viewer)) cy~ -2) (ftf viewer :label))))
        (jmp-to-position-fn viewer  y 0)))

(defun jmp-next-label( viewer )
  (let ((y  (forward-find viewer (+ (length (upper-roll viewer)) cy~ ) (ftf viewer :label))))
        (jmp-to-position-fn viewer  y 15)))


;;;---------------------------------------------------------------------------
;;; :section:           find-cmds

(defun find-push-command( viewer )
        "// read string to find from text."
        (let ((tap (thing-at-point viewer)))
           (find-push tap)
           (say / (concat "(pet:find-push   \"" tap  "\")"))
           (mark.swap viewer)))

(defun find-prev( viewer )
  (let ((str  (find-string-value)))
;;;     (say /"find-prev: " str)
     (when str
        (multiple-value-bind (y x)
              (backward-find viewer( + (length (upper-roll viewer)) cy~ -2) (lambda( line ) (search str line)))
           (jmp-to-position-fn viewer y x)
           (if y (scroll-up viewer 8) (say-not-found (find-string-value)))
           ))))

(defun find-next( viewer )
  (let ((str  (find-string-value)))
;;;     (say /"find-next: " str)
     (when str
        (multiple-value-bind (y x)
              (forward-find viewer (+ (length (upper-roll viewer)) cy~   ) (lambda( line ) (search str line)))
           (when y  (jmp-to-position-fn viewer y (+ (length str) x)))
           (if y (scroll-up viewer 8) (say-not-found (find-string-value)))
           ))))

(defun find-definition( viewer )
   (let ((str (concat +label+ (find-push (thing-at-point viewer)))))
     (say  /"find-definition: <" str ">")
     (when str
        (multiple-value-bind (y)   (forward-find viewer  0  (lambda( line ) (search str line)))
           (jmp-to-position-fn viewer y 0)
           (if  y
                (mark.push viewer :user)
                (say-not-found (find-string-value)))
           ))))

(defun print-definition( viewer )
  (let ((str (concat +label+ (thing-at-point viewer))))
     (say "print-definition: <" str ">" /)
     (when str
        (multiple-value-bind (y)   (forward-find viewer 0 (lambda( line ) (search str line)))
           (when y  (let ((txt alltext~))
                (loop for  i from 0 to 25
                        until (or (>= (+ y i) (length txt)) (equal (elt txt (+ i y -1)) ""))
                        do (say / (elt txt (+ y i -1)))
                )))))))

(defun find-all( viewer )
  "print all lines containing thing-at-points defintion name"
  (let ((str (thing-at-point viewer))
        (ln 0)
        )
    (say / "find-all: <" str ">")
    (find-push str nil)
    (when str
        (dolist (l alltext~)
                (when (search str l) (say / ln :> l))
                (incf ln)
                ))))

(defun find-prev-x-mark( viewer )
        ""
        (let ((str (xmark.text viewer)))
           (multiple-value-bind (y x)
                (backward-find viewer (+ (length (upper-roll viewer)) cy~ -2)  (lambda( l ) (search str l)))
              (jmp-to-position-fn viewer y x)
              (if y (scroll-up viewer 8) (say-not-found str)))))

(defun find-next-x-mark( viewer )
        ""
        (let ((str (xmark.text viewer)))
           (multiple-value-bind (y x)
                (forward-find viewer (+ (length (upper-roll viewer)) cy~   )  (lambda( l ) (search str l)))
              (jmp-to-position-fn viewer y x)
              (if y (scroll-up viewer 8) (say-not-found str)))))


;;;---------------------------------------------------------------------------
;;; :section:           switch to bookmarks

(defun switch-to-bookmarks(  viewer search-str )                                                ; X-mark-X
  (let* ((p             (pathname (path viewer)))
         (dir           (directory-namestring p))
         (file          (file-namestring p))
        )
  (labels (
        (fmt( dir )
                (let ((dirnick  (dirnick.nick dir)))
                (if dirnick     (concat ":"(symbol-name dirnick))
                                (concat "\""  (concat ""dir)  "\""))    ; locate-1
                                ))
        (fmt-locate-1( dir &optional file section )
                (concat "(locate-1 "      (fmt dir)
                                        (if file        (concat "   \"" file    "\"")    "")
                                        (if section     (concat "   \"" section "\"")    "")
                                        ")" )
                )
        (fmt-locate( dir file )
                (multiple-value-bind (path subdir) (path-location dir)
                (if (symbolp path) (setf path (dirnick.nick-show path)))
                (if file
                    (concat
                        "    (locate\" "
                        (justify-left 94 (concat
                                (justify-left 20   (concat (justify-right 14 path) " "subdir ))
                                " "
                                (justify-right 10  (pathname-type file))
                                " "
                                                   (pathname-name file)
                                ))
                                "\")"  )
                   (concat
                        "    (locate\" "
                        (justify-left 94   (concat (justify-right 14 path) " "subdir ))
                        "\")"  )
                )))
        (previous-dirs()
                (map 'list (lambda( pathname )
                                  (let* ((p             (pathname pathname))
                                         (dir           (directory-namestring p))
                                        )
                                        (fmt-locate dir nil)
                                        ))
                        (sort
                            (remove-duplicates
                                (concatenate 'list (previous-directories-list) (dirnick.directories))
                                :test #'equal)
                            #'string<=)
                        ))
        (previous-files()
                (map 'list (lambda( pathname )
                                  (let* ((p             (pathname pathname))
                                         (dir           (directory-namestring p))
                                         (file          (file-namestring p))
                                        )
                                        (fmt-locate dir file)
                                        ))
                        (previous-files-list)
                        ))
        (contents()
           (if (equal "## Sections" search-str)
              (let* ((cont nil))
              (dolist (l alltext~)
                (when (or (funcall (ftf viewer :section) l)
                          (funcall (ftf viewer :chapter) l))
                    (push   (fmt-locate-1 dir file l)  cont)))
                (reverse cont))
              (list "Contents was not requested.")))
        (contents-short()
           (if (equal "## Sections" search-str)
              (let* ((cont nil))
              (dolist (l alltext~)
                (when (funcall (ftf viewer :chapter) l)
                    (push   (fmt-locate-1 dir file l)  cont)))
                (reverse cont))
              (list "Short contents was not requested.")))
        ) ; labels
        (let ((contents (contents))
              (contents-short (contents-short))
              (previous-dirs (previous-dirs))
              (tablines nil)
              )
        (show viewer "pet-bookmarks.sit")

        ; rewrite Tabs
        (jmp-text-start viewer)


        (let ((y (forward-find viewer 0 (lambda( line ) (search "## Previous" line)))))
          (when y
                (jmp-to-position-fn viewer (+ y 2) 0)
                (let* ((start    cy~)
                       (start    (+ 2 (find-next-empty-line viewer start)))
                       (end      (find-next-empty-line viewer   start))
                       (ll       (format-location   *last-location*))
                      )
                   (setf tablines (subseq (text viewer)  (- start 1) (- end 1)))
                   (setf tablines (remove-if (lambda( l ) (equal l ll)) tablines))
                   (push ll tablines)
                   (setf-to-maxlen +tablines-max+ tablines)
   ;;;                (setf tablines (sort tablines #'string<))                    ; bad idea without timestamp
                   )))



        ; drop everything behind Autogen
        (jmp-text-start viewer)
        (let ((y (forward-find viewer  0 (lambda( line ) (search "; Autogen" line)))))
          (jmp-text-start viewer)
          (when y
            (set-text~  (subseq (text viewer)  0 (- y 2 (length (upper-roll viewer)))))))

        ; concat new text
        (set-text~ (append
                        (text viewer)
                        (list   ""
                                "; Autogen (Don't edit the following lines.)"
                                "; Autogen (Don't edit the following lines.)"
                                "## Previous"
                                "")
                        (previous-files)
                        (list   "")
                        (list   "")
                        tablines
                        (list   "")
                        (list   "")
                        previous-dirs
                        (list   ""
                                ""
                                (concat "## Sections   " file )
                                "")
                        contents
                        (list   ""
                                ""
                                (concat "## Chapters   " file )
                                "")
                        contents-short
                        (list   ""
                                "; file-end"
                                "")))
        )
        (let ((y (forward-find viewer  0  (lambda( line ) (search search-str line )))))
          (when y (cond
           ((equal "## Previous" search-str)
;;;         (say / :go-to-previous)
            (jmp-to-position-fn viewer y 0)
            (jmp-down-1 viewer)
            (jmp-down-1 viewer)
            (jmp-down-1 viewer)
            (setf cx~ 44)
            )
           ((equal "## Sections" search-str)
            (jmp-to-position-fn viewer y 0)
            (let ((y2 (forward-find viewer y (lambda( line ) (search (fifth *last-location*)   line)))))
               (dotimes (i (- y2 y)) (jmp-down-1 viewer))
               (jmp-line-end viewer)
               ))
           ((equal "## Bookmarks" search-str)
            (jmp-to-position-fn viewer y 0)
            )
           (t   (say / "no attempt to jmp"))
           )))
        ) ; labels
        (setf (modifiedp viewer) t)      ; in order to save tabs
        (rewrite-screen viewer)
        ))


(defun scroll-lock-cmd( viewer )
  (if (viewer.views-bookmarks-file-p viewer)
      (exec-line viewer)
      (switch-to-bookmarks viewer "## Sections")))


(defun pause-cmd( viewer )
  (if (viewer.views-bookmarks-file-p viewer)
      (exec-line viewer)
      (switch-to-bookmarks viewer "## Previous")))


(defun ctrl-scroll-lock-cmd( viewer )
  (if (viewer.views-bookmarks-file-p viewer)
      (let ((f (first (previous-files-list))))
        (locate-1 (directory-namestring f) (file-namestring f)))
      (say / "not in pet-bookmarks")))


;;;---------------------------------------------------------------------------
;;; :section:           repl

(defun switch-to-repl( viewer )
        "s|nil --  // switch to repl"
        (show viewer "pet-repl.sit")
        )

;;;---------------------------------------------------------------------------
;;; :section:           locate

(defun locate-1( dirnick &optional file section )
        (let ((viewer    (viewers.get-last-active))
              (dir  (cond
                      ((symbolp dirnick)        (dirnick.dir dirnick))
                      ((stringp dirnick)        dirnick)
                      (t                        (err "locate-1: string or symbol for dirnick expected."))
                      )))
             (if (not dir)
                (err "locate-1: got no valid dir for dirnick: " dirnick)
                (if (not file)
                        (filer-spawn dir)
                        (let ((path (concat "" dir file)))
;;;                         (mark.push viewer :locate-1)
                            (change-file viewer path)
                            (cond
                              (section
                                (multiple-value-bind ( y )
                                                (forward-find viewer 0   (lambda( line ) (search section line)))
                                        (if y
                                                (progn  (jmp-to-position-fn viewer y 0)
                                                        (scroll-up viewer 1))
                                                (progn
                                                        (say-not-found section)
;;;                                                     (marks-sort viewer)
;;;                                                     (mark.find viewer (mark.pop viewer))
                                                ))))
                              (t
;;;                             (marks-sort viewer)
;;;                             (mark.find viewer (mark.pop viewer))
                                ))
                            (rewrite-screen viewer)
                            )))))

(defun locate(  parameter-string )
        (let*  ((viewer         (viewers.get-last-active))
                (vbar-position  (position #\| parameter-string))
                (chunks         (chunks (subseq parameter-string 0 (if vbar-position vbar-position nil))))
                (section        (if vbar-position   (subseq parameter-string (1+ vbar-position))   nil))
                (section        (if section (despace section) nil))
                (section        (if (equal section "") nil section))
                (path           (first  chunks))
                (path-2         (dirnick.dir (dirnick.nick-read path)))
                (path           (if path-2 path-2 path))
                (subdir         (second chunks))
                (subdir         (if (equal subdir ".")  "" (concat subdir "/")))
                (ext            (third  chunks))
                (name           (fourth chunks))
                (full-path      (concat "" path subdir  (if name (concat name "." ext) ext)))
                )
;;;         (mark.push viewer :locate)
            (cond
              (ext
                (change-file viewer full-path)
                (cond
                  (section
                    (let (( y       (forward-find viewer 0   (lambda( line ) (search section line)))))
                     (if y
                      (progn  (jmp-to-position-fn viewer y 0)
                              (scroll-up viewer 1)
                              (jmp-down-1 viewer))
                      (progn
                        (say-not-found section)
;;;                                             (marks-sort viewer)
;;;                                             (mark.find viewer (mark.pop viewer))
                        ))))
                  (t
;;;                 (marks-sort viewer)
;;;                 (mark.find viewer (mark.pop viewer :locate))
                    )))
              (t
                (filer-spawn (concat "" path subdir))
                ))
            (rewrite-screen viewer)
            ))


;;;---------------------------------------------------------------------------
;;; :section:           others

(defun mark-push-cmd( viewer )     (mark.push viewer :user))

(defun mark-find-cmd( viewer )   (mark.find viewer (mark.rotate viewer)))


(defun toggle-char-case( viewer )
  (if (zerop cx~)                                                               ; at line start
        (set-line~  (asline  (toggle-char-case-fn (line~ cy~))))                ;       toggle line
        (let*  ((line  (line~ cy~))                                             ;  else toggle word-head
                (x     (find-prev-word-start line cx~))
                (word  (toggle-char-case-fn (body line x cx~)))
               )
           (set-line~   (asline  (head line x)  word  (tail line cx~)))
           )))

(defun toggle-pascal-comment( viewer )
  (let* ((line (line~ cy~))
         (has-comment  (equal "(*" (take 2 line)))                                      ;-)
         (has-spaces   (equal "  " (take 2 line)))
         (head  (if has-comment
                    "  "
                    "(*"))                                                              ;-)
         (tail  (if has-comment
                    (subseq line (idxin line 2)  (idxin line (- (length line) 2)))
                    (concat (if has-spaces   (subseq line (idxin line 2))   line)  (spaces (- (if has-spaces 88 86) (length line))) "     *)")))    ; (
         )
       (set-line~  (asline head tail))
       (incf cy~)
       ))

(defun toggle-c-comment( viewer )
  (let* ((line (line~ cy~))
         (has-comment  (equal "/*" (take 2 line)))                                      ;-)
         (has-spaces   (equal "  " (take 2 line)))
         (head  (if has-comment
                    "  "
                    "/*"))                                                              ;-)
         (tail  (if has-comment
                    (subseq line (idxin line 2)  (idxin line (- (length line) 2)))
                    (concat (if has-spaces   (subseq line (idxin line 2))   line)  (spaces (- (if has-spaces 88 86) (length line))) "     */")))    ; (
         )
       (set-line~  (asline head tail))
       (incf cy~)
       ))

(defun toggle-v-comment( viewer )
  (let* ((line (line~ cy~))
         (has-comment  (equal "// " (subseq line 0 (idxin line 3))))
         (has-spaces   (equal (if has-comment "   ""      ")
                                (subseq line   (idxin line   (if has-comment 3 0))   (idxin line 6))))
         (head  (if has-comment
                    (if has-spaces   "   "   "")
                    "// "))
         (tail  (if has-comment
                    (subseq line (idxin line 3))
                    (if has-spaces   (subseq line (idxin line 3))   line)))
         )
       (set-line~  (asline head tail))
       (incf cy~)
       ))

(defun toggle-comment( viewer )
  (when (eql :pas (file.type-simplified path~))  (toggle-pascal-comment viewer) (return-from toggle-comment))
  (when (eql :ml  (file.type-simplified path~))  (toggle-pascal-comment viewer) (return-from toggle-comment))
  (when (eql :v   (file.type-simplified path~))  (toggle-v-comment viewer) (return-from toggle-comment))
  (when (eql :c   (file.type-simplified path~))  (toggle-c-comment viewer) (return-from toggle-comment))
  (let* ((line (line~ cy~))
         (has-comment  (equal ";;;" (subseq line 0 (idxin line 3))))
         (has-spaces   (equal (if has-comment "   ""      ")
                                (subseq line   (idxin line   (if has-comment 3 0))   (idxin line 6))))
         (head  (if has-comment
                    (if has-spaces   "   "   "")
                    ";;;"))
         (tail  (if has-comment
                    (subseq line (idxin line 3))
                    (if has-spaces   (subseq line (idxin line 3))   line)))
         )
       (set-line~  (asline head tail))
       (incf cy~)
       ))


(defun indent-more( viewer )
        (set-line~  (asline "        " (line~ cy~)))
        (incf cy~))

(defun indent-less( viewer )
    (let* ((line (line~ cy~))
           (i    (indentation line)))
        (set-line~  (tail line (min i 8)))
        (incf cy~)))

(defun window-maximize-vertical( viewer )
  (let ((xpos  (mezzano.gui.compositor:window-x window~)))
    (mezzano.gui.compositor:move-window window~ xpos  0)
    (multiple-value-bind   (width height)   (mezzano.gui.compositor::screen-dimensions)
    (declare (ignore width))
    (let ((surface  (mezzano.gui:make-surface
                                (editor-win.width  cols~)
                                height
                                )))
        (mezzano.gui.widgets:resize-frame     frame~   surface)
        (mezzano.gui.compositor:resize-window window~  surface)
        (editor-win.height height)
        (setf lines~   (- (truncate (- height 4) +line-height+) 2)
              cols~    cols~)
    ))))


(defun window-original-size( viewer )
    (mezzano.gui.compositor:move-window  window~ bwin-xpos~  bwin-ypos~)
    (let* ((height   (+ 4 editor-win.+border-width-bottom+  (* +line-height+ (+ 2 bwin-lines~))))
           (surface  (mezzano.gui:make-surface
                                (editor-win.width bwin-cols~)
                                height
                                )))
        (mezzano.gui.widgets:resize-frame     frame~   surface)
        (mezzano.gui.compositor:resize-window window~  surface)
        (mezzano.gui.compositor::damage-whole-screen)
        (editor-win.height height)
        (setf lines~   bwin-lines~
              cols~    bwin-cols~)))

(defun window-maximize( viewer )
    (mezzano.gui.compositor:move-window window~ 0 0)
    (multiple-value-bind   (width height)   (mezzano.gui.compositor::screen-dimensions)
    (let ((surface  (mezzano.gui:make-surface width height))    ; so far no border around text field
         )
        (mezzano.gui.widgets:resize-frame     frame~   surface)
        (mezzano.gui.compositor:resize-window window~  surface)
        (setf lines~   (- (truncate (- height 6) +line-height+) 2)
              cols~    (truncate (- width
                                    editor-win.+border-width-left+
                                    editor-win.+border-width-right+)
                                  +char-width+)))))


(defun move-pet-console( viewer )
  (declare (ignore viewer))
  (dolist (w mezzano.gui.compositor::*window-list*)
    (let ((ypos (mezzano.gui.compositor:window-y w))
          (xpos (mezzano.gui.compositor:window-x w)))
      (when (and (< 250 ypos 450) (< 300 xpos 700))
        (mezzano.gui.compositor:move-window w +pet-console-xpos+ +pet-console-ypos+)))))


(defun print-contents ( viewer )
   (dolist (l alltext~)
        (when (or (search +section+ l)
                  (search +chapter+ l))
            (format t "(pet:loc :q/mezzano/pet.sit   ~a )~%" l)))
    t)


(defun do-fmt( viewer )
        (let ((line-length  (if (eql ruler-vertical-style :line)
                                *ruler-vertical-line-position*
                                69)))
        (when selection.active?
                (let ((newlines
                        (format-markdown-paragraph line-length (selection.lines))))
                (if (not newlines)
                        (err "format failed")
                        (progn  (selection.delete viewer)
                                (set-text~ (above~ cy~)  newlines  (below~ (1- cy~)))))
                (selection.clear)
                ))))


(defun do-sort( viewer )
        (when selection.active?
                (let ((newlines
                        (sort-lines cx~ (selection.lines))))
                (if (not newlines)
                        (err "sorting failed")
                        (progn  (selection.delete viewer)
                                (set-text~ (above cy~)  newlines  (below (1- cy~)))))
                (selection.clear)
                )))

(defun untabify-cmd( viewer )
        "untabify selection"
        (when selection.active?
                (let ((newlines
                        (untabify (selection.lines))))
                (if (not newlines)
                        (err "untabify failed")
                        (progn  (selection.delete viewer)
                                (set-text~ (above~ cy~) newlines (below~ (1- cy~)))))
                (selection.clear)
                )))


(defun inline-file(  viewer name )
        (let* ((above  (above~ cy~))
               (below  (below~ (1- cy~)))
               (lines   (file.read-lines name))
               )
        (set-text~  above  (concat ";;; inline-file: " name) lines ";;; inline-file"  below)
        ))

(defun export-hostclip( viewer )
        (when selection.active?
                (file.write +hostclip-file+ (format-text (selection.lines)))
                (selection.clear)
                ))


(defun copy-to-pet-console( viewer )
        "print a few lines of text to pet-console."
        (let ((ls (if selection.active?
                        (segment~ (selection.y1) (selection.y2))
                        )))
                (say (format nil "~{~%~a~}" ls)))
        (selection.clear))


(defun xmark.insert( viewer )                                                                   ; X-mark-X
        "// insert xmark at current line"
        (let ((line (line~ cy~))
              (col  xmark.+col+))
           (if (> col (length line))
                (set-line~ (concat (justify-left col line) (xmark.text viewer)))
                (err "xmark.insert: Someone decided the current line be too long.")
                )))

(defun copy-viewer( viewer )
        "// copy viewer"
        (gui :name :copy :cols 112 :lines 17 :xpos 1400 :ypos 760)
        (sleep 1)
        (change-file (viewer.with-name :copy)   path~))


(defun exec-selection-or-line( viewer )
        "// C-Enter, label-exec"
        (let* ((src (if selection.active?
                                (format-text (selection.lines))
                                (format-text (subseq (text viewer) (1- cy~) cy~)))))
           (princ src)
           (let ((len  (length src))
                 (i    0)
                 (sep  nil))
           (loop
                (return-when (eql len i))
                (multiple-value-bind (prg j) (read-from-string src nil :exec-sel-done :start i :preserve-whitespace t)
                        (let ((res (multiple-value-list (eval prg))))
                           (unless (eql :exec-sel-done (first res))
                                (when sep (say / ";---"))
                                (setf sep t)
                                (dolist (a res) (say / a))))
                        (setf i j)
                        ))))
        (say /))


(defun exec-line( viewer )
        "load-lisp-source of current line in package pet: pause, scroll-lock"
        (let ((snip   (concat "(in-package :pet) "  (line~ cy~))))
                (with-input-from-string ( s snip )   (mezzano.internals::load-lisp-source s))
                ))


(defun switch-to-bookmarks/bookmarks( viewer ) (switch-to-bookmarks viewer "## Bookmarks"))

(defun cfg-caller( viewer )
        (declare (ignore viewer))
        (change-file viewer (concat +load-path+ "pet-config.lisp")))


(defun toggle-ruler-vertical-style( viewer )
  (setf ruler-vertical-style   (case ruler-vertical-style
                                 (:ruler (set-ruler-position cx~) :line)
                                 (:line  nil)
                                 ((nil)  :ruler)
                                 (otherwise (err "setf ruler-vertical-style"))
                                 )))

(defun toggle-ruler-horizontal-p( viewer )      (declare (ignore viewer))  (setf ruler-horizontal-p (not ruler-horizontal-p)))
(defun toggle-echo-command-p( viewer )          (declare (ignore viewer))  (setf *echo-command-p* (not *echo-command-p*)))
(defun toggle-time-exec-p( viewer )             (declare (ignore viewer))  (setf *time-exec-p*    (not *time-exec-p*)))
(defun inline-hostclip( viewer )                (inline-file viewer +hostclip-file+))
(defun describe-tap( viewer )                   (describe (read-from-string (thing-at-point viewer))))
(defun apropos-tap( viewer )            (eval (read-from-string (concat "(apropos '"(thing-at-point viewer)" 'pet)"))))

(defun fundoc-tap( viewer )    (eval (read-from-string (concat   "(pet.lib:ddw  pet::"   (thing-at-point viewer)  ")")   )))

;;;---------------------------------------------------------------------------
;;; :section:           commands with active selection

(defun delete-selection( viewer )
        (selection.delete viewer))

(defun cut-selection( viewer )
        (selection.to-clipboard)
        (selection.delete viewer)
        (selection.clear))

(defun replace-selection( viewer )
        (selection.delete viewer)
        (selection.clear)
        (clipboard.insert))

(defun copy-selection( viewer )
        (selection.to-clipboard))

;;;---------------------------------------------------------------------------
;;; :section:           mouse commands

(defun jmp-to-mouse( viewer )
  (selection.clear)
  (when (and (>= mx 0) (< mx (cols viewer)))
          (setf cx~ (+ hx~ mx)
                cy~ (min my (length text~)))))

(defun mouse-select-tap( viewer )
        (print :mouse-select-tap)
        )

(defun mouse-select( viewer )
        (when (not (zerop hx~))   (ROADWORK :hx-not-zero))
        (when (and (>= mx 0) (< mx (cols viewer)))
                (cond
                  (selection.active?    (selection.shift-up viewer (selection.y1) my))
                  ((>= my cy~)          (selection.shift-up viewer cy~ my)  (setf selection.active? t))
                  (t                    (format t "select downwards")))
                (setf cy~ my cx~ mx)))

(defun mouse-push-to-clipboard( viewer )
        (if selection.active?
                (selection.to-clipboard)
                (err "mouse-push-to-clipboard: no selectin active")))

(defun mouse-delete( viewer )
        (when selection.active?     (selection.delete viewer) (selection.clear)))

(defun mouse-insert( viewer )
        (let* ((line  (line~ cy~))
               (len   (length line)))
           (when (> cx~ len)
                (set-line~ (concat line (spaces (1+ (- cx~ len)))))))
        (clipboard.insert viewer))

(defun mouse-exec( viewer )
        (if selection.active?
                (progn
                        (load-file-or-selection viewer (viewer.lisp-package viewer))
                        (selection.drop)                        ; in case of previous selection as parameter
                        (selection.clear))
                (say / "mouse-exec: aborted / no selection to exec.")
                ))

;;;===========================================================================
;;; :chapter: command calls
(print :---command-calls)
;;;---------------------------------------------------------------------------
;;; :section:           keyboard bindings

(defmacro dispatch-on-selection( if-active-fn std-fn )
        `(lambda( viewer )
                (if selection.active?
                        (funcall ,if-active-fn viewer)
                        (funcall ,std-fn       viewer))))

(defmacro insert/overwrite( c )
        "to insert characters bound to special keys like M-e for €"
        `(lambda( viewer )  (funcall (if insertp~   #'insert-char   #'overwrite-char) viewer ,c)))


(defun dispatch-keyboard-key( viewer  k )
  (let ((p  (assoc k +key-map+)))
    (cond
      (p        (when (or *echo-command-p* (second p))
                        (format t "## ~a~%" (third p)))
                (funcall (third p) viewer))
      (t        (if insertp~
                        (insert-char viewer k)
                        (overwrite-char viewer k))
                (nextdrawis :thisline))
      )))

;;;---------------------------------------------------------------------------
;;; :section:           mouse bindings
(defun count-clicks-1()
        (let ((tm   (truncate (mezzano.internals::get-internal-run-time) internal-run-time-to-msec)))
           (if (or (>= mouse-1-click-count 3)
                   (>  (- tm mouse-1-click-time) mouse-delta-click-time))
               (setf mouse-1-click-count 1)
               (incf mouse-1-click-count))
           (setf mouse-1-click-time tm)
           ))

(defun dispatch-mouse-event( viewer  e )
  (flet((downp( was now )    (and (not was) now))
        (upp( was now )      (and was (not now)))
        (is( printp fn )     (when (or *echo-command-p* printp) (format t "~&mouse ~a~%"  fn))
                             (funcall fn viewer )
                             (logcmd :mouse fn mx my)
                             ))
    (multiple-value-bind (now1 now2 now3 now4 now5 line col) (values-list e)
      ;; (format t "~a ~a ~a   ~a ~a  ~a  ~a~%" now1 now2 now3 now4 now5 line col)
      (setf   mx col    my line)
      (when (downp was1 now1)  (count-clicks-1))
      (let* ((atscrbar          (< mx 0))
             (attext            (> line 0))
             (atlabels          (eql line 0))
             (attitle           (eql line -1))
             (down1             (downp was1 now1))
             (down2             (downp was2 now2))
             (down3             (downp was3 now3))
             (down4             (downp was4 now4))
             (down5             (downp was5 now5))
             (up2               (upp was2 now2))
             (double1           (eql 2 mouse-1-click-count))
;;;          (within-colnum     (if (and atlabels (<= 0 mx   4))  t nil))
;;;          (within-linenum    (if (and atlabels (<= 5 mx   9))  t nil))
;;;          (within-gap        (if (and atlabels (<= 10 mx 11))  t nil))
             (k (cond

                 ((and atscrbar down1)                  (scroll-down viewer (1- my)))
                 ((and atscrbar down3)                  (scroll-up   viewer (- (lines viewer) my)))
                 ((and atscrbar down5)                  (jmp-top-of-screen viewer) (jmp-down-1 viewer) (jmp-prev-section viewer))
                 ((and atscrbar down4)                  (jmp-top-of-screen viewer) (jmp-down-1 viewer) (jmp-next-section viewer))
;;;              ((and atscrbar down4)                  (find-next viewer))
;;;              ((and atscrbar down5)                  (find-prev viewer))

                 ((and attext   was1 down5)             (cond   ; attext was1 down5: B1+scroll-up
;;;                                                       ((<  col 78)  (jmp-top-of-screen viewer) (jmp-prev-label viewer))
                                                          ((<  col 78)  (find-prev viewer))
                                                          (t            (jmp-top-of-screen viewer) (jmp-prev-chapter viewer))))

                 ((and attext   was1 down4)             (cond   ; attext was1 down4: B1+scroll-down
;;;                                                       ((<  col 78)  (jmp-top-of-screen viewer) (jmp-next-label viewer))
                                                          ((<  col 78)  (find-next viewer))
                                                          (t            (jmp-top-of-screen viewer) (jmp-next-chapter viewer))))

                                                                                                                                                   ;-----------------------------------------;
                                                                                                                                                   ;       mouse wheel on screen areas       ;
                 ((and attext   down5)                  (cond   ; attext down5: scroll-up                                                          ;-----------------------------------------;
                                                          ((<  col 78)  (cond                                                                      ; 1 line up/down   | page lines up/down   ;
                                                                          ((< line 4)                   (nextdrawis :oneup)  (scroll-up viewer 1))       ;                                         ;
                                                                          ((> line (- (lines viewer) 2))(nextdrawis :oneup)  (scroll-up viewer 1))       ; 3 lines up/down    section up/down      ;
                                                                          (t                            (nextdrawis :oneup)  (scroll-up viewer 1)))) ; B1+wheel:label     B1+wheel:chapter     ;
                                                          (t            (cond                                                                      ;                                         ;
                                                                          ((< line 4)                   (scroll-up viewer (1- lines~ )))           ; 1 line up/down   | page lines up/down   ;
                                                                          ((> line (- (lines viewer) 2))(find-prev-x-mark viewer))           ;-----------------------------------------;
                                                                          (t                            (find-prev viewer))   ))))


                 ((and attext   down4)                  (cond   ; attext down4: scroll-down
                                                         ((<  col 78)   (cond
                                                                          ((< line 4)                   (nextdrawis :onedown)  (scroll-down viewer 1))
                                                                          ((> line (- (lines viewer) 2))(nextdrawis :onedown)  (scroll-down viewer 1))
                                                                          (t                            (nextdrawis :onedown)  (scroll-down viewer 1))))
                                                          (t            (cond
                                                                          ((< line 4)                   (scroll-down viewer (1- lines~)))
                                                                          ((> line (- (lines viewer) 2))(find-next-x-mark viewer))
                                                                          (t                            (find-next viewer))))))


                 ((and attext   was3 down1 double1)     (is nil 'mouse-delete))
                 ((and attext   was3 down1)             (is nil 'mouse-push-to-clipboard))
                 ((and attext   down1 double1)          (is nil 'mouse-select-tap))
                 ((and attext   down1)                  (is nil 'jmp-to-mouse))
                 ((and attext   was1 down3)             (is nil 'mouse-insert))
                 ((and attext   down2)                  (is nil 'mouse-select))
                 ((and attext   was1 up2)               (is nil 'selection.clear))
                 ((and attext   up2)                    (is nil 'mouse-exec))
                 ((and attext   down3)                  (is nil 'mouse-select))


               ; ((and attitle  down1)                  ;is      move window)
                 ((and attitle  down1  double1)         (is nil 'window-original-size))
                 ((and attitle  down2)                  (is nil 'window-maximize))
                 ((and attitle  down3)                  (is nil 'window-maximize-vertical))

;;;              ((and atlabels down5 within-colnum)    (is nil 'jmp-prev-chapter))     ; mouse wheel on colnum
;;;              ((and atlabels down5 within-linenum)   (is nil 'jmp-prev-section))     ; mouse wheel on linenum
;;;              ((and atlabels down5 within-gap)       (is nil 'jmp-prev-label))
;;;              ((and atlabels down4 within-colnum)    (is nil 'jmp-next-chapter))
;;;              ((and atlabels down4 within-linenum)   (is nil 'jmp-next-section))
;;;              ((and atlabels down4 within-gap)       (is nil 'jmp-next-label))

                 ((and atlabels down1)                  (is nil 'exec-label))

                 (t                                     :nothing))))                    ; just a running mouse
        (unless (eql k :nothing)   (rewrite-screen viewer))                             ; just a running mouse
        (setf  was1 now1   was2 now2   was3 now3))
        )))


;;;---------------------------------------------------------------------------
;;; :section:           label bindings

(defun dispatch-label(  viewer label )
  (let ((p   (assoc-if   (lambda( l ) (equal l label))   +label-map+)))
    (cond
      (p        (when (or *echo-command-p* (second p))   (format t "## ~a   ~a~%" label (third p)))
                (funcall (third p) viewer)
                )
      (t        (print "label not found"))
      )))
;;;
(defun exec-label( viewer )
  (let ((col mx))
    (decf col 11)                                       ; modifiedp col-number and line-number left of labels
    (let* ((l   (find-if (lambda( l )
                           (and (<= (first l) col)
                                (> (second l) col)))
                         +label-click-positions+))
           (name (if l (third l) nil))
          )
      (if name
          (dispatch-label viewer name)
          (say / "---  No Label found.  ---")
          ))))


;;;-------------------------------------
(defun label-map-do(  start end ls )
  "+labels+ begins and ends with spaces"
  (cond
   ((or (>= start (length +labels+))
        (>= end   (length +labels+)))
    ls)

   ((/= end 0)
    (if (char-at= +labels+ end #\space)
        (label-map-do  (+ end 1) 0 (cons (list start end (subseq +labels+ start end)) ls))
        (label-map-do   start (+ end 1)  ls)))

   ((char/= (char +labels+ start) #\space)
    (label-map-do   start start ls))

   (t
    (label-map-do   (1+ start) 0 ls))

   ))

(defun labels-print(  )   (say  (map 'list #'first +label-map+)  /)  t)

(defun labels-set(  labels )
        "Set labels of labels- or tag-line."
        (setf +labels+ labels)
        (setf +label-click-positions+ (label-map-do  0 0 nil))
        t)


;;;===========================================================================
;;; :chapter: dispatch events
;;;---------------------------------------------------------------------------

(defgeneric dispatch-event (viewer event))

;;;---------------------------------------------------------------------------
;;; :section:           keyboard dispatch

(defmethod dispatch-event (viewer (event mezzano.gui.compositor:key-event))
    "key-event"
    (when (not (mezzano.gui.compositor:key-releasep event))
      (let* ((k0  (mezzano.gui.compositor:key-key event))
             (mod (mezzano.gui.compositor:key-modifier-state event))
             (k (if mod
                    (mezzano.internals::make-character (char-code (char-upcase k0))
                                :control (find :control mod)
                                :meta    (find :meta    mod)
                                :super   (find :super   mod)
                                :hyper   (find :hyper   mod))
                    k0)))
                (with-text-locked viewer
                        (dispatch-keyboard-key viewer k)
                        t)
                (selection.clear)
                (rewrite-screen viewer)
                )))


;;;---------------------------------------------------------------------------
;;; :section:           mouse dispatch

(defmethod dispatch-event (viewer (event mezzano.gui.compositor:mouse-event))
        "mouse-event"
        (mezzano.gui.widgets:frame-mouse-event (frame viewer) event)     ; screen rel event --> frame rel event
        (let* ((bs            (mezzano.gui.compositor:mouse-button-state   event))
               (ypos          (mezzano.gui.compositor:mouse-y-position     event))
               (xpos          (mezzano.gui.compositor:mouse-x-position     event))
               (left-button   (logbitp 0 bs))                         ; logbitp 0   -> botton 1
               (middle-button (logbitp 2 bs))                         ; logbitp 2   -> botton 2
               (right-button  (logbitp 1 bs))                         ; logbitp 1   -> botton 3
               (wheel-up      (logbitp 3 bs))
               (wheel-down    (logbitp 4 bs))
               (my            (1- (truncate ypos +line-height+)))
               (mx            (if (< xpos editor-win.+border-width-left+)
                                -1
                                (truncate (- xpos editor-win.+border-width-left+) +char-width+)))
               (e             (list left-button middle-button right-button   wheel-up wheel-down   my mx))
               )
           (unless (equal e *last-mouse-event*)
                (setf *last-mouse-event* e)
                (with-text-locked viewer
                        (dispatch-mouse-event  viewer e)
                        t))))


;;;---------------------------------------------------------------------------
;;; :section:           activation

(defmethod dispatch-event( viewer (event mezzano.gui.compositor:window-create-event))
        "// So far everything is ok, right?")

(defmethod dispatch-event (viewer (event mezzano.gui.compositor:window-activation-event))
  "window activation event"
     (let* ((statep  (mezzano.gui.compositor:state event)))     ; window is active or not
        (setf activep~ statep)
        (when statep (setf *last-active-viewer* viewer))
        (rewrite-screen viewer)   ; to change cursor colour
        ))


;;;---------------------------------------------------------------------------
;;; :section:           close quit resize

(defmethod dispatch-event (viewer (event mezzano.gui.compositor:window-close-event))
        "we are going to be zaped out"
        (say / :window-close-event)
        (gui-close-fn viewer)
        (throw 'mezzano.supervisor::terminate-thread nil))


(defmethod dispatch-event (viewer (event mezzano.gui.compositor:quit-event))
        "we get a request to quit, Meta-F4"
        (say / :quit-event)
        (gui-close-fn viewer)
        (throw 'mezzano.supervisor::terminate-thread nil))


(defmethod dispatch-event (viewer (event mezzano.gui.compositor:resize-request-event))
  (let ((old-width      (mezzano.gui.compositor:width  (window viewer)))
        (old-height     (mezzano.gui.compositor:height (window viewer)))
        (new-width      (max 100 (mezzano.gui.compositor:width  event)))
        (new-height     (max 100 (mezzano.gui.compositor:height event))))
    (when (or (not (eql old-width  new-width))
              (not (eql old-height new-height)))
      (let ((new-framebuffer (mezzano.gui:make-surface  new-width new-height)))
                (editor-win.height new-height)
                (setf (lines viewer) (-  (truncate (- new-height 6) +line-height+)  2))
                (setf (cols viewer) (truncate (- new-width
                                                 editor-win.+border-width-left+
                                                 editor-win.+border-width-right+)
                                              +char-width+))
                (format t "resize-window: screen-height: ~4d line-length: ~4d ~%" (lines viewer) (cols viewer))

                (mezzano.gui.widgets:resize-frame (frame viewer) new-framebuffer)
                (mezzano.gui.compositor:resize-window
                        (window viewer) new-framebuffer
                        :origin (mezzano.gui.compositor:resize-origin event))))))


(defmethod dispatch-event (viewer (event mezzano.gui.compositor:resize-event))
          (rewrite-screen viewer))

                                                                                                                                                                                                                                                    bannd
;;;===========================================================================
;;; :chapter: draw
;;; :section:           rewrite screen
(print :---draw)
;;;---------------------------------------------------------------------------

(defun rewrite-screen( viewer )
  (setf (mezzano.gui.widgets:frame-title (frame viewer))
        (if (equal path~ "")
            (format nil "~a...(--Plain-Editor-for-Text--)" name~)
            (format nil "~a   ~a" name~ (file-namestring path~))
            ))
  (let* ((window (window viewer))
         (fb     (mezzano.gui.compositor:window-buffer window)))
    (labels (
           (paint-borders()
                (mezzano.gui:bitset :set
                                         editor-win.+border-width-left+
                                         (* +line-height+ (lines viewer))
                                         +text-border-colour+ fb
                                         0
                                         44)
                (mezzano.gui:bitset :set
                                         editor-win.+border-width-right+
                                         (* +line-height+ (lines viewer))
                                         +text-border-colour+ fb
                                         (+ editor-win.+border-width-left+ (* +char-width+ (cols viewer)))
                                         44)
                (mezzano.gui:bitset :set
                                         (editor-win.width (cols viewer))
                                         (editor-win.bottom-border-width viewer)
                                         +text-border-colour+ fb
                                         0
                                         (+ 4 (* (+ 2 (lines viewer)) +line-height+)))
                )

           (paint-area( bg   x1 y1   x2 y2 )
                (mezzano.gui:bitset :set (* +char-width+  (- (1+ x2) x1))                       ; width
                                         (* +line-height+ (- (1+ y2) y1))                       ; height
                                         bg fb
                                         (+ editor-win.+border-width-left+ (* x1 +char-width+)) ; x start
                                         (+ 4 (* y1 +line-height+))))                           ; y start

           (draw-char (c fg x y)
                (let* ((glyph  (mezzano.gui.font:character-to-glyph +font+ c))
                       (mask   (mezzano.gui.font:glyph-mask glyph))
                       (xpos   (+  editor-win.+border-width-left+  (* x +char-width+) (mezzano.gui.font:glyph-xoff glyph)))
                       (ypos   (- (* (+ y 2) +line-height+) (mezzano.gui.font:glyph-yoff glyph))))
                    (mezzano.gui:bitset :blend  +char-width+ +line-height+ fg fb xpos ypos mask 0 0)))

           (draw-string( str fg x y )
                (loop
                        for c across str
                        with i = 0
                        do (draw-char c fg (+ x i) y)
                           (incf i)))

           ;;---------------------------------------------------
           (write-labels()
                (paint-area +as-labels-bg-colour+   0 1   (cols viewer) 1)
                (draw-string (concat (format nil "~a ~3d ~4d ~a"
                                                        (if (modifiedp viewer) "*" "-")
                                                        cx~
                                                        (1+ (all-y viewer cy~))
;;;                                                     (get-keymap-nickname)
                                                        +labels+))
                             +as-labels-fg-colour+
                             0 0
                             )
                )

           (ruler-shade-vertical()   (paint-area +as-ruler-shade+ (- cx~  hx~)  2   (- cx~  hx~)  (1+ (lines viewer))))
           (ruler-line-vertical(col) (paint-area +as-ruler-line+  (- col hx~)  2   (- col hx~)  (1+ (lines viewer))))
           (ruler-shade-horizontal() (paint-area +as-ruler-shade+  0 (1+ cy~)  (1- (cols viewer)) (1+ cy~)))

           (paint-selected-area()    (paint-area +as-selection-bg+
                                                0 (1+ (selection.y1))  (1- (cols viewer)) (1+ (selection.y2))))

           (draw-cursor() (paint-area (cursor-bg-colour viewer)  (- cx~ hx~) (1+ cy~)  (- cx~ hx~) (1+ cy~)))  ; 1+ : skip labels

           (write-text( start count )
                (let ((ln start))
                  (dolist (l (take count (drop (1- start) (visible-text viewer))))
                      (let ((l0 l))
                        (unless (eql hx~ 0)
                                (setf l (if (> (length l) hx~)  (subseq l hx~)  "")))
                        (when (>= (length l) (cols viewer))
                                (setf l (subseq l 0 (1- (cols viewer) )))
                                (draw-char #\# +as-long-line-hint+ (1- (cols viewer)) ln))
                        (draw-string l (line-fg-colour viewer l) 0 ln)
                        (when (and (not(eql hx~ 0)) (not(equal l0 "")) (> hx~ (indentation l0)))
                                (draw-char #\# +as-long-line-hint+ 0 ln))
                        (incf ln)))))

            (write-scroll-wheel-marks()
                (mezzano.gui:bitset :set
                        1                                                               ; x-width
                        (* 2 +line-height+)                                             ; y-size
                        +as-long-line-hint+ fb                                          ; colour framebuffer
                        (+  editor-win.+border-width-left+  (* 78 +char-width+))        ; from x
                        (+ 4 (* 2 +line-height+))                                       ; from y
                        )
                (mezzano.gui:bitset :set
                        1                                                               ; x-width
                        (* 2 +line-height+)                                             ; y-size
                        +as-long-line-hint+ fb                                          ; colour framebuffer
                        (+  editor-win.+border-width-left+  (* 78 +char-width+))        ; from x
                        (1- (* (lines viewer) +line-height+))                           ; from y
                        ))
           ;;---------------------------------------------------
          ) ; flet/labels
      (write-labels)
      (when (or selection.active?
                (>= cx~ (+ hx~ cols~))
                (< cx~ hx~)
                (eql :ruler ruler-vertical-style))
          (nextdrawis :screen))
      (when (< cx~ hx~)              (setf hx~ cx~))
      (when (>= cx~ (+ hx~ cols~))   (setf hx~ (1+ (- cx~ cols~))))
      (case redrawp~
        (:nowt  )

        (:thisline
                (if ruler-horizontal-p
                        (ruler-shade-horizontal)
                        (paint-area +text-bg-colour+  0 (1+ cy~)  (1- cols~) (1+ cy~))
                        )
                (draw-cursor)
                (write-text cy~ 1))

        (:oneup ; scroll-up --> text moves down
                ;;;(defun bitblt (  mode width height                           ; gui/surface.lisp
                ;;;                 source-surface  source-x source-y
                ;;;                 dest-surface    dest-x   dest-y   ) ...)
                (dotimes (i (1- lines~))
                   (let ((j  (- lines~ i)))
                      (mezzano.gui:bitblt
                                :set   (* +char-width+ cols~)   +line-height+
                                fb   editor-win.+border-width-left+   (+ 4 (* (+ j 0) +line-height+))
                                fb   editor-win.+border-width-left+   (+ 4 (* (+ j 1) +line-height+))
                                )))
                (paint-area +text-bg-colour+   0 2   (1- cols~) 2)
                (when (= lines~ cy~)   (draw-cursor))
                (write-text 1 1))

        (:onedown ; scroll-down --> text moves up
                (mezzano.gui:bitblt
                                :set (* +char-width+ cols~)                                     ; width
                                (1- (* (1- lines~) +line-height+))                              ; height
                                fb   editor-win.+border-width-left+   (+ 4 (* 3 +line-height+)) ; src
                                fb   editor-win.+border-width-left+   (+ 4 (* 2 +line-height+)) ; dest
                                                )
                (paint-area +text-bg-colour+   0 (1+ lines~)   (1- cols~) (1+ lines~))
                (when (= 1 cy~)   (draw-cursor))
                (write-text lines~ 1))

        (otherwise ; :screen
                (unless (eql :screen redrawp~)            (say / "#######  strange redrawp~ : " redrawp~))
                (paint-borders)
                (paint-area +text-bg-colour+  0 2  (1- (cols viewer)) (1+ (lines viewer)))
                (when (eql ruler-vertical-style :ruler)   (ruler-shade-vertical))
                (when (eql ruler-vertical-style :line)    (ruler-line-vertical *ruler-vertical-line-position*))
                (when ruler-horizontal-p                  (ruler-shade-horizontal))
                (when selection.active?                   (paint-selected-area))
                (draw-cursor)
                (write-text 1 lines~)
                (write-scroll-wheel-marks)
        ))
        (mezzano.gui.widgets:draw-frame (frame viewer))
        (nextdrawis :screen)
        )))

;;;===========================================================================
;;; :chapter: main
;;; :section:           main
;;;---------------------------------------------------------------------------
(defun main (name lines cols xpos ypos )
  (with-simple-restart
   (abort "Close pet")
   (let ((font +font+)
         (fifo (mezzano.supervisor:make-fifo 50))
         (width (editor-win.width cols)))
     (multiple-value-bind ( height lines ) (editor-win.height-2 nil lines :lines)
       (mezzano.gui.compositor:with-window (window fifo width height)
                (let* ((framebuffer (mezzano.gui.compositor:window-buffer window))
                       (frame (make-instance 'mezzano.gui.widgets:frame
                                             :framebuffer framebuffer
                                             :title " initial-pathname "
                                             :close-button-p t
                                             :resizablep t
                                             :damage-function (mezzano.gui.widgets:default-damage-function window)
                                             :set-cursor-function (mezzano.gui.widgets:default-cursor-function window)))
                       (pet (make-instance 'pet
                                           :fifo fifo
                                           :thread (mezzano.supervisor:current-thread)
                                           :window window
                                           :thread (mezzano.supervisor:current-thread)
                                           :font font
                                           :frame frame
                                           :name name
                                           :lines lines
                                           :cols cols
                                           :width width
                                           :height height
                                           :bwin-xpos xpos
                                           :bwin-ypos ypos
                                           :bwin-lines lines
                                           :bwin-cols  cols
                                           )))
                  (setf (mezzano.gui.compositor:name window) pet)
                  (mezzano.gui.compositor:move-window window xpos ypos)
                  (viewers.insert pet)
                  (rewrite-screen pet)
                  (loop
                   (handler-case
                       (dispatch-event pet (mezzano.supervisor:fifo-pop fifo))
                     (error (c)
                        (ignore-errors  (format t "~2&############  Error~%~A~2%" c)))
                     (mezzano.gui.widgets:close-button-clicked ()
                        (gui-close-fn pet)
                        (return-from main))))))))))


;;; print to *terminal-io*
;;; redirect *terminal-io* to logstream.*stream*
;;; which is going to pet-log.md
(defun spawn ( name lines cols xpos ypos  )
  (unless logstream.*stream*
       (setf   logstream.*stream*  (make-string-output-stream)))
  (mezzano.supervisor:make-thread (lambda () (main name lines cols xpos ypos ))
                                  :name (concat "pet-gui-"(symbol-name name))
                                  :initial-bindings `((*terminal-io*      ,logstream.*stream*)
                                                      (*standard-input*   ,(make-synonym-stream '*terminal-io*))
                                                      (*standard-output*  ,(make-synonym-stream '*terminal-io*))
                                                      (*error-output*     ,(make-synonym-stream '*terminal-io*))
                                                      (*trace-output*     ,(make-synonym-stream '*terminal-io*))
                                                      (*debug-io*         ,(make-synonym-stream '*terminal-io*))
                                                      (*query-io*         ,(make-synonym-stream '*terminal-io*)))
                                                      ))


(defun gui-close-fn( viewer )
  (mark.push viewer :changefile)
  (text-save viewer)
  (when (member *text-locked-by* '(:dispatch-keyboard-key :dispatch-mouse-event))
    (text-unlock))
  (viewers.drop viewer)
  (say /))


;;;===========================================================================
;;; :chapter: logstream
;;;---------------------------------------------------------------------------

(defun logstream.main()
        "listen at pets logstream,
        update viewer with file `pet-log.md`
        cache messages in case there is no such viewer.
        "
        (let ((messages    nil)
              (logfile     "pet-log.md")
              (maxlines    2222)
              (maxlinelen  1000))
        (flet ((logviewer()   (ignore-errors(viewer.with-file logfile))))

        (say /(today-now) :started logstream.*thread*)
        (loop
          (let ((string  (if (output-stream-p logstream.*stream*)               ; stream is valid?
                            (get-output-stream-string logstream.*stream*)
                            "")))
               (unless (and (emptyp string) (emptyp messages))                  ; something to push?
                       (let ((lines   (mapcar (lambda( l )
                                                (take maxlinelen l))            ; limit line length
                                          (chunks-1 string #\Linefeed))))
                           (setf messages (append messages lines))
                           (setf messages (last messages maxlines)))
                       (let ((viewer  (logviewer)))
                           (when viewer
                                (set-text~ (append alltext~ messages))
                                (set-text~ (last text~ maxlines))
                                (setf upper-roll~ nil)
                                (setf messages    nil)
                                (jmp-text-end viewer)
                                (when  (> cols~ (length (line~ cy~)))   (jmp-line-end viewer))
                                (rewrite-screen viewer)))))
          (sleep 1)
          ))))

(defun logstream.stop()
        "stop pet-logstream-thread"
        (when logstream.*thread*
                (mezzano.supervisor::terminate-thread logstream.*thread*)
                (setf logstream.*thread* nil)
                (format t "~&pet-logstream-thread terminated.~%")))

(defun logstream.start()
        "start pet-logstream-thread"
        (logstream.stop)
        (say / :logstream.start)
        (setf logstream.*thread*  (mezzano.supervisor:make-thread
                                                (lambda() (logstream.main))
                                                :name :pet-logstream-thread
                                                )))

;;;===========================================================================
;;; :chapter: autosave
(print :---further)
;;;---------------------------------------------------------------------------

(defun autosave.main()
        (say /(today-now) :started autosave.*thread*)
        (loop   (say / (today-now) :pet-autosave-thread)
                (dolist   (viewer *viewers*)
                        (say /)
                        (with-text-locked viewer
                                (text-save viewer)
                                t)
                        (rewrite-screen viewer)
                        (sleep 1))
                (when (and irc.*autostart-toggle* (not (irc.connectedp)))
                        (say / (today-now) "IRC Client lost connection.")
                        (irc.stop)
                        (sleep 10)
                        (irc.start)
                        )
                (sleep autosave.+interval+)
                ))


(defun autosave.stop()
        (when autosave.*thread*
                (mezzano.supervisor::terminate-thread autosave.*thread*)
                (setf autosave.*thread* nil)
                (format t "pet-autosave-thread terminated.~%")))


(defun autosave.start()
        (autosave.stop)
        (setf autosave.*thread* (mezzano.supervisor:make-thread
                                        (lambda() (autosave.main))
                                        :name :pet-autosave-thread
                                        ))
        (say /"pet autosave thread started."))


;;;===========================================================================
;;; :chapter: exported functions
;;;---------------------------------------------------------------------------
(defun show( viewer file-name )
  "Save current file, load new file from mdir."
  (change-file viewer (path.make +load-path+ file-name))
  t)

(defun gui( &key        (name  :pet)
                        (lines 15)                      ; (lines +editor-win-editlines+)
                        (cols  76)                      ; (cols  +editor-win-cols+)
                        (xpos  +editor-win-xpos+)
                        (ypos  +editor-win-ypos+)
                        (from-mdir nil))
        "Create new pet window, here called a viewer.
        xpos:  pixel | viewer
        ypos:  pixel | viewer
        lines: lines | :max
        cols:  cols
        from-mdir: load file from +load-path+"
        (when (symbolp xpos)
                (let* ((v (viewer.with-name xpos))
                       (x (bwin-xpos v))
                       (w (width v)))
                   (setf xpos (+ x w))))
        (when (symbolp ypos)
                (let* ((v (viewer.with-name ypos))
                       (y (bwin-ypos v))
                       (h (height v)))
                   (setf ypos (+ y h))))
        (when (eql :max lines)
                (let* ((start ypos)
                       (end   (multiple-value-bind (x y)  (mezzano.gui.compositor::screen-dimensions)   x y))
                       (x     (truncate (- end start editor-win.+border-width-bottom+ ) +line-height+)))
                   (setf lines (- x 2))))
        (spawn name lines cols xpos ypos)
        (say / :gui-spawned)
        (when from-mdir (sleep 2) (show name from-mdir)))

(defun gui-close( viewer )
        (gui-close-fn viewer)
        (mezzano.supervisor:terminate-thread thread~))

(defun gui-close-all( &optional viewer )
        (declare (ignore viewer))
        (map 'list
                (lambda( v )
                        (mezzano.gui.compositor:close-window (window v)))
                 *viewers*)
        (setf *last-active-viewer* nil)
        )

(defun ed-hook( &key initial-pathname  initial-position )
  "Ed is the standard editor."
  (declare (ignore initial-position))
  (let ((*standard-output* logstream.*stream*))
    (change-file (viewers.get-last-active) (namestring initial-pathname))
    t))

(defun ed-hook-set()
        (setf   *ed-hook-old*                     mezzano.internals::*ed-hook*
                mezzano.internals::*ed-hook*      'ed-hook))

(defun ed-hook-reset()
        (setf   mezzano.internals::*ed-hook*      *ed-hook-old*))


(defun save( viewer )
  "v --  // Write text of viewer v to file."
  (text-save viewer)
  t)

(defun save-all()
        "Write all modified texts to file."
        (dolist   (v *viewers*)
                (save v)
                (rewrite-screen v))
        (values))


(defun save-as( viewer new-name &key (path +load-path+))
        (let ((viewer (viewer.with-name viewer))
              (name   (concat path new-name)))
           (file.write-lines name (text-get-lines viewer))
           (say / :save-as name~  name)))


(defun text-get( viewer )
  "Return current text as string."
  (format-text  (if *remove-trailing-spaces*
                                (text.remove-trailing-spaces alltext~)
                                alltext~)))

(defun text-get-lines( viewer )
  "Return current text as fresh list of lines."
  (let ((viewer (viewer.with-name viewer)))
     (text.copy (if *remove-trailing-spaces*
                        (text.remove-trailing-spaces alltext~)
                        alltext~))))

(defun text-set( viewer text )
        "v text --  // set all text of viewer to text, discard current text, jmp to text start."
        (let ((viewer (viewer.with-name viewer))
              (text  (if text text (list ""))))
           (setf cx~ 0
                 cy~ 1
                 upper-roll~ nil)
           (set-text~ text)))

(defun set-ruler-position( column )
        "i --"
        (setf *ruler-vertical-line-position* column))

(defun reset()
  "Drop all viewers, dead or alive. Reset flags. Keep marks and bookmarks."
  (setf *viewers*               nil)
  (setf *last-active-viewer*    nil)
  (setf ruler-horizontal-p      t)
  (setf ruler-vertical-style    nil)
  (setf *time-exec-p*           nil)
  (setf selection.active?       nil)
  t)


;;;===========================================================================
;;; :chapter: viewer[s]

(defparameter *last-active-viewer*      nil     "setf to viewer, whenever someone gets activated")

(defun viewer.active-p( viewer )
  (if (mezzano.gui.widgets:activep (frame viewer))
      t
      nil))

(defun viewer.views-bookmarks-file-p( viewer )
        (equal (path.base path~) "pet-bookmarks"))

(defun viewer.lisp-package( viewer )
        "-- s // return '(in-package :...)' line if found in current text or '(in-packge :cl-user)"
        (if (equal "lisp" (path.ext path~))
                (let ((y (forward-find viewer   1   (lambda( line ) (search "(in-package " line)))))  ; )
                    (if y
                        (text.line viewer y)
                        "(in-package :cl-user)"
                        ))
                "(in-package :cl-user)"))

(defun viewer.with-name( x )
        "v|u -- v  // viewer|name --> viewer"
    (let ((v   (cond
                ((eql 'pet (type-of x)) x)
                ((symbolp x)            (find x *viewers* :test (lambda( x v ) (eql (name v) x))))
                (t                      (err "viewer.with-name: got strange type:" (type-of x))))))
        (if v v (err "viewer.with-name: viewer not found:" x))))

(defun viewer.with-file( file )
        "s -- v  // filename.ext -> viewer|error"
        (let ((v  (find file *viewers* :test (lambda( f v ) (equal f (file-namestring (path  v)))))))
            (if v v (err "viewer.with-file: viewer not found:" file))))


(defun viewer.show( viewer )
        (format nil "~a  last:~a act:~a mod:~a len:~a+~a ~a~55T~a~%~55T~a~% ~55T~a~%"
                name~
                (eql viewer *last-active-viewer*)
                (mezzano.gui.widgets:activep frame~)
                modifiedp~
                (length upper-roll~)
                (length text~)
                (mezzano.supervisor:thread-state thread~)
                path~
                viewer
                thread~
                ))


(defun viewers.print()
        "--  // print viewers"
        (map 'list      (lambda( v ) (say / (viewer.show v)))
                        *viewers*)
        (say / :> (length *viewers*) :viewers-found))

(defun viewers.insert( viewer )
        "v --  // register viewer in *viewers*"
        (push viewer *viewers*))

(defun viewers.drop( viewer )
        "v -- // remove viewer from *viewers*"
        (let ((v  (viewer.with-name viewer)))
           (setf *viewers* (remove v *viewers*))))

(defun viewers.showing( path )
        "path -- v  // is open in viewer v"
        (find path   *viewers*   :test (lambda( p v ) (path.= p (path v))))
        )

(defun viewers.get-last-active()
        "-- v  // return last or now active viewer"
        (let* ((viewer   *last-active-viewer*)
               (oks      '( :active :sleeping :runnable ))      ; :waiting-for-page :pager-request   not: :dead
               (state    (if viewer (mezzano.supervisor:thread-state thread~) nil)))
           (unless viewer               (err "viewers.get-last-active: is nil"))
           (unless (member state oks)   (err "viewer.get-last-active: thread is:" state))
           viewer))

;;;===========================================================================


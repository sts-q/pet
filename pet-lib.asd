
(defpackage #:pet-lib-asd
  (:use :cl :asdf))

(in-package :pet-lib-asd)

(defsystem pet-lib
  :name         "pet-lib"
  :version      nil                             ; current version: ROADWORD
  :maintainer   "sts-q"
  :author       "sts-q"
  :licence      "MIT"
  :description  "utility funs for Pet : Plain Editor for Text."
  :long-description 
  "Library functions, not specific to Pet."
  
  :serial t                                     ; the dependencies are linear.
  
  :components (
               (:file "pet-lib")                ; library definitions, not specific to the editor
               ))


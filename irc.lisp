;;;; IRC client.
;;;; This file is a modified version of MBuild/Mezzano/applications/irc.lisp

;;;===========================================================================

;;; :file: IRC client, local

;;;---------------------------------------------------------------------------
;;; :chapter:   package  defparameter

(defpackage :mezzano.irc-client
  (:use :split-sequence :cl :mezzano.network)
  (:export #:spawn
           #:irc-send
           #:connectedp
           ))

(in-package :mezzano.irc-client)
;;;---------------------------------------------------------------------------

(defun now()
  (multiple-value-bind (ss mm hh)
      (get-decoded-time)
    (declare (ignore ss))
    (format nil "~2,'0d:~2,'0d" hh  mm)))

(defun logfn( irc prefix command parameters )
  (when (logfun irc)
    (funcall (logfun irc)   (get-universal-time) prefix command parameters )))

;;;---------------------------------------------------------------------------
(defvar *irc-history* (make-instance 'mezzano.line-editor:history-table))
(defvar *irc-init-file* "SYS:HOME;IRC-INIT.lisp")
(defvar *default-nick* "Mezzie")

;;;---------------------------------------------------------------------------
(defparameter *numeric-replies*
  '((401 :err-no-such-nick)
    (402 :err-no-such-server)
    (403 :err-no-such-channel)
    (404 :err-cannot-send-to-channel)
    (405 :err-too-many-channels)
    (406 :err-was-no-such-nick)
    (407 :err-too-many-targets)
    (409 :err-no-origin)
    (411 :err-no-recipient)
    (412 :err-no-text-to-send)
    (413 :err-no-toplevel)
    (414 :err-wild-toplevel)
    (421 :err-unknown-command)
    (422 :err-no-motd)
    (423 :err-no-admin-info)
    (424 :err-file-error)
    (431 :err-no-nickname-given)
    (432 :err-erroneus-nickname)
    (433 :err-nickname-in-use)
    (436 :err-nick-collision)
    (441 :err-user-not-in-channel)
    (442 :err-not-on-channel)
    (443 :err-user-on-channel)
    (444 :err-no-login)
    (445 :err-summon-disabled)
    (446 :err-users-disabled)
    (451 :err-not-registered)
    (461 :err-need-more-params)
    (462 :err-already-registred)
    (463 :err-no-perm-for-host)
    (464 :err-password-mismatch)
    (465 :err-youre-banned-creep)
    (467 :err-key-set)
    (471 :err-channel-is-full)
    (472 :err-unknown-mode)
    (473 :err-invite-only-chan)
    (474 :err-banned-from-chan)
    (475 :err-bad-channel-key)
    (481 :err-no-privileges)
    (482 :err-chanop-privs-needed)
    (483 :err-cant-kill-server)
    (491 :err-no-oper-host)
    (501 :err-umode-unknown-flag)
    (502 :err-users-dont-match)
    (300 :rpl-none)
    (302 :rpl-userhost)
    (303 :rpl-ison)
    (301 :rpl-away)
    (305 :rpl-unaway)
    (306 :rpl-nowaway)
    (311 :rpl-whoisuser)
    (312 :rpl-whoisserver)
    (313 :rpl-whoisoperator)
    (317 :rpl-whoisidle)
    (318 :rpl-endofwhois)
    (319 :rpl-whoischannels)
    (314 :rpl-whowasuser)
    (369 :rpl-endofwhowas)
    (321 :rpl-liststart)
    (322 :rpl-list)
    (323 :rpl-listend)
    (324 :rpl-channelmodeis)
    (331 :rpl-notopic)
    (332 :rpl-topic)
    (333 :rpl-topic-time)
    (341 :rpl-inviting)
    (342 :rpl-summoning)
    (351 :rpl-version)
    (352 :rpl-whoreply)
    (315 :rpl-endofwho)
    (353 :rpl-namreply)
    (366 :rpl-endofnames)
    (364 :rpl-links)
    (365 :rpl-endoflinks)
    (367 :rpl-banlist)
    (368 :rpl-endofbanlist)
    (371 :rpl-info)
    (374 :rpl-endofinfo)
    (375 :rpl-motdstart)
    (372 :rpl-motd)
    (376 :rpl-endofmotd)
    (381 :rpl-youreoper)
    (382 :rpl-rehashing)
    (391 :rpl-time)
    (392 :rpl-usersstart)
    (393 :rpl-users)
    (394 :rpl-endofusers)
    (395 :rpl-nousers)
    (200 :rpl-tracelink)
    (201 :rpl-traceconnecting)
    (202 :rpl-tracehandshake)
    (203 :rpl-traceunknown)
    (204 :rpl-traceoperator)
    (205 :rpl-traceuser)
    (206 :rpl-traceserver)
    (208 :rpl-tracenewtype)
    (261 :rpl-tracelog)
    (211 :rpl-statslinkinfo)
    (212 :rpl-statscommands)
    (213 :rpl-statscline)
    (214 :rpl-statsnline)
    (215 :rpl-statsiline)
    (216 :rpl-statskline)
    (218 :rpl-statsyline)
    (219 :rpl-endofstats)
    (241 :rpl-statslline)
    (242 :rpl-statsuptime)
    (243 :rpl-statsoline)
    (244 :rpl-statshline)
    (221 :rpl-umodeis)
    (251 :rpl-luserclient)
    (252 :rpl-luserop)
    (253 :rpl-luserunknown)
    (254 :rpl-luserchannels)
    (255 :rpl-luserme)
    (256 :rpl-adminme)
    (257 :rpl-adminloc1)
    (258 :rpl-adminloc2)
    (259 :rpl-adminemail)

                                        ; received from irc-seven, which is running freenode
    (001 :rpl-welcome)
    (002 :rpl-yourhost)
    (003 :rpl-created)
    (004 :rpl-myinfo)
    (005 :rpl-isupport)
    (008 :rpl-snomask)
    (010 :rpl-redir)
    (015 :rpl-map)
    (017 :rpl-mapend)
    (249 :rpl-statsdebug)
    (250 :rpl-statsconn)
    (265 :rpl-localusers)
    (266 :rpl-globalusers)
    (328 :rpl-channelurl)
    (330 :rpl-whoisloggedin)
    (378 :rpl-whoishost)
    (470 :err-linkchannel)
    (477 :err-needreggednick)
    (670 :rpl-starttls)
    (671 :rpl-whoissecure)
    (704 :rpl-helpstart)
    (705 :rpl-helptxt)
    (706 :rpl-endofhelp)

    ))

;;;===========================================================================
;;; :section:           decode-command
;;;---------------------------------------------------------------------------

(defun decode-command (line)
  "Explode a line into (prefix command parameters...)."
  ;; <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
  ;; <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
  ;; <command>  ::= <letter> { <letter> } | <number> <number> <number>
  ;; <SPACE>    ::= ' ' { ' ' }
  ;; <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
  (let ((prefix nil)
        (offset 0)
        (command nil)
        (parameters '()))
    (when (and (not (zerop (length line)))
               (eql (char line 0) #\:))
      ;; Prefix present, read up to a space.
      (do () ((or (>= offset (length line))
                  (eql (char line offset) #\Space)))
        (incf offset))
      (setf prefix (subseq line 1 offset)))

    ;; Eat leading spaces.
    (do () ((or (>= offset (length line))
                (not (eql (char line offset) #\Space))))
      (incf offset))
    ;; Parse a command, reading until space or the end.
    (let ((start offset))
      (do () ((or (>= offset (length line))
                  (eql (char line offset) #\Space)))
        (incf offset))
      (setf command (subseq line start offset)))

    (when (and (= (length command) 3)
               (every (lambda (x) (find x "1234567890")) command))
      (setf command (parse-integer command))
      (setf command (or (second (assoc command *numeric-replies*))
                        command)))

    ;; Read parameters.
    (loop
     ;; Eat leading spaces.
     (do () ((or (>= offset (length line))
                 (not (eql (char line offset) #\Space))))
       (incf offset))
     (cond ((>= offset (length line)) (return))
           ((eql (char line offset) #\:)
            (push (subseq line (1+ offset)) parameters)
            (return))
           (t (let ((start offset))
                (do () ((or (>= offset (length line))
                            (eql (char line offset) #\Space)))
                  (incf offset))
                (push (subseq line start offset) parameters)))))

    (values prefix command (nreverse parameters))))


(defun parse-command (line)
  (cond ((and (>= (length line) 1)
              (eql (char line 0) #\/)
              (not (and (>= (length line) 2)
                        (eql (char line 1) #\/))))
         (let ((command-end nil)
               (rest-start nil)
               (rest-end nil))
           (dotimes (i (length line))
             (when (eql (char line i) #\Space)
               (setf command-end i
                     rest-start i)
               (return)))
           (when rest-start
             ;; Eat leading spaces.
             (do () ((or (>= rest-start (length line))
                         (not (eql (char line rest-start) #\Space))))
               (incf rest-start)))
           (values (subseq line 1 command-end)
                   (subseq line (or rest-start (length line)) rest-end))))
        (t (values "say" line))))


;;;===========================================================================
;;; :chapter:   server commands
;;;---------------------------------------------------------------------------

(defvar *command-table* (make-hash-table :test 'equal :synchronized t))

(defmacro define-server-command (name (state . lambda-list) &body body)
  (let ((args (gensym)))
    `(setf (gethash ,(if (and (symbolp name) (not (keywordp name)))
                         (symbol-name name)
                         name)
                    *command-table*)
           (lambda (,state ,(first lambda-list) ,args)
             (declare (mezzano.internals::lambda-name (irc-command ,name)))
             (destructuring-bind ,(rest lambda-list) ,args
               ,@body)))))


(define-server-command privmsg (irc from channel message)
  ;; ^AACTION [msg]^A is a /me command.
  (cond ((and (>= (length message) 9)
              (eql (char message 0) (code-char #x01))
              (eql (char message (1- (length message))) (code-char #x01))
              (string= "ACTION " message :start2 1 :end2 8))

         (format (display-pane irc) "~&~A  [~A]* ~A ~A" (now) channel from
                 (subseq message 8 (1- (length message)))))
        (t (format (display-pane irc) "~&~A [~A]<~A>~%  ~A" (now) channel from message))))


(define-server-command ping (irc from message)
  (buffered-format (irc-connection irc) "PONG :~A~%" message))

(defun parse-origin (origin)
  "Return the nick, ident and host parts of a nick!ident@host name.
If ORIGIN is a server name, then only the host is valid. Nick and ident will be false."
  (let ((bang (position #\! origin))
        (at (position #\@ origin)))
    (cond ((and bang at)
           (values (subseq origin 0 bang)
                   (subseq origin (1+ bang) at)
                   (subseq origin (1+ at))))
          ((and (not bang) (not at))
           (values nil
                   nil
                   origin))
          (t (error "Unknown origin form ~S" origin)))))

(define-server-command nick (irc from message)
  (multiple-value-bind (nick ident host)
      (parse-origin from)
    (declare (ignore host))
    (cond ((and nick ident
                (string= nick (nickname irc)))
           (format (display-pane irc) "~&-!- Nickname changed to ~A." message)
           (setf (nickname irc) message))
          ((and nick ident)
           (format (display-pane irc) "~&~A  -!- ~A is now known as ~A." (now) nick message))
          (t (format (display-pane irc) "~&:~A NICK ~A" from message)))))


;;; :section:           defvar *known-servers*
(defparameter *known-servers*
  '(
        (:libera   "irc.libera.chat"   6667)
        (:freenode "chat.freenode.net" 6667)
  )
  "A list of known/named IRC servers.")

(defun resolve-server-name (name)
  (let ((known (assoc name *known-servers* :key 'symbol-name :test 'string-equal)))
    (cond (known
           (values (second known) (third known)))
          (t
           (let* ((colon (position #\: name))
                  (server (if colon
                              (subseq name 0 colon)
                              name))
                  (port (if (and colon (not (eql (1+ colon) (length name))))
                            (parse-integer name :start (1+ colon))
                            6667)))
             (values server port))))))


;;;---------------------------------------------------------------------------
;;; :section:   communication events

(defclass server-disconnect-event ()
  ())

(defclass server-line-event ()
  ((%line :initarg :line :reader line)))

(defun irc-receive (irc)
  (let ((connection (irc-connection irc))
        (fifo (fifo irc)))
    (loop
     (let ((line (read-line connection nil)))
       (when (not line)
         (mezzano.supervisor:fifo-push (make-instance 'server-disconnect-event) fifo)
         (return))
       (setf (last-receive-time irc) (get-universal-time))
       (mezzano.supervisor:fifo-push (make-instance 'server-line-event :line line) fifo)))))


(defclass user-line-event ()
  ((%line :initarg :line :reader line)))

(defun irc-send (irc line)
  (let ((fifo (fifo irc)))
    (mezzano.supervisor:fifo-push (make-instance 'user-line-event :line line) fifo)))


(defun connectedp( irc )
  "Are we still connected? Here currently: Were we connected 5min ago?"
  (let ((max-temporal-ping-distance 300))                 ; pings come every 2-3 min
    (cond
     ((not (typep irc 'irc-client))     nil)
     ((not (irc-connection irc))        nil)
     (t   (> max-temporal-ping-distance  (- (get-universal-time) (last-receive-time irc))))
     )))


;;;===========================================================================
;;; :chapter:   client commands
;;;---------------------------------------------------------------------------

(defvar *top-level-commands* (make-hash-table :test 'equal :synchronized t))
(defvar *top-level-command-doc* (make-hash-table :test 'equal :synchronized t))

(defmacro define-command (name (irc text) docstring &body body)
  `(progn
     (setf (gethash ',(string-upcase (string name))
                    *top-level-command-doc*)
           ',docstring)
     (setf (gethash ',(string-upcase (string name))
                    *top-level-commands*)
           (lambda (,irc ,text)
             (declare (mezzano.internals::lambda-name (irc-command ,name)))
             ,@body))))

(define-command quit (irc text)
  "QUIT [message]
  Disconnect and close IRC."
  (when (irc-connection irc)
    (buffered-format (irc-connection irc) "QUIT :~A~%" text))
  (throw 'quit nil))

(define-command raw (irc text)
  "RAW <text>
  Send TEXT directly to the server without parsing."
  (when (irc-connection irc)
    (write-string text (irc-connection irc))
    (terpri (irc-connection irc))))

(define-command eval (irc text)
  "EVAL <code>
  Read and evaluate CODE, printing the result in the display pane."
  (let ((*standard-output* (display-pane irc)))
    (format t "~&[eval] ~A~%" text)
    (format t "~A~%" (eval (read-from-string text)))
    (fresh-line)))

(define-command say (irc text)
  "SAY <text>
  Send a message to the current channel."
  (cond ((and (irc-connection irc) (current-channel irc))
         (format (display-pane irc) "~&[~A]<~A>~&  ~A" (current-channel irc) (nickname irc) text)
         (buffered-format (irc-connection irc) "PRIVMSG ~A :~A~%" (current-channel irc) text)
         (logfn irc   nil "SAY" (list (current-channel irc) (nickname irc) text)))
        (t (error "Not connected or not joined to a channel."))))

(define-command msg (irc text)
  "MSG <target> [message]
   Send a message to or begin a conversation with the specified target."
  (multiple-value-bind (split-text returned-index)
      (split-sequence #\Space text :start 0 :count 1)
    (let ((leftovers (subseq text returned-index)))
      (cond ((irc-connection irc)
             (buffered-format (irc-connection irc) "PRIVMSG ~A :~A~%" (elt split-text 0) leftovers))))))

(define-command me (irc text)
  "ACTION <text>
  Send a CTCP ACTION to the current channel."
  (cond ((and (irc-connection irc) (current-channel irc))
         (format (display-pane irc) "~&[~A]* ~A ~A" (current-channel irc) (nickname irc) text)
         (buffered-format (irc-connection irc) "PRIVMSG ~A :~AACTION ~A~A~%"
                          (current-channel irc) (code-char 1) text (code-char 1)))
        (t (error "Not connected or not joined to a channel."))))

(define-command nick (irc text)
  "NICK [nickname]
  Set, change or view your nickname."
  (cond ((zerop (length text))
         (format (display-pane irc) "~&Your nickname is ~S." (nickname irc)))
        ((irc-connection irc)
         ;; Connected, let the server drive the nickname change.
         (buffered-format (irc-connection irc) "NICK ~A~%" text))
        (t (format (display-pane irc) "~&Nickname changed to ~A." text)
           (setf (nickname irc) text))))

(define-command connect (irc text)
  "CONNECT <server or address>
  Connect to a server. Addresses use the address:port format, with port defaulting to 6667."
  (cond ((zerop (length text))
         (format (display-pane irc) "~&Known servers:")
         (loop for (name address port) in *known-servers*
               do (format (display-pane irc) "~&  ~:(~A~)  ~A:~D" name address port)))

        ((not (nickname irc))
         (error "No nickname set. Use /nick to set a nickname before connecting."))

        ((irc-connection irc)
         (error "Already connected to ~S." (irc-connection irc)))

        (t (multiple-value-bind (address port)
               (resolve-server-name text)
             (format (display-pane irc) "~&Connecting to ~A (~A:~A)." text address port)
             (setf (mezzano.gui.widgets:frame-title (frame irc)) (format nil "IRC - ~A" text))
             (mezzano.gui.widgets:draw-frame (frame irc))
             (mezzano.gui.compositor:damage-window (window irc)
                                                   0 0
                                                   (mezzano.gui.compositor:width (window irc))
                                                   (mezzano.gui.compositor:height (window irc)))
             (setf (irc-connection irc) (mezzano.network.tcp:tcp-stream-connect address port)
                   (receive-thread irc) (mezzano.supervisor:make-thread (lambda () (irc-receive irc))
                                                                        :name "IRC receive"))

             (buffered-format (irc-connection irc)  "USER ~A hostname servername :~A~%"
                              (copy-seq(nickname irc))
                              (copy-seq(nickname irc)))
             (buffered-format (irc-connection irc)  "NICK ~A~%" (nickname irc))))))

(define-command disconnect (irc text)
  "DISCONNECT [text]
  Close the current connection."
  (cond ((irc-connection irc)
         (buffered-format (irc-connection irc) "QUIT :~A~%" text)
         (close (irc-connection irc)))
        (t (error "Not connected."))))

(define-command join (irc text)
  "JOIN <channel> [key]
  Join a channel."
  (let ((channel (subseq text 0 (position #\Space text))))
    (cond ((find channel (joined-channels irc) :test 'string-equal)
           (error "Already joined to channel ~A." channel))
          ((irc-connection irc)
           ;; Send the whole text to include the key.
           (buffered-format (irc-connection irc) "JOIN ~A~%" text)
           (push channel (joined-channels irc))
           (unless (current-channel irc)
             (setf (current-channel irc) channel)))
          (t (error "Not connected.")))))

(define-command chan (irc text)
  "CHAN <channel>
  Switch to a channel you are joined to."
  (when (irc-connection irc)
    (if (find text (joined-channels irc) :test 'string-equal)
        (setf (current-channel irc) text)
        (error "Not joined to channel ~A." text))))

(define-command part (irc text)
  "PART [message]
  Leave the current channel."
  (when (and (irc-connection irc) (current-channel irc))
    (buffered-format (irc-connection irc) "PART ~A :~A~%" (current-channel irc) text)
    (setf (joined-channels irc) (remove (current-channel irc) (joined-channels irc) :test #'string-equal))
    (setf (current-channel irc) (first (joined-channels irc)))))

(define-command who (irc text)
  "WHO <text>
   Return list of users matching <text>."
  (when (and (irc-connection irc) (current-channel irc))
    (buffered-format (irc-connection irc) "WHO ~A :~A~%"  (current-channel irc) text)
    (format (display-pane irc) "~2&")
    ))

(define-command whois (irc text)
  "WHOIS <text>
   Return info about users."
  (when (and (irc-connection irc)  )
    (buffered-format (irc-connection irc) "WHOIS  ~A~%"   text)
    (format (display-pane irc) "~2&")
    ))

(define-command whowas (irc text)
  "WHOWAS <text>
   Return info about users, even some time after they quit."
  (when (and (irc-connection irc)  )
    (buffered-format (irc-connection irc) "WHOWAS  ~A~%"   text)
    (format (display-pane irc) "~2&")
    ))

(define-command names (irc text)
  "NAMES <text>
   Display a list of nicks in a given channel, default: current channel."
  (when (and (irc-connection irc) (current-channel irc))
    (buffered-format (irc-connection irc) "NAMES ~A~%"   (if (equal "" text) (current-channel irc) text))
    (format (display-pane irc) "~2&")
    ))

(define-command topic (irc text)
  "TOPIC <text>
   Display topic information of channel or change topic."
  (when (irc-connection irc)
    (when (and (equal "" text) (current-channel irc))
      (setf text (current-channel irc)))
    (buffered-format (irc-connection irc) "TOPIC ~A~%"  text)
    ))

(define-command list ( irc text )
  "LIST <text>
   Display list of channels.  See: '/help list'"
  (when (and (irc-connection irc) (current-channel irc))
    (buffered-format (irc-connection irc) "LIST ~A~%"   text)
    (format (display-pane irc) "~2&")
    ))

(define-command stats ( irc text )
  "STATS <text>
    Query server for statistics."
  (when (irc-connection irc)
    (buffered-format (irc-connection irc) "STATS ~A~%"   text)
    (format (display-pane irc) "~2&")
    ))

(define-command time ( irc text )
  "TIME <text>
    Query local time from specified server."
  (when (irc-connection irc)
    (buffered-format (irc-connection irc) "TIME ~A~%"   text)
    ))


(define-command version ( irc text )
  "VERSION
    Display server version."
  (declare (ignore text))
  (when (irc-connection irc)
    (buffered-format (irc-connection irc) "version ~%")
    ))

(define-command help ( irc text )
  "HELP <text>
    Ask server for help."
  (format (display-pane irc) "~2&")
  (if (irc-connection irc)
      (buffered-format (irc-connection irc) "HELP ~A~%"   text)
      (format (display-pane irc) "~&Not connected.")
      ))


(define-command clienthelp (irc text)
  "CLIENTHELP
  Display help on clients commands."
  (declare (ignore text))
  (format (display-pane irc) "~2&Available commands:")
  (mapc (lambda (name)
          (format (display-pane irc) "~&/~A" (gethash name *top-level-command-doc*)))
        (sort (alexandria:hash-table-keys *top-level-command-doc*) #'string<))
  (cond
   ((irc-connection irc)
    (format (display-pane irc) "~&irc-connection:  ~A" (irc-connection irc))
    (format (display-pane irc) "~&joined-channels: ~{~A ~}" (joined-channels irc)))
   (t (format (display-pane irc) "~&To connect, first set your nickname with the NICK command, then connect to a server with CONNECT.")
      (format (display-pane irc) "~&Known servers, use with the CONNECT command instead of an address:")
      (loop for (name address port) in *known-servers*
            do (format (display-pane irc) "~&  ~:(~A~)  ~A:~D" name address port))))
  (format (display-pane irc) "~2&"))


;;;===========================================================================
;;; :chapter:   class irc-client
;;;---------------------------------------------------------------------------
(defclass irc-client ()
  ((%fifo :initarg :fifo :accessor fifo)
   (%window :initarg :window :accessor window)
   (%frame :initarg :frame :accessor frame)
   (%font :initarg :font :accessor font)
   (%display-pane :initarg :display-pane :accessor display-pane)
   (%input-pane :initarg :input-pane :accessor input-pane)
   (%current-channel :initarg :current-channel :accessor current-channel)
   (%joined-channels :initarg :joined-channels :accessor joined-channels)
   (%nickname :initarg :nickname :accessor nickname)
   (%logfun :initarg :logfun :accessor logfun)
   (%last-receive-time :initarg :last-receive-time :accessor last-receive-time)
   (%connection :initarg :connection :accessor irc-connection)
   (%receive-thread :initarg :receive-thread :accessor receive-thread))
  (:default-initargs :current-channel nil :joined-channels '() :nickname nil :connection nil))

(defclass irc-input-pane (mezzano.line-editor:line-edit-mixin
                          mezzano.gray:fundamental-character-input-stream
                          mezzano.gui.widgets:text-widget)
  ((%irc :initarg :irc :reader irc)))

(defmethod mezzano.gray:stream-read-char ((stream irc-input-pane))
  (let* ((irc (irc stream))
         (fifo (fifo irc)))
    (unwind-protect
        (catch 'next-character
          (setf (mezzano.gui.widgets:cursor-visible stream) t)
          (loop
           (dispatch-event irc (mezzano.supervisor:fifo-pop fifo))))
      (setf (mezzano.gui.widgets:cursor-visible stream) nil))))

(defun reset-input (irc)
  (mezzano.gui.widgets:reset (input-pane irc))
  (format (input-pane irc) "~A] " (or (current-channel irc) "")))


;;;===========================================================================
;;; :chapter:   dispatch-event
;;;---------------------------------------------------------------------------

(defgeneric dispatch-event (irc event)
  (:method (irc event)))

(defmethod dispatch-event (irc (event mezzano.gui.compositor:window-activation-event))
  (setf (mezzano.gui.widgets:activep (frame irc)) (mezzano.gui.compositor:state event))
  (mezzano.gui.widgets:draw-frame (frame irc)))

(defmethod dispatch-event (irc (event mezzano.gui.compositor:mouse-event))
  (handler-case
      (mezzano.gui.widgets:frame-mouse-event (frame irc) event)
    (mezzano.gui.widgets:close-button-clicked ()
                                              (throw 'quit nil))))

(defmethod dispatch-event (irc (event mezzano.gui.compositor:window-close-event))
  (throw 'quit nil))

(defmethod dispatch-event (irc (event mezzano.gui.compositor:quit-event))
  (throw 'quit nil))

(defmethod dispatch-event (irc (event mezzano.gui.compositor:key-event))
  ;; should filter out strange keys?
  (when (not (mezzano.gui.compositor:key-releasep event))
    (throw 'next-character
           (if (mezzano.gui.compositor:key-modifier-state event)
               ;; Force character to uppercase when a modifier key is active, gets
               ;; around weirdness in how character names are processed.
               ;; #\C-a and #\C-A both parse as the same character (C-LATIN_CAPITAL_LETTER_A).
               (mezzano.internals::make-character
                (char-code (char-upcase (mezzano.gui.compositor:key-key event)))
                :control (find :control (mezzano.gui.compositor:key-modifier-state event))
                :meta (find :meta (mezzano.gui.compositor:key-modifier-state event))
                :super (find :super (mezzano.gui.compositor:key-modifier-state event))
                :hyper (find :hyper (mezzano.gui.compositor:key-modifier-state event)))
               (mezzano.gui.compositor:key-key event)))))

(defmethod dispatch-event (irc (event server-disconnect-event))
  (format (display-pane irc) "~&Disconnected from server.")
  (close (irc-connection irc))
  (setf (irc-connection irc) nil
        (receive-thread irc) nil)
  (setf (mezzano.gui.widgets:frame-title (frame irc)) (format nil "IRC"))
  (mezzano.gui.widgets:draw-frame (frame irc))
  (mezzano.gui.compositor:damage-window (window irc)
                                        0 0
                                        (mezzano.gui.compositor:width (window irc))
                                        (mezzano.gui.compositor:height (window irc))))


;;;---------------------------------------------------------------------------
;;; :section:           server-line-event

(defmethod dispatch-event (irc (event server-line-event))
  (let ((line (line event)))
    (multiple-value-bind (prefix command parameters)
        (decode-command line)
      (logfn irc prefix command parameters)
      (let ((fn (gethash command *command-table*)))
        (cond (fn (funcall fn irc prefix parameters))
              ((keywordp command)
               (format (display-pane irc) "~&~A  [~A] -!- ~A" (now) prefix (cdr parameters)))
              ((integerp command)
               (format (display-pane irc) "~&~A  [~A] ~D ~A" (now) prefix command parameters))
              (t (format (display-pane irc) "~&~A  ~A" (now) line)
               ))))))

;;;---------------------------------------------------------------------------
;;; :section:           user-line-evenb

(defmethod dispatch-event (irc (event user-line-event))
  (let ((line  (line event)))
    (multiple-value-bind (command rest)
        (parse-command line)
      (let ((fn (gethash (string-upcase command) *top-level-commands*)))
        (if fn
            (funcall fn irc rest)
            (error "Unknown command ~S." command))))))


(defmethod dispatch-event (app (event mezzano.gui.compositor:resize-request-event))
  (let ((old-width (mezzano.gui.compositor:width (window app)))
        (old-height (mezzano.gui.compositor:height (window app)))
        (new-width (max 100 (mezzano.gui.compositor:width event)))
        (new-height (max 100 (mezzano.gui.compositor:height event))))
    (when (or (not (eql old-width new-width))
              (not (eql old-height new-height)))
      (let ((new-framebuffer (mezzano.gui:make-surface
                              new-width new-height))
            (frame (frame app))
            (font (font app)))
        (mezzano.gui.widgets:resize-frame (frame app) new-framebuffer)
        (mezzano.gui.widgets:resize-widget (display-pane app)
                                           new-framebuffer
                                           (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                           (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                           (- new-width
                                              (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                              (nth-value 1 (mezzano.gui.widgets:frame-size frame)))
                                           (- new-height
                                              (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                              (nth-value 3 (mezzano.gui.widgets:frame-size frame))
                                              1
                                              (mezzano.gui.font:line-height font)))
        (mezzano.gui.widgets:resize-widget (input-pane app)
                                           new-framebuffer
                                           (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                           (+ (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                              (- new-height
                                                 (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                                 (nth-value 3 (mezzano.gui.widgets:frame-size frame))
                                                 (mezzano.gui.font:line-height font)))
                                           (- new-width
                                              (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                              (nth-value 1 (mezzano.gui.widgets:frame-size frame)))
                                           (mezzano.gui.font:line-height font))
        (draw-seperating-line app new-width new-height new-framebuffer)
        (mezzano.gui.compositor:resize-window
         (window app) new-framebuffer
         :origin (mezzano.gui.compositor:resize-origin event))))))

(defmethod dispatch-event (app (event mezzano.gui.compositor:resize-event))
  (reset-input app))

;;;===========================================================================
;;; :section:           draw-seperating-line
;;;---------------------------------------------------------------------------

(defun draw-seperating-line (irc width height framebuffer)
  (let* ((frame (frame irc))
         (font (font irc)))
    ;; Line seperating display and input panes.
    (mezzano.gui:bitset :set
                        (- width
                           (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                           (nth-value 1 (mezzano.gui.widgets:frame-size frame)))
                        1
                        (mezzano.gui:make-colour 0.5 0.5 0.5)
                        framebuffer
                        (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                        (+ (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                           (- height
                              (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                              (nth-value 3 (mezzano.gui.widgets:frame-size frame))
                              (mezzano.gui.font:line-height font)
                              1)))))


;;;===========================================================================
;;; :chapter:   irc-main
;;;---------------------------------------------------------------------------

(defun irc-main ( xpos ypos width height irc )
  (catch 'quit
    (let ((font (mezzano.gui.font:open-font
                 mezzano.gui.font:*default-monospace-font*
                 mezzano.gui.font:*default-monospace-font-size*))
          (fifo (mezzano.supervisor:make-fifo 50))
          (*print-circle* nil))
      (ignore-errors
        (load *irc-init-file* :if-does-not-exist nil))
      (mezzano.gui.compositor:with-window (window fifo width height)
                                          (let* ((framebuffer (mezzano.gui.compositor:window-buffer window))
                                                 (frame (make-instance 'mezzano.gui.widgets:frame
                                                                       :framebuffer framebuffer
                                                                       :title "IRC"
                                                                       :close-button-p t
                                                                       :resizablep t
                                                                       :damage-function (mezzano.gui.widgets:default-damage-function window)
                                                                       :set-cursor-function (mezzano.gui.widgets:default-cursor-function window)))
                                                 (display-pane (make-instance 'mezzano.gui.widgets:text-widget
                                                                              :font font
                                                                              :framebuffer framebuffer
                                                                              :x-position (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                                                              :y-position (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                                                              :width (- width
                                                                                        (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                                                                        (nth-value 1 (mezzano.gui.widgets:frame-size frame)))
                                                                              :height (- height
                                                                                         (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                                                                         (nth-value 3 (mezzano.gui.widgets:frame-size frame))
                                                                                         1
                                                                                         (mezzano.gui.font:line-height font))
                                                                              :damage-function (mezzano.gui.widgets:default-damage-function window)))
                                                 (input-pane (make-instance 'irc-input-pane
                                                                            :history-table *irc-history*
                                                                            :font font
                                                                            :framebuffer framebuffer
                                                                            :x-position (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                                                            :y-position (+ (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                                                                           (- height
                                                                                              (nth-value 2 (mezzano.gui.widgets:frame-size frame))
                                                                                              (nth-value 3 (mezzano.gui.widgets:frame-size frame))
                                                                                              (mezzano.gui.font:line-height font)))
                                                                            :width (- width
                                                                                      (nth-value 0 (mezzano.gui.widgets:frame-size frame))
                                                                                      (nth-value 1 (mezzano.gui.widgets:frame-size frame)))
                                                                            :height (mezzano.gui.font:line-height font)
                                                                            :damage-function (mezzano.gui.widgets:default-damage-function window)))
                                                 )
                                            (setf (mezzano.gui.compositor:name window) irc)
                                            (setf (fifo irc) fifo
                                                  (font irc) font
                                                  (window irc) window
                                                  (frame irc) frame
                                                  (display-pane irc) display-pane
                                                  (input-pane irc) input-pane)
                                            (setf (slot-value input-pane '%irc) irc)
                                            (draw-seperating-line irc width height framebuffer)
                                            (mezzano.gui.widgets:draw-frame frame)
                                            (mezzano.gui.compositor:damage-window window 0 0  width height)
                                            (when (and xpos ypos)
                                              (mezzano.gui.compositor:move-window window xpos ypos))
                                            (unwind-protect
                                                (progn
                                                  (format (display-pane irc) "~&Mezzano IRC Client.")
                                                  (format (display-pane irc) "~&Type:")
                                                  (format (display-pane irc) "~&   /clienthelp   for help about this client.")
                                                  (format (display-pane irc) "~&   /help         for a list of server commands, when connected.")
                                                  (loop
                                                   (handler-case
                                                       (progn
                                                         (reset-input irc)
                                                         (let ((line (read-line (input-pane irc))))
                                                           (multiple-value-bind (command rest)
                                                               (parse-command line)
                                                             (let ((fn (gethash (string-upcase command) *top-level-commands*)))
                                                               (if fn
                                                                   (funcall fn irc rest)
                                                                   (error "Unknown command ~S." command)))))
                                                         (format (display-pane irc) "~&"))
                                                     (error (c)
                                                            (format (display-pane irc) "~&Error: ~A" c)))))
                                              (when (irc-connection irc)
                                                (close (irc-connection irc)))))))))

;;;===========================================================================
;;; :section:           spawn
;;;---------------------------------------------------------------------------

(defun spawn ( &key (width 640) (height 480) logfun xpos ypos)
  (let* ((instance (make-instance 'irc-client
                                  :nickname *default-nick*
                                  :logfun logfun
                                  :last-receive-time 0))
         (thread  (mezzano.supervisor:make-thread (lambda()  (irc-main xpos ypos width height instance))
                                                  :name "IRC"
                                                  :initial-bindings `((*terminal-io* ,(make-instance 'mezzano.gui.popup-io-stream:popup-io-stream
                                                                                                     :title "IRC console"))
                                                                      (*standard-input* ,(make-synonym-stream '*terminal-io*))
                                                                      (*standard-output* ,(make-synonym-stream '*terminal-io*))
                                                                      (*error-output* ,(make-synonym-stream '*terminal-io*))
                                                                      (*trace-output* ,(make-synonym-stream '*terminal-io*))
                                                                      (*debug-io* ,(make-synonym-stream '*terminal-io*))
                                                                      (*query-io* ,(make-synonym-stream '*terminal-io*))))))
    (format t "...spawn done" )(sleep 5)
    (values thread instance)))

;;;===========================================================================


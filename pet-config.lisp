;;;===========================================================================

;;; :file:   pet-config.lisp            pet configuration file

;;;===========================================================================
(in-package :pet)

(print :pet-config)


;;;---------------------------------------------------------------------------
;;; :section:           config colour scheme, restart
'(
        ;; B1 B2 one of the following lines to restart with an other window config.
        (pet:start :config :std)
        (pet:start :config :stack)
        (pet:start :config :bird)
        (pet:start :config :pine)
        (pet:start :config :mezz)

        ;; B1 B2 one of the following lines to change colour scheme.
        (colours.mezzano-style)
        (colours.on-lightgray)
        (colours.on-darkgray)
        (colours.on-black)
        (colours.on-seashell)
        (colours.on-linen)
        (colours.on-tan)
        (colours.on-cornflowerblue)
        (colours.on-midnightblue)
        )


;;;---------------------------------------------------------------------------
;;; :section:           config start

(defun start( &key (config nil) )
      (when config (setf *config* config))
      (gui-close-all)
      (autosave.stop)
      (logstream.stop)
      (cl-user::__threads :name "pet-log")
      (reset)
      (case *config*
        (:std
                (colours.on-lightgray)
                (pet:gui  :name :main :from-mdir "README.md"    :lines 20   :cols 112 :xpos 90    :ypos  920)
                (pet:gui  :name :log  :from-mdir "pet-log.md"   :lines 36   :cols 112 :xpos :main :ypos    0)
                (pet:gui  :name :side :from-mdir "pet.sit"      :lines :max :cols 112 :xpos :main :ypos :log))
        (:bird
                (colours.on-lightgray)
                (pet:gui  :name :main :from-mdir "README.md"    :lines 20   :cols 112 :xpos 100   :ypos  920)
                (pet:gui  :name :log  :from-mdir "pet-log.md"   :lines 25   :cols 112 :xpos :main :ypos    0)
                (pet:gui  :name :side :from-mdir "pet.sit"      :lines 25   :cols 112 :xpos :main :ypos :log)
                (pet:gui  :name :repl :from-mdir "pet-repl.sit" :lines :max :cols 112 :xpos :main :ypos :side))
        (:pine  ; e.g. Pinebook Pro:   1080p Full HD 1920x1080 16:9   14" 35cm  ~30x17cm
                (colours.on-darkgray)
                (pet:gui  :name :main :from-mdir "README.md"    :lines 51   :cols 112 :xpos 50    :ypos 2)
                (pet:gui  :name :log  :from-mdir "pet-log.md"   :lines 32   :cols  65 :xpos :main :ypos 0)
                (pet:gui  :name :side :from-mdir "pet.sit"      :lines 17   :cols  65 :xpos :main :ypos :log))
        (:stack
                (colours.on-tan)
                (pet:gui  :name :main :from-mdir "README.md"    :lines  22  :cols 94  :xpos 0     :ypos 4)
                (pet:gui  :name :side :from-mdir "pet.sit"      :lines  18  :cols 94  :xpos 0     :ypos :main)
                (pet:gui  :name :log  :from-mdir "pet-log.md"   :lines :max :cols 94  :xpos 0     :ypos :side))
        (otherwise ; :mezz
                (colours.mezzano-style)
                (pet:gui  :name :main   :lines 25  :cols  80   :xpos 4      :ypos 540)
                (pet:gui  :name :side   :lines 13  :cols  72   :xpos :main  :ypos 730)
                (pet:gui  :name :log    :lines 13  :cols  72   :xpos :side  :ypos 730)
                (change-file :main (mdir +load-path+ "README.md"))
                (change-file :side (mdir +load-path+ "pet.sit"))
                (change-file :log  (mdir +load-path+ "pet-log.md"))))
      (autosave.start)
      (logstream.start)
      (values))


;;;---------------------------------------------------------------------------
;;; :section:           config labelmap

;;; edit, then B1 B2 the following lines to change labels
;;; X space-character requiered as last char X
(labels-set"  << ;; >> bookmarks SAVE BAK IHC EHC FIND ^ cp cfg more PET | LOAD FMT unTAB DEF PDEF DOC     SAY  ")

(defun show-pet.sit( viewer ) (show viewer "pet.sit"))
(defun show-pet-log( viewer ) (show viewer "pet-log.md")  (jmp-text-end viewer) (jmp-line-end viewer))

(setf +label-map+
  '(
    ( "<<"              nil     indent-less)
    ( ">>"              nil     indent-more)
    ( ";;"              nil     toggle-comment)
    ( "bookmarks"       nil     switch-to-bookmarks/bookmarks)
    ( "log"             nil     show-pet-log)
    ( "todo"            nil     show-pet.sit)
    ( "repl"            nil     switch-to-repl)
    ( "FMT"             nil     do-fmt)
    ( "SORT"            nil     do-sort)
    ( "unTAB"           nil     untabify-cmd)
    ( "UTF"             nil     insert-apl-chars)
    ( "ALL"             nil     find-all)
    ( "PDEF"            nil     print-definition)
    ( "DEF"             nil     find-definition)
    ( "IHC"             nil     inline-hostclip)
    ( "EHC"             nil     export-hostclip)
    ( "BAK"             nil     backup-file viewer)
    ( "SAVE"            nil     text-save)
    ( "LOAD"            t       load-current-file)
;;;    ( "MXP"             t       macroexpand-line)
    ( "EXEC"            t       exec-selection-or-line)
    ( "DESC"            nil     describe-tap)
    ( "APP"             nil     apropos-tap)
    ( "DOC"             nil     fundoc-tap)
;;;    ( "cp"              nil     copy-to-pet-console)
    ( "^"               nil     xmark.insert)
    ( "cp"              nil     copy-viewer)
    ( "cfg"             nil     cfg-caller)
    ( "|"               nil     toggle-ruler-vertical-style)
    ( "-"               nil     toggle-ruler-horizontal-p)
    ( "E"               t       toggle-echo-command-p)
    ( "tm"              t       toggle-time-exec-p)
    ( "MPC"             t       move-pet-console)
    ( "FIND"            nil     find-push-command)
    ( "more"            nil     print-more-commands)

    ( "IRC"             t       irc-labels)
    ( "CONNECT"         t       irc-connect)
    ( "JOIN"            t       irc-join)
    ( "DISCONNECT"      t       irc-disconnect)
    ( "SAY"             t       irc.say)
    ( "QUIT"            t       irc-quit)
    ))

;;;---------------------------------------------------------------------------
;;; :section:           more

(defun print-more-commands( viewer)
  (declare (ignore viewer))
  (say /"
    Mezzano:
        (__testq)
        (__stats)
        (say (multiple-value-bind (a b) (__screen-dimensions) (list a b)))
        (__print-global-shortcuts)
        (__threads)
        ; (__threads :name XXX :state :active :terminate :now)
        (file.copy \" \"  \" \" +load-path+)

    Lisp:
        (unintern 'pet::XXX  :pet)

    Pet:
        (pet:gui :name :XnameX :lines 12 :cols 112)
        (pet:pub :main)                                 ; `publish` viewer at port 80
        (pet:start :config :std)
        (pet:start :config :stack)
        (pet:start :config :pine)
        (pet::viewers.print)
        (pet:save-as  :main    <name>  )        ; &key (path +load-path+)

    Others:
        (fin:more)
        "))


;;;---------------------------------------------------------------------------
;;; :section:           config keymap

(setf +key-map+
  `(
;;;  key                echo    eval                          ; comment
   ( #\Linefeed         nil     insert-newline)               ; enter or return, here: linefeed
   ( #\C-Linefeed       t       exec-selection-or-line)
   ( #\C-S-Linefeed     nil     iter)                         ; save text,write iterfile in +current-iterdir+
   ( #\M-S-Linefeed     nil     insert-indented-newline)
   ( #\S-Linefeed       nil     join-with-next-line)
   ( #\M-Linefeed       nil     join-with-prev-line)
   ( #\tab              nil     insert-tab)
   ( #\Backspace        nil     backspace-cmd)
   ( #\C-Backspace      nil     delete-line-head)
   ( #\M-Backspace      nil     delete-word)
;;;   ( #\C-Backspace      nil     delete-word)
;;;   ( #\M-Backspace      nil     delete-line-head)
   ( #\Delete           nil     ,(dispatch-on-selection   #'delete-selection   #'delete-cmd))
   ( #\C-Delete         nil     delete-line-tail)
   ( #\Left-Arrow       nil     ,(dispatch-on-selection   #'cut-selection      #'jmp-left-1))
   ( #\Right-Arrow      nil     ,(dispatch-on-selection   #'replace-selection  #'jmp-right-1))
   ( #\Up-Arrow         nil     jmp-up-1)
   ( #\Down-Arrow       nil     ,(dispatch-on-selection   #'copy-selection     #'jmp-down-1))

   ( #\Home             nil     jmp-line-start)
   ( #\End              nil     jmp-line-end)
   ( #\C-Home           nil     jmp-text-start)
   ( #\C-End            nil     jmp-text-end)
   ( #\Insert           nil     delete-word)                  ; insert: delete-word
   ( #\C-Insert         nil     ,(lambda( viewer ) (setf insertp~ (not insertp~))))

                                                                ; Left/Right-Arrow: jmp in this line
   ( #\C-Left-Arrow     nil     jmp-prev-word-start)
   ( #\C-Right-Arrow    nil     jmp-next-word-end)
   ( #\S-Left-Arrow     nil     jmp-prev-tab)
   ( #\S-Right-Arrow    nil     jmp-next-tab)
   ( #\M-Right-Arrow    nil     jmp-next-sexp)
   ( #\M-Left-Arrow     nil     jmp-prev-sexp)

                                                                ; C-S-Arrow: copy, move, delete line
   ( #\C-S-Left-Arrow   nil     delete-line)
   ( #\C-S-Right-Arrow  nil     copy-line)
   ( #\C-S-Up-Arrow     nil     move-line-up)
   ( #\C-S-Down-Arrow   nil     move-line-down)
   ( #\C-M-S-Up-Arrow   nil     move-line-tail-up)
   ( #\C-M-S-Down-Arrow nil     move-line-tail-down)

                                                                ; PageUp PageDown: scroll screen
   ( #\Page-Up          nil     ,(lambda( viewer )  (nextdrawis :oneup)    (scroll-up-1 viewer)))
   ( #\Page-Down        nil     ,(lambda( viewer )  (nextdrawis :onedown)  (scroll-down-1 viewer)))
   ( #\C-Page-Up        nil     jmp-prev-chapter)
   ( #\C-Page-Down      nil     jmp-next-chapter)
   ( #\S-Page-Up        nil     jmp-prev-section)
   ( #\S-Page-Down      nil     jmp-next-section)
   ( #\M-Page-Up        nil     jmp-prev-label)
   ( #\M-Page-Down      nil     jmp-next-label)

   ( #\M-S-Page-Up      nil     ,(lambda( viewer ) (scroll-up    viewer (1- lines~))))
   ( #\M-S-Page-Down    nil     ,(lambda( viewer ) (scroll-down  viewer (1- lines~))))
   ( #\C-M-S-Page-Up    nil     ,(lambda( viewer ) (scroll-left  viewer +hscroll-step-width+)))
   ( #\C-M-S-Page-Down  nil     ,(lambda( viewer ) (scroll-right viewer +hscroll-step-width+)))

                                                                ; Up-Arrow Down-Arrow: find (-label)
   ( #\C-Up-Arrow       t       do-nothing)
   ( #\C-Down-Arrow     t       do-nothing)
   ( #\S-Up-Arrow       nil     find-prev)
   ( #\S-Down-Arrow     nil     find-next)
   ( #\M-Up-Arrow       t       do-nothing)
   ( #\M-Down-Arrow     t       do-nothing)

   ( #\C-Space          nil     mark-push-cmd)
   ( #\C-S-Space        nil     mark-find-cmd)
   ( #\M-Space          nil     delete-whitespace)
   ( #\M-O              nil     jmp-matching-paren)
   ( #\C-F              t       find-push-command)
   ( #\C-O              nil     ,(lambda( viewer ) (filer-spawn (pathname (path.path path~)))))
   ( #\C-S              nil     ,(lambda( viewer ) (declare(ignore viewer)) (save-all)))
   ( #\C-W              t       gui-close)
   ( #\C-Q              t       gui-close-all)


   ( #\Print-Screen     t       do-nothing)                          ; ROAD to undo
   ( #\Scroll-Lock      nil     scroll-lock-cmd)
   ( #\Pause            nil     pause-cmd)
   ( #\C-Scroll-Lock    nil     ctrl-scroll-lock-cmd)

   ( #\C-Menu           t       do-nothing)

   ( #\C-<              nil     indent-less)
   ( #\C->              nil     indent-more)
   ( #\C-Comma          nil     toggle-comment)


   ))

;× ÷ ⎕ ⌶ ⌷ ⌸ ⌹ ⌺ ⌻ ⌼ ⌽ ⌾ ⌿ ⍀ ⍁ ⍂ ⍃ ⍄ ⍅ ⍆ ⍇ ⍈ ⍉ ⍊ ⍋ ⍌ ⍍ ⍎ ⍏ ⍐ ⍑ ⍒
;;;  gui/compostior: (defmethod process-event
;;;  #\M-Tab                                    ; mezzano-window-manager: other window
;;;  Meta-F1                                    ; damage-whole-screen
;;;  Meta-F4                                    ; send close-event to active window
;;;  Meta-Esc                                   ; mezzano-window-manager: interrupt thread of this window

;;;   ( #\C-Print-Screen   t     (say / "C-Print-Screen"))
;;;     ( #\C-Pause        t     (say / "C-Pause"))                ; no signal
;;;   ( #\M-Print-Screen   t     (say / "M-Print-Screen"))         ; M-Print-Screen locks ALL Meta Keys
;;;     ( #\M-Scroll-Lock  t     (say / "M-Scroll-Lock"))          ; yes
;;;     ( #\M-Pause        t     (say / "M-Pause"))                ; yes
;;;   ( #\S-Print-Screen   t     (say / "S-Print-Screen"))         ; yes
;;;     ( #\S-Scroll-Lock  t     (say / "S-Scroll-Lock"))          ; yes
;;;     ( #\S-Pause        t     (say / "S-Pause"))                ; yes
;;;   ( #\C-S-Print-Screen t     (say / "C-S-Print-Screen"))       ; yes
;;;     ( #\C-S-Scroll-Lock t     (say / "C-S-Scroll-Lock"))        ; yes
;;;     ( #\C-S-Pause      t     (say / "C-S-Pause"))              ; no signal
;;;
;;;   ( #\M-S-Print-Screen t     (say / "M-S-Print-Screen"))       ; no signal
;;;     ( #\M-S-Scroll-Lock t     (say / "M-S-Scroll-Lock"))       ; no signal
;;;     ( #\M-S-Pause      t     (say / "M-S-Pause"))              ; no signal
;;;   ( #\C-M-Print-Screen t     (say / "C-M-Print-Screen"))       ; no signal
;;;     ( #\C-M-Scroll-Lock t     (say / "C-M-Scroll-Lock"))       ; no signal
;;;     ( #\C-M-Pause      t     (say / "C-M-Pause"))              ; no signal
;;;   ( #\C-M-S-Print-Screen t   (say / "C-M-S-Print-Screen"))     ; no signal
;;;     ( #\C-M-S-Scroll-Lock t   (say / "C-M-S-Scroll-Lock"))     ; no signal
;;;     ( #\C-M-S-Pause    t   (say / "C-M-S-Pause"))              ; no signal


(defun define-key( key echo fn )   (push (list key echo fn) +key-map+))
;;;   (define-key   #\9            nil      'rotate-last-character)
;;;   (define-key   #\M-9          nil      'rotate-last-character)
;;;   (define-key   #\0            nil      'toggle-char-case)
;;;   (define-key   #\|            nil      (insert/overwrite  #\°  ))  ; shift-9
;;;
;;;   ;;;=================  via Meta  ========================================
;;;   (define-key   #\M-quotation-mark nil  (insert/overwrite  #\^  ))
;;;   (define-key   #\M-s          nil      (insert/overwrite  #\~  ))
;;;   (define-key   #\M-u          nil      (insert/overwrite  #\@  ))
;;;   (define-key   #\M-g          nil      (insert/overwrite  #\7  ))
;;;   (define-key   #\M-q          nil      (insert/overwrite  #\8  ))
;;;   (define-key   #\M-v          nil      (insert/overwrite  #\9  ))
;;;   (define-key   #\M-p          nil      (insert/overwrite  #\{  ))
;;;   (define-key   #\M-c          nil      (insert/overwrite  #\[  ))
;;;   (define-key   #\M-d          nil      (insert/overwrite  #\]  ))
;;;   (define-key   #\M-w          nil      (insert/overwrite  #\}  ))
;;;
;;;   (define-key   #\M-a          nil      (insert/overwrite  #\!  ))
;;;   (define-key   #\M-o          nil      (insert/overwrite  #\?  ))
;;;   (define-key   #\M-i          nil      (insert/overwrite  #\&  ))
;;;   (define-key   #\M-l          nil      (insert/overwrite  #\4  ))
;;;   (define-key   #\M-f          nil      (insert/overwrite  #\5  ))
;;;   (define-key   #\M-b          nil      (insert/overwrite  #\6  ))
;;;   (define-key   #\M-t          nil      (insert/overwrite  #\.  ))
;;;   (define-key   #\M-e          nil      (insert/overwrite  #\(  ))
;;;   (define-key   #\M-n          nil      (insert/overwrite  #\)  ))
;;;   (define-key   #\M-r          nil      (insert/overwrite  #\<  ))
;;;   (define-key   #\M-+          nil      (insert/overwrite  #\>  ))
;;;
;;;   (define-key   #\M-y          nil      (insert/overwrite  #\\  ))
;;;   (define-key   #\M-x          nil      (insert/overwrite  #\|  ))
;;;   (define-key   #\M-z          nil      (insert/overwrite  #\/  ))
;;;   (define-key   #\M-h          nil      (insert/overwrite  #\1  ))
;;;   (define-key   #\M-j          nil      (insert/overwrite  #\2  ))
;;;   (define-key   #\M-k          nil      (insert/overwrite  #\3  ))
;;;   (define-key   #\M-m          nil      (insert/overwrite  #\0  ))
;;;   (define-key   #\M-comma      nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\M-.          nil      (insert/overwrite  #\=  ))
;;;   (define-key   #\M--          nil      (insert/overwrite  #\#  ))
;;;
;;;   (define-key   #\M-ß          nil      (insert/overwrite  #\°  ))

      ;;;=================  via Shift  =======================================
;;;   (define-key   #\'          nil      (insert/overwrite  #\'  ))
;;;   (define-key   #\S          nil      (insert/overwrite  #\^  ))
;;;   (define-key   #\U          nil      (insert/overwrite  #\~  ))
;;;   (define-key   #\G          nil      (insert/overwrite  #\@  ))
;;;   (define-key   #\Q          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\V          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\P          nil      (insert/overwrite  #\{  ))
;;;   (define-key   #\C          nil      (insert/overwrite  #\[  ))
;;;   (define-key   #\D          nil      (insert/overwrite  #\]  ))
;;;   (define-key   #\W          nil      (insert/overwrite  #\}  ))
;;;
;;;   (define-key   #\A          nil      (insert/overwrite  #\!  ))
;;;   (define-key   #\O          nil      (insert/overwrite  #\?  ))
;;;   (define-key   #\I          nil      (insert/overwrite  #\&  ))
;;;   (define-key   #\L          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\F          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\B          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\T          nil      (insert/overwrite  #\(  ))
;;;   (define-key   #\E          nil      (insert/overwrite  #\)  ))
;;;   (define-key   #\N          nil      (insert/overwrite  #\<  ))
;;;   (define-key   #\R          nil      (insert/overwrite  #\>  ))
;;;
;;;   (define-key   #\Y          nil      (insert/overwrite  #\\  ))
;;;   (define-key   #\X          nil      (insert/overwrite  #\|  ))
;;;   (define-key   #\Z          nil      (insert/overwrite  #\/  ))
;;;   (define-key   #\H          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\J          nil      (insert/overwrite  #\°  ))
;;;   (define-key   #\K          nil      (insert/overwrite  #\#  ))
;;;   (define-key   #\M          nil      (insert/overwrite  #\=  ))
;;;   (define-key   #\comma      nil      (insert/overwrite  #\comma  ))        ; unchanged
;;;   (define-key   #\.          nil      (insert/overwrite  #\.  ))
;;;   (define-key   #\-          nil      (insert/overwrite  #\-  ))

;;;   (define-key   #\M-ß          nil      (insert/overwrite  #\°  ))


      ;;;=================  via Meta for digits  =============================
;;;   (define-key   #\M-p          nil      (insert/overwrite  #\7  ))
;;;   (define-key   #\M-c          nil      (insert/overwrite  #\8  ))
;;;   (define-key   #\M-d          nil      (insert/overwrite  #\9  ))
;;;
;;;   (define-key   #\M-t          nil      (insert/overwrite  #\4  ))
;;;   (define-key   #\M-e          nil      (insert/overwrite  #\5  ))
;;;   (define-key   #\M-n          nil      (insert/overwrite  #\6  ))
;;;
;;;   (define-key   #\M-m          nil      (insert/overwrite  #\1  ))
;;;   (define-key   #\M-comma      nil      (insert/overwrite  #\2  ))
;;;   (define-key   #\M-.          nil      (insert/overwrite  #\3  ))
;;;
;;;   (define-key   #\M--          nil      (insert/overwrite  #\0  ))


      ;;;=================  via Meta  ========================================
;;; additional S-30 key-bindings
;;;   (define-key   #\C-E          nil     'jmp-line-end)
;;;   (define-key   #\C-A          nil     'jmp-line-start)
;;;   (define-key   #\C-B          nil     'left-arrow-caller)
;;;   (define-key   #\C-F          nil     'right-arrow-caller)
;;;   (define-key   #\M-B          nil     'jmp-prev-word-start)
;;;   (define-key   #\M-F          nil     'jmp-next-word-end)
;;;
;;;   (define-key   #\C-C          nil     'jmp-up-1)
;;;   (define-key   #\C-E          nil     'down-arrow-caller)
;;;   (define-key   #\M-N          nil     'delete-cmd-caller)
;;;   (define-key   #\M-T          nil     'backspace-cmd-caller)

;;;   ( #\M-7              nil     ,(insert/overwrite  #\{  ))
;;;   ( #\M-8              nil     ,(insert/overwrite  #\[  ))
;;;   ( #\M-9              nil     ,(insert/overwrite  #\]  ))
;;;   ( #\M-0              nil     ,(insert/overwrite  #\}  ))
;;;   ( #\M-ß              nil     ,(insert/overwrite  #\\  ))
;;;   ( #\M-+              nil     ,(insert/overwrite  #\~  ))
;;;   ( #\M-<              nil     ,(insert/overwrite  #\€  ))
;;;   ( #\M-q              nil     ,(insert/overwrite  #\@  ))
;;;   ( #\M-e              nil     ,(insert/overwrite  #\€  ))
;;;   ( #\M-i              nil     ,(insert/overwrite  #\|  ))
;;;   ( #\M-m              nil     ,(insert/overwrite  #\⍒  ))
;;;   ( #\M-S-m            nil     ,(insert/overwrite  #\⍋  ))
;;;


;;;---------------------------------------------------------------------------
;;; :section:           directories and dir-nicks

(defparameter +load-path+               "REMOTE:/home/heiko/q/mezzano/"                 "pet-config, README,...")
(defparameter +hostclip-file+           "REMOTE:/home/heiko/q/mezzano/hostclip.txt"     "simple exchange with host")
(defparameter +current-iterdir+         "REMOTE:/home/heiko/q/"                         "save iterfile here")

(setf *dir-nicks* '(
        (:adds                  "REMOTE:/home/heiko/adds/")
        (:MBuild/home           "REMOTE:/home/heiko/adds/mezzano-ci/MBuild/home/")
        (:MBuild/Mezzano        "REMOTE:/home/heiko/adds/mezzano-ci/MBuild/Mezzano/")
        (:q                     "REMOTE:/home/heiko/q/")
        (:mecrisp               "REMOTE:/home/heiko/q/mecrisp/")
        (:mezzano               "REMOTE:/home/heiko/q/mezzano/")
        (:rabbit                "REMOTE:/home/heiko/q/rabbit-asm/rabbit/")
        (:rabbit-fasm/fasm      "REMOTE:/home/heiko/q/rabbit-fasm/fasm/")
        (:rabbit-fasm/src       "REMOTE:/home/heiko/q/rabbit-fasm/src/")
        (:sit                   "REMOTE:/home/heiko/q/sitool/")
        (:pub                   "REMOTE:/home/heiko/pub/")
        ))


;;;===========================================================================


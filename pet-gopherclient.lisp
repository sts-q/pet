;;;===========================================================================

;;; :file:   pet-gopherclient.lisp      Gopherclient for Pet

;;;===========================================================================
(defpackage :gopher
        (:use
                :cl
                :pet.lib)
        (:export
                #:gopher
                #:ask
                #:pedia
                #:veronica
                #:explain
                #:bsd-manpage
                #:searx
                #:back
                #:clear
                #:download
                #:history
                #:html
                ))

(in-package :gopher)

(print :gopher)

;;;===========================================================================
;;; From RFC-1436
;;;     * The server responds with a block of text terminated by a period
;;;       on a line by itself and closes the connection.
;;;     * No state is retained by the server.
;;;
;;; Current limitations:
;;;     * No local caching of pages.
;;;     * Supported descritors: i 0 1 3 7
;;;     * No expansion of tab characters.
;;;     * No translation of ansi escape sequences.
;;;     * No support for Gopher+.
;;;
;;; Available Gopher Selectors (but not supported here)
;;; ------------------------------------------------------------------------------------
;;;     keychar description                                     What I have seen, here.
;;; ------------------------------------------------------------------------------------
;;;     i       Item is inline text
;;;     0       Item is a text file
;;;     1       Item is a directory
;;;     2       Item is a CSO (qi) phone-book server
;;;     3       Item is an Error message
;;;     4       Item is a BinHexed Macintosh file.
;;;     5       Item is DOS binary archive of some sort.        archive file (zip tar etc)
;;;     6       Item is a UNIX uuencoded file.                  (UUENCODE)
;;;     7       Item is an Index-Search server.
;;;     8       Item points to a text-based telnet session.
;;;     9       Item is a binary file                           ( epub pdf ogv webm )
;;;     I       Image type.
;;;     M       MIME type.  Item contains MIME data.            ( mbox email )
;;;     P       PDF file
;;;     T       TN3270 connection.
;;;     c       calendar file
;;;     d       document file (ps, pdf, doc etc)
;;;     g       GIF type.
;;;     h       html type (or http link).
;;;     s       Sound type.  Data stream is a mulaw sound.
;;;     ;       Video file
;;;     +       Item is a redundant server
;;;
;;; ------------------------------------------------------------------------------------
;;; Usage
;;; =====
;;; Pet-gopherclient renders gopher links as lines of lisp source code.
;;; Mouse B1 B2 moves mouse cursor to line and evaluates it in the package of the
;;; current file or in package cl-user.
;;; That way, gopher links may be part of files of any type.
;;; A retrieved gopher page is displayed in viewer :main no matter
;;; in which viewer the calling link has been evaluated.
;;; At retrieving a page
;;;     a surf history
;;;     a `back-botton-line`
;;;     a `home-link-line`
;;;     a `current-link-line`
;;; is printed to viewer :log.
;;; In order to bookmark a link, copy and paste the current-link-line to a save place.
;;; There is no support for downloading files.
;;; There is no support for following links to html.
;;; The html link in question is printed to viewer :log and written to hostclip.
;;;
;;; Summary
;;; =======
;;; Pet-gopherclient works at gopher directories, text files and search queries.
;;;
;;; ------------------------------------------------------------------------------------
;;;
;;; Other Gopherclients found in Gopherspace:
'(  ; B1 B2 one of the following lines:
(gopher :dir   "ncgopher - A gopher client for the modern internet" :JAN.BIO 70 "ncgopher")
(gopher :DIR   "Nimph - My gopher client written in Nim           " :SILENTMESSENGERS.ORG 70 "/nimph")
(gopher :DIR   "2019-10-10 The CL-GOPHER Common Lisp Library      " :VERISIMILITUDES.NET 70 "2019-10-10")
(gopher :DIR   "clic: command line gopher client in Commnon Lisp"   :BITREICH.ORG 70 "/scm/clic")
(gopher :DIR   "sacc: command line gopher client in C"              :BITREICH.ORG 70 "/scm/sacc")
)
;;;
;;;===========================================================================
;;; Types
;;;     trio    class
;;;     ccmd    s       clickable command:  (gopher :txt "Main Gopher Server" :floodgap.com 70 "/servertemps.txt")
;;;
;;;===========================================================================
#|
# Observations from Gopherspace.

There are more people with depressions under way, than you
might expect. In gopher some talk about it. Of course depression as a
serious disease or depression as a lack of joy is in a medium like Gopher
difficult to distinguish.

Gopher is like the Forth Programming Language in that both are unknown to
most people. And many of those, who know about it and love it spend by far more
time with constructing their own versions than with using it.

Gopher is like the web in, that you have to wade through endless valleys of
this and that to find the sweet spot of happiness.

I really prefer reading personel blogs in Gopher than on the web. On a
typical web blog ~30% of my attention is going into *what not* to click at,
another ~30% is looking for *what* to click next. ~20% goes for something
like "Why do i read this?" What's left of my attention goes into reading
the blog. Reading on Gopher on the other hand allowes me to dive into the
writing person, what can be a very pleasant feeling.

Gopher is a simple, fast and effective medium to share text and files,
inhouse or with other poeple.

A lot of people is heavily disappointed about the modern web, especially
people, who know about the beginnings of the web. Gopher is a hope for
a life without ads, trackers, and with more content.
Well, web content in gopher-style is no sorcery.

Personally i prefer reading my favourit newspaper on Gopher over reading
in whatever web-app. It allows me to focus on content, decide what to
read and read it all, like with a printed newspaper.

Yes.

|#

;;; :section:           data

(defparameter *download-directory*      "REMOTE:/home/heiko/Downloads"  "")

(defparameter *directory-viewer*        :main   "standard viewer for directory pages")
(defparameter *text-viewer*             :main   "standard viewer for text pages")

(defvar       *history*                 nil     "List of visited pages.")
(defconstant  +eol+                     (format nil "~a"  #\linefeed))


(defclass trio ()
        ((%type     :initarg :type     :reader type     :documentation "document type to expect here: txt dir ...")
         (%text     :initarg :text     :reader text     :documentation "description of selector as found in directory")
         (%server   :initarg :server   :reader server   :documentation "server")
         (%port     :initarg :port     :reader port     :documentation "port")
         (%selector :initarg :selector :reader selector :documentation "selector string: server local path"))
        (:default-initargs      :type      :type
                                :text      ""
                                :server    :server
                                :port      70
                                :selector  "")
        (:documentation "The trio of server port selector,
                         plus the document type to expect there
                         and the description text."))


(defun make-trio( type text server port selector )
        (flet((err( k v ) (error (format nil "make-trio: strange ~a found:~%~a" k v))))
        (let ((type    (if (symbolp type) type (err :type type)))
              (text    (if (stringp text) text (err :text text)))
              (server  (typecase server
                         (string        (ignore-errors (read-from-string (concat ":" server))))
                         (symbol        server)
                         (t             (err :server server))))
              (port    (typecase port
                         (string        (let ((p (ignore-errors (read-from-string port)))) (if (integerp p) p (err :port port))))
                         (integer       port)
                         (t             (err :port port))))
              (selector (typecase selector
                          (string       selector)
                          (symbol       (symbol-name selector))
                          (t            (err :selector selector)))))
           (make-instance 'trio   :type type
                                  :text text
                                  :server server
                                  :port port
                                  :selector selector))))


(defun format-trio( trio )
        (format nil "(gopher :~a   ~S ~70T:~a ~d ~S)"
                (type trio) (text trio) (server trio) (port trio) (selector trio)))

(defun trio-equal( a b )
        "trio trio -- b  // test if server port and selector are equal"
        (and (equal (server a) (server b))
             (eql (port a) (port b))
             (equal (selector a) (selector b))))


'(
(describe            (make-trio   :txt  "hallo there at onna" :onna.be   70    :hallo-there))
(describe            (make-trio   :txt  "hallo there at onna" "onna.be"  70    "hallo-there"))
(say / (format-trio  (make-trio   :dir  "hallo there at onna" "onna.be" "70"   "/code.md.txt")))
)


;;; :section:           format directory entries

(defvar *other-file-types*              nil     "")
(defun other-file-types() *other-file-types*)

(defun file-type( selector )
        "s -- u"
        (let ((p (position #\/ selector)))
           (if p (file.type (subseq selector p))
                 nil)))

(defun format-others( keychar text server port selector )
        "Format other selector types, we don't know how to handle."
        (let ((fext (file-type selector)))
             (cond
               ((eql #\h keychar)               (format nil "(gopher:html ~s   ~s)" text selector))
               ((member fext '(:html :pdf :mobi :gif :epub :rtf :doc :xy :zip :png :gz :djvu :jpg :wav :mp3))
                                                (format nil "(gopher:download :~a ~8T~s ~70T :~a ~a ~100T~s)" fext text server port selector))
               (t                               (push fext *other-file-types*)
                                                (format nil "~c ~a ~8T~s ~70T ~a ~a ~100T~s" keychar fext text server port selector))
               )))
               ; I png: gpher.viste.fr

(defun translate-dirline( line )
        "s -- s  // translate a line of text from a directory to text"
        (if (emptyp line)
            ""
            (let* ((keychar   (char line 0))
               (fields    (chunks-1 (subseq line 1) #\Tab))
               (text      (map 'string (lambda( c ) (if (eql c #\") #\' c))   (first fields)))
               (selector  (second fields))
               (server    (third fields))
               (port      (fourth fields)))
             (cond
               ((eql #\i keychar)       text)
               ((eql #\0 keychar)       (format-trio (make-trio :txt text server port selector)))
               ((eql #\1 keychar)       (format-trio (make-trio :dir text server port selector)))
               ((eql #\3 keychar)       (concat "ERROR   " text))
               ((eql #\7 keychar)       (format nil "(gopher:ask   ~S   ~S   :~a ~d ~S)" "QUERY" text server port selector))
               (t                       (format-others keychar text server port selector))))))


;;; :section:           Pet

(defun change-to( viewer text )
        "Display text in pet viewer"
        (let* ((append (if (eql viewer :log) t nil))
               (viewer (pet:viewer.with-name viewer))
               (old    (pet:text-get-lines viewer)))
           (pet:show viewer "pet-gopherpage.txt")
           (pet:text-set viewer  (if append (append old text)  text))
           (when append (pet:jmp-text-end viewer))
           (pet:rewrite-screen viewer)))


;;; :section:           TCP

(defun read-lines( connection )
        "stream -- text  // read lines until fullstop or nil"
        (let ((txt  nil)
              (l    ""))
          (loop
           (format t ".")
           (setf l   (read-line connection nil))
           (when (or (not l) (string= l "."))   (return))    ; no more lines or end of page signal for directories
           (setf txt (cons l txt)))
;;;     (say / txt)
        (reverse txt)))

(defun read-page( server port page )
  "u i s -- text  // retrieve page from server"
  (let ((c (mezzano.network.tcp:tcp-stream-connect  (symbol-name server) port)))
    (format c "~A~A"  page +eol+)
    (format t ":")
    (prog1 (read-lines c)
      (format t ":")
      (close c)
      )))

(defun display-page( trio )
        "trio -- // display gopher page"
        (let*((text (read-page (server trio) (port trio) (selector trio))))
           (case (type trio)
             (:txt      (change-to *text-viewer*        text))
             (:dir      (change-to *directory-viewer*   (let ((text (ignore-errors(map 'list #'translate-dirline text))))
                                                           (if text
                                                                text
                                                                (error "gopher:display-page: Unable to parse Gopher directory.")))))
             (otherwise (error (format nil "gopher:display-page: don't know what to do with type ~a" (type trio))))
             ))
        (say //)
        (setf *history* (remove-if (lambda( a ) (trio-equal a trio)) *history*))
        (push trio *history*)
        (history 12)
        (say /"(gopher:back)")
        (say / (format nil "(gopher :DIR \"HOME:\" :~a ~d \"\")"  (server trio) (port trio)))
        (values))


;;; :section:           exported functions

(defun gopher( type text server port selector )
        "Display a gopher page."
        (display-page (make-trio type text server port selector))
        (values))

(defun back()
        "Go back in client history."
        (pop *history*)                                         ; current page
        (let ((trio (pop *history*)))                           ; previous page
           (if trio
                (display-page trio)
                (say / "#######  History is empty." /)))
        (values))

(defun clear()
        "Clear gopher history."
        (setf *history* nil)
        (values))

(defun download( type text server port selector )
        "Download into local download folder."
        (say    //"------------ download" /
                :dest :> *download-directory* /
                :desc :> type "<"text ">" /
                :src. :> server port selector
                /"------------"))

(defun history( &optional max-items )
        "Print gopher history. If given only the last n visited pages."
        (let ((h   (if max-items (take max-items *history*) *history*)))
           (dolist (trio (reverse h)) (say / (format-trio trio))))
        (say / :> (if max-items (format nil "~a /" max-items) "")  (length *history*) "items.")
        (values))

(defun html( text link )
        "Print link of html selector, write to hostclip."
        (let ((link  (if (headseqp "URL:" link) (subseq link 4)  link)))
           (file.write pet::+hostclip-file+ link)
           (say // "------------" / text // link // "Hostclip written." / "------------"))
        (values))

(defun ask( question text server port selector )
           "Send query to gopher server."
           (display-page (make-trio :dir text server port (format nil "~a~a~a"  selector #\Tab question)))
           (values))

(defun pedia( str )     (ask str   (concat "gopedia:      " str)    :gopherpedia.com 70 "/lookup"))
(defun veronica( str )  (ask str   (concat "veronica:     " str)    :gopher.floodgap.com 70 "/v2/vs"))
(defun explain( str )   (ask str   (concat "explain:      " str)    :forthworks.com 7005 "/"))
;;;(defun explain( str )   (ask str ""    :khzae.net 70 "/dict/search"))
(defun bsd-manpage( str ) (ask str (concat "BSD man page: " str)   :perso.pw 70 "/man.dcgi"))
(defun searx( str )       (ask str (concat "Searx@gopher: " str)   :me0w.net 70 "/searx.dcgi"))


'(
(gopher :txt "Nutella" :jan.bio 70 "/recipes/homemade-nutella.txt")
)


;;;===========================================================================


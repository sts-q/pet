
;;;     https://common-lisp.net/~mmommer/asdf-howto.shtml

(defpackage #:pet-asd
  (:use :cl :asdf))

(in-package :pet-asd)

(defsystem pet
  :name         "pet"
  :version      nil                             ; current version: ROADWORD
  :maintainer   "sts-q"
  :author       "sts-q"
  :licence      "MIT"
  :description  "Pet : Plain Editor for Text"
  :long-description 
  "A small and simple text editor, that works the same way on many different sorts of text files."
  
  :serial       t                               ; the dependencies are linear.
  :depends-on   ("pet-lib")
  
  :components (
               (:file "irc")
               (:file "pet-colours")            ; rgb hwb etc, not specific to the editor
               (:file "pet")                    ; the editor
               (:file "pet-config")             ; configuration of pet
               (:file "pet-extensions")         ; extensions to pet
               (:file "pet-gopherclient")       ; optional
               ))


;;; import into cl-user in order to evaluate lines in bookmark.md files:
;;;  (defpackage :cl-user
;;;     (:import-from   :gopher   #:gopher)
;;;     (:import-from   :pet      #:locate #:locate-1)
;;;     )


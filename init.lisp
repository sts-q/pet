;;;===========================================================================

;;; :file:   init.lisp                   first local lisp file for Mezzano

;;; This file gets loaded at every boot.

;;;===========================================================================

(in-package :mezzano.gui.desktop)

;;;---------------------------------------------------------------------------
;;; resize VM window in order to apply changes

;;;---------------------------------------------------------------------------

(defun render-icon (desktop icon-pen column-offset icon-data)
  (let* ((window (window desktop))
         (font (font desktop))
         (framebuffer (comp:window-buffer window))
         (icon (first icon-data))
         (name (subseq (second icon-data)   0 4))               ; sts-q: write max 4 chars of text
         (image (load-image icon))
         (text (get-cached-text-surface desktop name)))
    (gui:bitblt :blend
                (gui:surface-width image) (gui:surface-height image)
                image 0 0
                framebuffer column-offset icon-pen)
    (when (eql icon (clicked-icon desktop))
      (gui:bitset :xor
                  (gui:surface-width image) (gui:surface-height image)
                  #x00FFFFFF
                  framebuffer
                  column-offset icon-pen))
    (gui:bitblt :blend
                (gui:surface-width text) (gui:surface-height text)
                text 0 0
                framebuffer
                1                                               ; sts-q: overwrite icon with text
                (- (+ icon-pen (truncate (gui:surface-height image) 2) (font:ascender font))
                   (gui:surface-height text)))))

(setf *icons*
         '(
           ("LOCAL:>Icons>Filer.png"            "q   "  "(mezzano.gui.filer:spawn :initial-path \"remote:/home/heiko/q/\" )")
           ("LOCAL:>Icons>Filer.png"            "mz  "  "(mezzano.gui.filer:spawn)")
           ("LOCAL:>Icons>Terminal.png"         "    "  "(mezzano.gui.fancy-repl:spawn :width (* 112 10) :height 250)"  ) ; pet.lib:+std-window-line-length+
           ("LOCAL:>Icons>Mandelbrot.png"       "    "  "(mezzano.mandelbrot:spawn)")
           ("LOCAL:>Icons>Chat.png"             "    "  "(mezzano.irc-client:spawn)")
           ("LOCAL:>Icons>Editor.png"           "    "  "(med:spawn)")
           ("LOCAL:>Icons>Peek.png"             "Key "  "(mezzano.gui.settings:spawn)")
           ("LOCAL:>Icons>Peek.png"             "Peek"  "(mezzano.gui.peek:spawn)")
           ("LOCAL:>Icons>Peek.png"             "Spy "  "(mezzano.gui.spy:spawn)")
           ("LOCAL:>Icons>Peek.png"             "Mem "   "(mezzano.gui.memory-monitor:spawn)")
;;;        ("LOCAL:>Icons>Editor.png" "Memory Monitor" "(pet:start :config :bird)")
;;;        ("LOCAL:>Icons>Editor.png" "Memory Monitor" "(pet:gui)")
         ))

(defun redraw-desktop-window (desktop)
  (let* ((window (window desktop))
         (desktop-width (comp:width window))
         (desktop-height (comp:height window))
         (framebuffer (comp:window-buffer window))
         ;(font (font desktop))                         ; not used
         )
    (gui:bitset :set
                desktop-width desktop-height
                (colour desktop)
                framebuffer 0 0)
    (when (image desktop)
      (let* ((image (image desktop))
             (image-width (gui:surface-width image))
             (image-height (gui:surface-height image)))
        (gui:bitblt :blend
                    image-width image-height
                    image 0 0
                    framebuffer
                     90                                 ; sts-q: new position of birdy
                     90 )))                             ; sts-q: new position of birdy
    (loop
       with icon-pen = 0
       with column = *icon-horizontal-offset*
       with widest = 0
       for icon-repr in *icons*
       do
         (cond ((consp icon-repr)
                (incf icon-pen *icon-vertical-space*)
                (multiple-value-bind (width height)
                    (icon-geometry desktop icon-repr)
                  (when (> (+ icon-pen height) desktop-height)
                    (incf column (+ widest *icon-horizontal-offset*))
                    (setf widest 0)
                    (setf icon-pen *icon-vertical-space*))
                  (render-icon desktop icon-pen column icon-repr)
                  (incf icon-pen height)
                  (setf widest (max widest width))))
               ((eql icon-repr :next-column)
                (incf column (+ widest *icon-horizontal-offset*))
                (setf widest 0)
                (setf icon-pen 0))))
    (comp:damage-window window 0 0 (comp:width window) (comp:height window))))


;;;===========================================================================

(in-package :cl-user)

;;;---------------------------------------------------------------------------
;;; As message into the bootlog:

(format t "~&Loading init.lisp, first local lisp file for Mezzano.~%")


;;;-------------------------------------
;;; Set font size, effective for the mezzano gui instances created from now on.

(setf mezzano.gui.font:*default-monospace-font-size* 16)


;;;-------------------------------------
;;; Switch to local keymap

(let* ((name   "De")   ; names: De sts-30
       (m     (find-if (lambda(x) (equal name (mezzano.gui.compositor::name x)))
                        mezzano.gui.compositor::*keymap-list*)))
   (if m
        (setf mezzano.gui.compositor::*current-keymap*  m)
        (format t "#######  Error: init.lisp: keymap not found: ~A~%" name)))



;;;-------------------------------------
;;; asdf set search path

(push "REMOTE:/home/heiko/q/mezzano/" asdf:*central-registry*)


;;;-------------------------------------
;;; Print less informative and less verbose error messages.
;;; Don't ask for a restart.

(defun my-debugger( condition me-or-my )
  "s x --  // print condition and abort"
  (declare (ignore me-or-my))
  (format t "~2&############  Error~%~A~2%" condition)
  (abort)
  )

(defparameter *alt-debugger-hook* *debugger-hook*)
(defun __exchange-debugger-hooks()
        (let ((temp *debugger-hook*))
            (setf *debugger-hook* *alt-debugger-hook*
                  *alt-debugger-hook* temp)))

'(
(__exchange-debugger-hooks)
)


;;;-------------------------------------
;;; Print some system stats.

(defun __stats()
        (format t "~&~A ~A ~A ~A   git-revision ~A~%"
             (lisp-implementation-type)
             (mezzano.internals::lisp-version-string)
             (machine-version) (machine-type)
             mezzano.internals::*git-revision*)
        (format t "~{~A~%~}"  (reverse mezzano.supervisor::*cpus*))
        (format t "~&Modules:    ~A" (sort (remove-duplicates (map 'list #'string-upcase *modules*) :test #'equalp) #'string<))
        (format t "~&Features:   ~A" (sort (remove-duplicates (map 'list #'string-upcase *features*) :test #'equalp) #'string<))
        (format t "~&Asdf:a-l-s: ~A" (sort (remove-duplicates (map 'list #'string-upcase (asdf:already-loaded-systems)) :test #'equalp) #'string<))
        (format t "~&(~A)~A (major-)GC-cycles in ~A seconds.~%"
             mezzano.internals::*gc-major-cycles*
             mezzano.internals::*gc-cycles*
             mezzano.internals::*gc-time*)
        (mezzano.internals::uptime)
        (values))


;;;-------------------------------------
;;; Retrieve a gopher page and print it.

(defconstant +gopher-eol+ (format nil "~a"  #\linefeed))

(defun gopher-read-lines( connection )
  "stream -- text  // read lines until fullstop or nil"
  (let ((l  (read-line connection nil)))
    (format t ".")
    (cond
     ((or (not l)               ; no more lines
          (string= l "."))      ; end of page signal for directories
      (list l))
     (t (cons l (gopher-read-lines connection)))
     )))

(defun gopher-read-page( server port page )
  "u i s -- text  // retrieve page from server"
  (let ((c (mezzano.network.tcp:tcp-stream-connect
            (symbol-name server) port)))
    (format c "~A~A"  page +gopher-eol+)
    (format t ":")
    (prog1 (gopher-read-lines c)
      (format t ":")
      (close c)
      )))

(defun make-cream-liqueur()
  "Retrieve a Gopher page and print it."
  (let ((text (gopher-read-page
               'sdf.org 70
               "/users/guinness0001/Recipes/Cream_Liqueur.txt")))
    (format t "~%~{~A~%~}---~%" text)
    t))

(defun print-ascii-table()
  "Retrieve a Gopher page and print it."
  (let ((text (gopher-read-page
               'i-logout.cz 70
               "/ascii/t/ascii.txt")))
    (format t "~%~{~A~%~}---~%" text)
    t))


;;;-------------------------------------
;;; Print running threads.
;;; Terminate a thread.
;;; Start new thread.
;;; more:       MBuild/home/bordeaux-threads/
;;;             http://trac.common-lisp.net/bordeaux-threads/wiki/ApiDocumentation


(defun __threads( &key name state terminate (one-of nil))
  "Print or terminate selected threads.
   usage:   (__threads   :name 'name'   :state :active   :terminate :now)
   name:       case insensitve substring of thread name
   states:     :active :runnable :sleeping :dead
               :waiting-for-page :pager-request :stopped  0
   terminate:  :now
   one-of:     nil|t   default:nil   If t terminate one of the found threads.
   All keyword arguments are optional.
   Terminate thread only if just one thread found."
  (let* ((ts  (mezzano.supervisor:all-threads))
         (number))
    (when name  (setf ts  (remove-if-not
                           (lambda(x)
                             (search  (string-downcase name)
                                      (string-downcase
                                        (format nil "~a" (mezzano.supervisor:thread-name x)))))
                           ts)))
    (when state (setf ts  (remove-if-not
                           (lambda(x)
                             (eql state
                                  (mezzano.supervisor:thread-state x)))
                           ts)))
    (setf number (length ts))

    (dolist (x ts)
      (format t " ~A ~12T~A~%"
              (mezzano.supervisor:thread-state x)
              (mezzano.supervisor:thread-name x))
      )
    (format t "   ~d threads found.~%" number)

    (when (and (eql terminate :now)
               (or (= number 1) one-of))
      (format t "Terminating thread:~%~A~%" (car ts))
      (mezzano.supervisor:terminate-thread (car ts)))
    t))



(defun __my-new-thread()
  (flet ((thread-fn()
                (format t "=======  my-new-thread started.~%")
                (sleep 30)
                (format t "-------  my-new-thead closing.~%")
                ))
      (let ((thread  (mezzano.supervisor:make-thread
                         (lambda() (thread-fn))
                         :name "my-new-thread"
                         )))
          (print thread)
          )))


;;;-------------------------------------
;;; timer

(defun __timer()
  (mezzano.supervisor::with-timer (myti :relative 3 :name :my-timer)
        (format t "~%timer: hallo ")
        (mezzano.supervisor::timer-wait myti)
        (format t "world~%")
        ))

(defun __timer-2()
  (let ((timer  (mezzano.supervisor::make-timer :name :myti :relative 30))
        (i      1))
    (format t "~&timer-2: start~%")
    (loop
     (mezzano.supervisor::timer-arm i timer)    ; first iteration overwrites :relative 30
     (format t "~d " i)
     (mezzano.supervisor::timer-wait timer)     ; == (event-wait (timer-event timer))
     (format t " timer~%" )
     (incf i)
     (when (> i 5) (return nil))
     ))
  (format t "done~%"))


;;;-------------------------------------
;;; Return screen dimensions.

(defun __screen-dimensions()
        (mezzano.gui.compositor::screen-dimensions))


;;;-------------------------------------
;;; find window by name

(defun find-window-by-name( name )
        "s -- window  // s may be substring of name, return first window found."
        (find (string-downcase name)  mezzano.gui.compositor::*window-list*
                :test (lambda( name w ) (search name (string-downcase(format nil "~A"(mezzano.gui.compositor:name w)))))))

;;;-------------------------------------
;;; global shortcuts

(defun __print-global-shortcuts()
        (format t  "~%Global Keyboard Shortcuts:~%")
        (dolist (key mezzano.gui.compositor::*global-shortcuts*)
                (format t "~A ~15,T  ~A~%" (char-name (first key)) (second key))))


;;;-------------------------------------
;;; quick test

(defun __testq()
        (print-ascii-table)             (sleep 1)
        (make-cream-liqueur)            (sleep 1)
        (__threads)                     (sleep 1)
        (__stats)                       (sleep 1)
        (__my-new-thread)               (sleep 1)
;;;     (__timer)                       (sleep 1)
;;;     (__timer-2)                     (sleep 1)
        (__screen-dimensions)
        (__print-global-shortcuts)
        )


;;;-------------------------------------
;;; Print and set keybindings for repl and Med.

(defun __line-editor-key-bindings ()
  (maphash  (lambda( key value ) (format t "~8d   ~A   ~A~%" (char-code key) key value))
            mezzano.line-editor::*line-editor-command-table*))

(defun __med-key-bindings()
  (maphash (lambda( key value )
             (when (not (equal value 'med::SELF-INSERT-COMMAND))
               (format t "~8d   ~A   ~A~%" (char-code key) key value)))
            med::*global-key-map*))

(defun __line-editor-set-keys()
     (mezzano.line-editor:global-set-key   #\C-Right-Arrow   'mezzano.line-editor::forward-word)
     (mezzano.line-editor:global-set-key   #\C-Left-Arrow    'mezzano.line-editor::backward-word)
     ;; bind insert to delete-word-under-cursor
     (mezzano.line-editor:global-set-key   #\Insert (lambda( x )
                                                    (mezzano.line-editor::backward-word x)
                                                    (mezzano.line-editor::forward-word x)
                                                    (mezzano.line-editor::backward-delete-word x)
                                                    )))


(defun __med-set-keys()
  (med::set-key #\C-Left-Arrow  'med::backward-word-command             med::*global-key-map*)
  (med::set-key #\C-Right-Arrow 'med::forward-word-command              med::*global-key-map*)
  (med::set-key #\M-Left-Arrow  'med::backward-sexp-command             med::*global-key-map*)
  (med::set-key #\M-Right-Arrow 'med::forward-sexp-command              med::*global-key-map*)

  (med::set-key #\Home          'med::move-beginning-of-line-command    med::*global-key-map*)
  (med::set-key #\End           'med::move-end-of-line-command          med::*global-key-map*)
  (med::set-key #\C-Home        'med::move-beginning-of-buffer-command  med::*global-key-map*)
  (med::set-key #\C-End         'med::move-end-of-buffer-command        med::*global-key-map*)
  ;; bind insert to delete-word-under-cursor
  (med::set-key #\Insert        (lambda()
                                        (med::backward-word-command)
                                        (med::forward-word-command)
                                        (med::backward-kill-word-command))   med::*global-key-map*))



;;;-------------------------------------
;;; Help Mezzanos Filer to open more file types with the text editor.

(defun collecting-set.insert( set key value )
        (let* ((keyrec  (assoc key set :test #'equal))
               (keyrec  (if keyrec keyrec (list key)))
               (rest    (remove-if   (lambda(rec) (equal (car rec) key))   set))
            )
          (cons (cons (car keyrec) (cons value  (cdr keyrec)))
                rest)))

(defmacro filer-view-as( type extension )
        `(setf mezzano.gui.filer::*type-registry*
                (collecting-set.insert mezzano.gui.filer::*type-registry* ,type ,extension))
                )

(mapc (lambda( ext )  (filer-view-as :text ext))
              '("sit"  "times"  "lyx"
                "csv"  "xml"
                "fasm" "asm"
                "forth"
                "rabbit"
                "stk"  "scm" "rkt"
                "ml"   "mli"
                "m" "k" "pas" "obn" "mod"               ; Oberon
                "fl"                                    ; freelang
                "bli"                                   ; bliss
                "ppr"                                   ; popr
                "c" "cc"  "h"  "cmake"
                "py"
                "v" "zig"
                "go"
                "lua" "nelua"
                ))


;;; Do the following only at first boot, because
;;; after loading this init-file Mezzano makes a "Post load GC" and a snapshot.
(unless (member "init" *modules* :test #'equalp)


;;;     (__line-editor-set-keys)
;;;     (__med-set-keys)



        ;; terminate repl, that is intended to do the first snapshot
        (__threads :name "lisp listener" :state :sleeping :terminate :now)

        ;; create repl window
        (mezzano.gui.fancy-repl:spawn   :width  (* 112 10) :height 250)

        ;;;-------------------------------------
        ;;; Start Memory Monitor
        ;;; Key Bindings:
        ;;;     P       :physical-visualizer
        ;;;     G       :graphs
        ;;;     <spc>   (throw 'redraw nil) ; refresh window
        (setf mezzano.gui.memory-monitor::*graph-update-interval* 1)            ; seconds
        (mezzano.gui.memory-monitor:spawn 150 200)                              ; size in pixel (x y)

        ;;;-------------------------------------
        ;;; set my-debugger hook
        (setf *debugger-hook* 'my-debugger)

        ;;;-------------------------------------
        ;;; From irc chatlog:
        ;;;     ; turn caps-lock into a control key
        ;;;     (push '(#\Caps-Lock :control) mezzano.gui.compositor::*keyboard-modifiers*)
        ;;;     ; turn left-control into a caps-lock
        ;;;     (push '(#\Left-Control :caps-lock t) mezzano.gui.compositor::*keyboard-modifiers*)
        ;;; remove #\Caps-lock from modifiers so we can set it to Esc or whatever in keymaps.lisp
        (let ((x (pop mezzano.gui.compositor::*keyboard-modifiers*)))
          (unless (eql #\Caps-lock (first x))
            (print "#######  Removed something other than #\Caps-lock from *keyboard-modifiers*: << ~a >>" x)))

        (sleep 3)
        (mezzano.gui.compositor:move-window (find-window-by-name "Fancy-Repl")     1550 1111)
        (mezzano.gui.compositor:move-window (find-window-by-name "Memory-Monitor") 2400   10)


        ) ; unless member init *modules*


;;;-------------------------------------
;;; Load my second init file.

(defmacro rr()
  (load "REMOTE:/home/heiko/q/mezzano/second.lisp"))


;;;-------------------------------------
;;; Did loading of this file succeed?

(defun seven?() :seven!)                                        ; check-definition
(format t "~&Loading init.lisp done.~%")                        ; bootlog-message

;;;===========================================================================

(provide 'init)
(rr)

;;;===========================================================================

